# Mysql主从复制

## 1、多主一从

### 1.1、使用场景

- 数据汇总，将多个主数据库的数据复制到从库，从而方便数据统计分析。
- 读写分离，从库只能用于查询，提高数据库的整体性能。

### 1.2、主库配置

在数据库my.cnf文件中添加下面的配置

```shell
[mysqld]
## 设置server_id,注意要唯一
server-id=390001  
## 开启二进制日志功能，以备Slave作为其它Slave的Master时使用
#log-bin=mysql-bin   
## relay_log配置中继日志
relay_log=edu-mysql-relay-bin 
innodb_buffer_pool_size = 1G
#mysql8.0部署配置文件
user=mysql
character-set-server=utf8
default_authentication_plugin=mysql_native_password
secure_file_priv=/var/lib/mysql
expire_logs_days=7
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,ONLY_FULL_GROUP_BY
max_connections=5000
#skip-grant-tables
skip-name-resolve
lower_case_table_names=1
character-set-server = utf8
wait_timeout=2880000 
interactive_timeout = 2880000 
max_allowed_packet=500M
log-bin=mysql-bin
binlog-format=Row
#需要同步的库，不指定默认同步全部库
#binlog-do-db=database1
#binlog-do-db=database2
#binlog-do-db=database3
```

### 	1.3、创建同步用户

```shell
##创建主库授权从库同步的用户
mysql> grant replication slave on *.* to 'replication'@'192.168.26.%' identified by '123456';
Query OK, 0 rows affected, 1 warning (0.21 sec)
##刷新设置（修改的内容刷新到数据库配置里 ）
mysql> flush privileges;
Query OK, 0 rows affected (0.20 sec)
```

- `replication slave`远程用户备份权限
- `*.*`第一个星号代表库，第二个星号代表数据库里的表。可指定库和表
- `tm-replication'@'192.168.26.%`@前为用户名，@后为授权的IP段(就是允许那些IP使用这个账号权限访问)
- `cmtt12345`远程备份用户密码

### 1.4、查询相关状态

```shell
##log_bin是否开启
mysql> show variables like 'log_bin';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set (0.00 sec)
##查看master状态
mysql> show master status \G
*************************** 1. row ***************************
             File: mysql-bin.000028
         Position: 850248371
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)
```

### 1.4、从库配置

在数据库my.cnf文件中添加下面的配置

```shell
[mysqld]
## 设置server_id,注意要唯一
server-id=400001  
## 开启二进制日志功能，以备Slave作为其它Slave的Master时使用
#log-bin=mysql-bin   
## relay_log配置中继日志
relay_log=edu-mysql-relay-bin 
innodb_buffer_pool_size = 1G
#mysql8.0部署配置文件
user=mysql
character-set-server=utf8
default_authentication_plugin=mysql_native_password
secure_file_priv=/var/lib/mysql
expire_logs_days=7
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,ONLY_FULL_GROUP_BY
max_connections=5000
#skip-grant-tables
skip-name-resolve
lower_case_table_names=1
character-set-server = utf8
wait_timeout=2880000 
interactive_timeout = 2880000 
max_allowed_packet=500M
log-bin=mysql-bin
binlog-format=Row
#需要同步的库，不指定默认同步全部库
#binlog-do-db=database1
#binlog-do-db=database2
#binlog-do-db=database3
```

### 1.5、指定主库信息

在从库执行

```sql
mysql> CHANGE MASTER TO
  MASTER_HOST='192.168.111.39',
  MASTER_USER='replication',
  MASTER_PASSWORD='123456',
  MASTER_PORT=13306,
  MASTER_LOG_FILE='mysql-bin.000052',
  MASTER_LOG_POS=775182;
  #MASTER_HOST:主库ip
  #MASTER_USER：主库授权同步的用户名
  #MASTER_PASSWORD：主库授权同步的用户密码
  #MASTER_PORT：主库端口
  #MASTER_LOG_FILE：binlog文件名称，在主库show master status \G可以查询到
  #MASTER_LOG_POS：binlog文件位置，在主库show master status \G可以查询到
mysql> start slave; #启动从库复制
#查看从库同步状态
mysql> show slave status \G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.0.1
                  Master_User: replication
                  Master_Port: 13306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000052
          Read_Master_Log_Pos: 803042
               Relay_Log_File: edu-mysql-relay-bin.000002
                Relay_Log_Pos: 2690
        Relay_Master_Log_File: mysql-bin.000052
             Slave_IO_Running: Yes #yes标识成功
            Slave_SQL_Running: Yes #yes标识成功
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 803042
              Relay_Log_Space: 2901
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 390001
                  Master_UUID: 9d596738-ac37-11ef-b73e-321e249d8ee7
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)
```

查询binlog状态信息：得到MASTER_LOG_FILE和MASTER_LOG_POS

```shell
root@mysql5-0:/# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 603
Server version: 5.7.35-log MySQL Community Server (GPL)

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show master status \G
*************************** 1. row ***************************
             File: mysql-bin.000052
         Position: 775182
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)

mysql> 
```

