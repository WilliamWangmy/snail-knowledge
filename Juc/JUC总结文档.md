# JUC并发编程

## 1、基础知识概念

###  1.1、并发

​		并发（Concurrency）是指在同一时间段内，两个或多个程序或任务同时执行，且它们之间可能会相互影响。就是在同一处理器上同时处理多个任务；但是同一时刻只有一个事情发生。

### 1.2、并行

​		并行是指多个任务在同一时刻同时执行，或者在一定条件下同时进行，以达到相辅相成、相互促进的关系。就是在多台处理器同时处理多个任务；同一时刻多件时间事情发生各做各的。

### 1.3、 进程

​		进程是计算机中的程序关于某个数据集合上的一次运行活动，是系统进行资源分配和调度的基本单位，是操作系统结构的基础。在系统中运行的一个应用程序就是一个进程，每一个进程都有自己的内存空间和系统资源。

### 1.4、线程

​		线程是操作系统能够进行运算调度的最小单位，被包含在进程之中，是进程中的实际运作单位。一条线程指的是进程中一个单一顺序的控制流，一个进程中可以并发多个线程，每条线程并行执行不同的任务。在一个进程里面有一个或者多个线程，是大多是操作系统运行时的基本单元。

#### 1.4.1、用户线程

​		用户线程是由应用进程利用线程库创建和管理，不依赖于操作系统核心，不需要进行用户态与核心态的切换，速度快。一般线程默认的都是用户线程。用户线程是系统的工作线程，它来完成程序需要完成的业务。

#### 1.4.2、守护线程

​		守护线程是一种特殊的线程为其他线程服务。比如JVM垃圾回收。守护线程作为一个服务线程，没有服务对象就没必要继续运行了（当只剩下守护线程的时候，java虚拟机会自动退出）。多线程里面isDaemon就是在判断线程是否是守护线程，为true表示守护线程，否则是用户线程，线程默认的是用户线程也可以在线程运行前设置为守护线程。

### 1.5、管程

​		Monitor（监视器）也就是的锁。Monitor是一种同步机制，保证在同一时间只有一个线程可以访问被保护的对象。

在JVM第三版里面的是这样描述的：

执行线程就要求先成功持有管程，然后才能执行方法，最后当方法完成（无论是正常完成还是非正常完成）时释放管程。在方法执行期间，执行线程持有了管程,其他线程都无法再获取到同一个管程。

### 1.6、线程状态

线程的状态：创建、就绪、运行、阻塞、死亡

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/juc/%E7%BA%BF%E7%A8%8B%E7%8A%B6%E6%80%81.png)

| 方法      | 作用                                                   | 区别             |
| --------- | ------------------------------------------------------ | ---------------- |
| start     | 启动线程，由虚拟机自动调度执行run()方法                | 线程处于就绪状态 |
| run       | 线程逻辑代码块处理，JVM调度                            | 线程处于运行状态 |
| sleep     | 让当前正在执行的现场休眠                               | 不释放锁         |
| wait      | 使得当前线程等待                                       | 释放同步锁       |
| notify    | 唤醒在此对象监视器上等待的单个线程                     | 唤醒单个线程     |
| notifyAll | 唤醒在此对象监视器上等待的所有线程                     | 唤醒所有线程     |
| yiled     | 停止当前线程，让同等优先权的线程运行                   | 用Thread类调用   |
| join      | 使当前线程停下来等待，直至另一个调用join方法的线程终止 | 用线程对象调用   |

![](../images/juc/线程状态1.png)

## 2、CompletableFuture

### 2.1、多线程实现方式

#### 2.1.1、实现Runnable接口

```java
	/**
     * 没有返回值，不会抛出异常
     */
    public class RunnableThread implements Runnable{
        @Override
        public void run() {

        }
    }
```

#### 2.1.2、实现Callable接口

```java
	/**
     * 有返回值，会抛出异常
     */
    public class CallableThread implements Callable {
        @Override
        public Object call() throws Exception {
            return null;
        }
    }
```

#### 2.1.3、继承Thread类

```java
/**
 * 没有返回值，不会抛出异常
 */
public class CustomThread extends Thread {
        @Override
        public void run() {
            super.run();
        }
    }
```

### 2.2、Future接口

​		Future接口定了异步任务执行的方法，比如获取异步执行的结果、取消任务的执行、判断任务是否被取消、判断任务执行是否完毕等。

```java
public interface Future<V> {

    /**取消任务的执行**/
    boolean cancel(boolean mayInterruptIfRunning);

    /**判断任务是否被取消**/
    boolean isCancelled();

    /**判断任务执行是否完毕**/
    boolean isDone();

    /**获取异步执行的结果**/
    V get() throws InterruptedException, ExecutionException;

    /**获取异步执行的结果**/
    V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException;
}

```

#### 2.2.1、FutureTask

​		由于RunnableFuture继承了Runable接口因此FutureTask也可以用来实现多线编程，并且它的构造方法中传递了Callable接口来达到有多线有返回和抛出异常的目的。

```java
public class FutureTask<V> implements RunnableFuture<V> {
     public FutureTask(Callable<V> callable) {
        if (callable == null)
            throw new NullPointerException();
        this.callable = callable;
        this.state = NEW;       // ensure visibility of callable
    }


    public FutureTask(Runnable runnable, V result) {
        this.callable = Executors.callable(runnable, result);
        this.state = NEW;       // ensure visibility of callable
    }
	......
}

public interface RunnableFuture<V> extends Runnable, Future<V> {
    /**
     * Sets this Future to the result of its computation
     * unless it has been cancelled.
     */
    void run();
}

```

```java
public static void main(String[] args) {
        FutureTask futureTask = new FutureTask(new CallableThread());
        Thread thread = new Thread(futureTask,"futureTask");
        thread.start();
    }

    /**
     * 有返回值，会抛出异常
     */
    public static class CallableThread implements Callable {
        @Override
        public Object call() throws Exception {
            System.out.println("CallableThread come in...");
            return "CallableThread";
        }
    }
```

#### 2.2.2、优缺点

- 优点：可以配合线程池进行多线程异步，能够明显的提高程序的执行效率。
- 缺点：
  1. 调用get方法获取异步结果时如果异步任务耗时比较长会阻塞程序。
  2. 通过isDone方法轮询判断异步任务是否完成来获取异步结果，这种轮询的方式也会无谓的消耗CPU资源。

### 2.3、CompletableFuture

​		CompletableFuture能够更优雅的解决FutureTask带来的缺点，对多线程任务提供了更丰富的方法并且简化了异步编程的复杂性。它提供了设计模式中的观察者模式类似的机制来达到让任务执行完成后通知监听的一方。

​		CompletionStage代表异步计算过程中的某一个阶段，一个阶段完成后可能会触发另外一个阶段。

```java
public class CompletableFuture<T> implements Future<T>, CompletionStage<T> {
    ......
}
```

- 主要用到4个方法，分别是：

  1. 没有返回使用默认线程池：runAsync(Runnable runnable)
  2. 没有返回自定义线程池：runAsync(Runnable runnable,Executor executor)
  3. 有返回使用默认线程池：supplyAsync(Supplier<U> supplier)
  4. 有返回使用自定义线程池：supplyAsync(Supplier<U> supplier,Executor executor)

  > 默认线程池是ForkJoinPool.commonPool()

```java
public static void supplyAsync() {
        CompletableFuture.supplyAsync(() ->{
            System.out.println("异步任务开始.........");
            return 1;
        }).whenComplete((v,e) ->{
            //没有异常的情况
            if(e == null){
                System.out.println(v+1);
            }
        }).exceptionally(e->{
            System.out.println("异步任务异常.......");
            return null;
        });
    }
```

#### 2.3.1、常用API

- 获取结果和设置默认值

  ```java
  public T get() //一直等待结果
  public T get(long timeout, TimeUnit unit) //可以设置获取结果的等待时间
  public T join() //和get一样，它会抛出异常
  public T getNow(T valueIfAbsent)//如果获取到结果了就返回，没有获取到就返回默认值valueIfAbsent
  public boolean complete(T value)//打断异步任务直接将value作为结果，返回是否打断成功
  ```

- 对结果进行处理

  1. thenApply：两个线程有依赖的情况，需要将其串行化，但是由于存在存在依赖关系当前线程异常了就直接停止不会继续走后面的串行线程。
  2. handle：根据thenApply一样唯一的区别就是当前线程异常了可以继续走后面串行的线程。

- 对结果进行消费

  1. thenAccept：接收任务的结果并消费处理，无返回结果。任务A执行完后执行任务B，任务B需要A的结果但是任务B无返回值。
  2. thenRun：任务A执行完后执行任务B，任务B不需要A的结果。
  3. thenApply：任务A执行完后执行任务B，任务B需要A的结果，并且任务B有返回值。

  > 只要第一个任务使用了对应的线程池后面的串行线程任务都会使用该线程池。如果中间使用了....Async异步的话就会重新分配线程池。

- 对计算速度进行选用

  1. applyToEither：任务A和任务B谁先执行完成用谁的结果。

  ```java
  public static void main(String[] args) throws ExecutionException, InterruptedException {
          CompletableFuture<Integer> futureA = CompletableFuture.supplyAsync(() -> {
              return 1;
          });
          CompletableFuture<Integer> futureB = CompletableFuture.supplyAsync(() -> {
              return 2;
          });
          CompletableFuture<Integer> future = futureA.applyToEither(futureB, r -> {
              return r;
          });
          System.out.println(future.get());
      }
  ```

- 对计算结果进行合并

  1. thenCombine：两个任务都执行完成后需要把两个任务的结果都使用此方法进行合并，必须要等两个都完成。

  ```java
  public static void main(String[] args) {
          CompletableFuture<Integer> futureA = CompletableFuture.supplyAsync(() -> {
              return 1;
          });
          CompletableFuture<Integer> futureB = CompletableFuture.supplyAsync(() -> {
              return 2;
          });
          CompletableFuture<Integer> future = futureA.thenCombine(futureB, Integer::sum);
          System.out.println(future.join());
      }
  ```

### 2.4、线程池

#### 2.4.1、创建方式

- 创建一个线程池n个线程：Executors.newFixedThreadPool(n)
- 创建一个线程池一个线程：Executors.newSingleThreadExecutor()
- 创建一个线程池可扩容线程：Executors.newCachedThreadPool()

### 2.4.2、自定义线程池

```java
new ThreadPoolExecutor(int corePoolSize,//核心线程数，一直存活的线程数
                    int maximumPoolSize,//最大线程数
                    long keepAliveTime,//线程存活时间，恢复到核心线程数
                    TimeUnit unit,//存活时间单位
                    BlockingQueue<Runnable> workQueue,//阻塞队列，核心线程数用完了就会进入队列
                    ThreadFactory threadFactory,//线程工厂
                    RejectedExecutionHandler handler)//拒绝策略
```

-  当核心线程数用完后，新来的线程就会进入阻塞队列
- 当阻塞队列的线程数+核心线程数大于了最大线程数，这时就会新开线程进行处理，直到线程数达到最大线程后才不会新建
- 当阻塞队列满了后会根据拒绝策略，拒绝新线程的加入。

拒绝策略：

1. AbortPolicy：直接抛出异常，阻止线程运行。
2. CallerRunsPolicy：调用者运行一种调节机制，该策略不会抛弃任务，也不会抛出异常，而是将任务退回给调用者执行。
3. DiscardOldestPolicy：抛弃队列中等待最久的任务，然后把当前任务加入队列。
4. DiscardPolicy：丢弃无法处理的任务，不做任何处理也不会抛出异常。允许任务丢失的情况下可以使用。

## 3、Java锁分类

### 3.1、乐观锁和悲观锁

- 乐观锁认为一个线程去使用数据的时候不会有其他线程对数据进行更改，所以不会上锁。如果数据被修改只有放弃或者重新抢锁。判断是否被修改的方法有两个：一是根据版本号机制；二是采用CAS算法

- 悲观锁认为一个线程去拿数据时一定会有其他线程对数据进行更改，所以一个线程在拿数据的时候都会顺便加锁，这样别的线程此时想拿这个数据就会阻塞。**synchronize关键字和lock实现类都是悲观锁**。适合写操作多的场景保证写数据的正确性。

### 3.2、公平锁和非公平锁

- 公平锁，也称为顺序锁，是指多个线程按照申请锁的顺序来获取锁。当一个线程需要获取锁时，它会先查看此锁维护的等待队列，如果队列为空，或者当前线程等待队列的第一个，则占有锁，否则会加入到等待队列，以后会按照FIFO（先进先出）的规则从队列中取出。公平锁可以减少饥饿发生的概率，等待越久的请求越是能够得到优先满足。
- 非公平锁是多个线程加锁时直接尝试获得锁，获取不到才会进入等待队列的队尾等待。如果此时锁刚好可用，那么这个线程可以无需阻塞直接获取到锁，所以非公平锁有可能出现后申请锁的线程先获得锁的场景。非公平锁的优点在于吞吐量比公平锁要大。

### 3.3、写锁（独占锁）和读锁（共享锁）

- 写锁（独占锁）是一种排他锁，它只允许一个线程对共享资源进行写操作。当一个线程获得写锁时，其他线程无法获得读锁或写锁，直到该线程释放锁。这样可以确保在任何时刻只有一个线程能够修改资源，从而避免数据不一致的问题。
- 读锁（共享锁）则允许多个线程同时获得读锁，进行读操作。当一个线程获得读锁时，其他线程也可以获得读锁，但无法获得写锁。这样可以允许多个线程同时读取共享资源，但在读取过程中不允许其他线程进行写操作，从而保证数据的一致性。

### 3.4、互斥锁和读写锁

- 互斥锁，也称为互斥量或独占锁，只允许一个线程在同一时间访问共享资源。当一个线程获得互斥锁时，其他线程必须等待该线程释放锁后才能访问共享资源。这种锁机制可以确保数据的一致性和完整性，但可能会导致线程饥饿或死锁等问题。
- 读写锁则是为了解决读写频繁的场景而设计的。读写锁分为读锁和写锁，允许多个线程同时读取共享资源，但在写入时则需要独占式的访问。这意味着在写操作时，其他线程无法进行读写操作，而在读操作时，其他线程可以进行读操作。这种锁机制可以提高并发性能和资源利用率，但需要谨慎处理写操作时的互斥访问。

### 3.5、可重入锁（递归锁）

- 可重入锁是一种特殊的锁机制，允许同一个线程在已经持有某个锁的情况下再次获取该锁，而不会发生死锁。这种锁机制在多线程编程中非常有用，可以避免因为多次获取锁而导致的问题。

### 3.6、死锁

- 死锁是指两个或两个以上的进程（线程）在执行过程中，由于竞争资源或者由于彼此通信而造成的一种阻塞的现象，若无外力作用，它们都将无法推进下去。此时称系统处于死锁状态或系统产生了死锁，这些永远在互相等待的进程（线程）称为死锁进程（线程）。

### 3.7、自旋锁

- 自旋锁是一种特殊的锁机制，用于防止多处理器并发访问共享资源。它通过让等待锁的线程不断循环检查锁是否可用，以实现锁的保护。自旋锁与互斥锁和读写锁不同，它不会使线程进入睡眠状态，而是在获取锁之前一直循环检查。如果锁被其他线程占用，则自旋锁的持有者会一直循环等待，直到该锁被释放。这种机制可以减少线程上下文切换的开销，提高并发性能。

### 3.8、偏向锁/轻量级锁/重量级锁

- 偏向锁是一种优化技术，通过消除无竞争情况下的同步操作来提高程序的运行性能。它适用于那些对时间要求比较苛刻的场景，例如读写频繁的场景。在无竞争的情况下，偏向锁能够减少加锁和解锁的操作，从而提高程序的执行效率。然而，如果存在线程竞争，偏向锁可能会撤销并升级为其他类型的锁，这会导致性能下降。

- 轻量级锁是一种新型的锁机制，相对于使用monitor的传统锁而言，轻量级锁指的是存在多线程竞争，但是任意时刻最多只允许一个线程竞争获得锁，即不存在锁竞争太过激烈的情况。轻量级锁情况下，线程不会发生阻塞。轻量级锁适用于线程交替执行同步块的场景，绝大部分的锁在整个同步周期内都不存在长时间的竞争。

- 重量级锁是基于操作系统的互斥量(Mutex)实现的，互斥量是一种同步机制，用于控制多个线程对共享资源的访问。在Linux操作系统中，互斥量是通过内核中的Futex(Fast Userspace Mutex)实现的。当一个线程需要获取重量级锁时，它会向操作系统发送一个请求，操作系统会将该线程阻塞，直到锁被释放。同时，操作系统还会将该线程从运行状态转换为阻塞状态，从而避免该线程的占用时间片，减少资源的浪费。由于获取和释放锁的过程需要向操作系统发送请求，因此重量级锁的性能比轻量级锁和自旋锁要低。在高并发场景下，重量级锁容易造成线程阻塞，导致系统的性能下降。

总的来说，这三种锁机制各有特点和使用场景，需要根据具体的情况进行选择。偏向锁适用于读写频繁的场景，轻量级锁适用于线程交替执行同步块的场景，而重量级锁适用于需要保护共享资源免受并发访问影响的场景。

### 3.9、邮戳锁StampedLock

StampedLock 是并发包里面JDK8版本新增的一个锁，该锁提供了三种模式的读写控制，当调用获取锁的系列函数时，会返回一个long型的变量，这个变量代表了锁的状态。其中try系列获取锁的函数，当获取锁失败后会返回为0的stamp值。当调用释放锁和转换锁的方法时需要传入获取锁时返回的stamp值。StampedLock提供的三种读写模式的锁分别如下：

1. 写模式：是一个排它锁或者独占锁，某时只有一个线程可以获取该锁，当一个线程获取该锁后，其他请求读锁和写锁的线程必须等待，这类似于ReentrantReadWriteLock的写锁（不同的是这里的写锁是不可重入锁）。当目前没有线程持有读锁或者写锁时才可以获取到该锁。并且它提供了非阻塞的tryWriteLock方法。
2. 读模式：相当于直接操作数据，不加任何锁，连读锁都不要。
3. 乐观读：相当于直接操作数据，不加任何锁，连读锁都不要。

StampedLock具有以下优点：

1. 没有饥饿发生。
2. 支持锁升级、锁降级。
3. 超时和中断。
4. 一次唤醒所有读节点。

然而，StampedLock也存在一些缺点：

1. 无condition。
2. 非公平。
3. 写不可重入。

总的来说，StampedLock是一种用于解决并发问题的新型锁机制，适用于多种场景。它通过提供三种模式的读写控制来平衡对共享资源的访问，避免饥饿和死锁等问题，从而提高程序的并发性能和可靠性。

## 4、LockSupport

### 4.1、LockSupport

​		LockSupport是Java中的一个线程阻塞工具类，提供了一些用于创建锁和其他同步类的阻塞原语。它位于java.util.concurrent.locks包下，使用静态方法，可以在任意位置阻塞线程。LockSupport通过许可（permit）实现挂起线程、唤醒挂起线程功能。每个使用LockSupport的线程都会与一个许可关联，如果该许可可用，并且可在线程中使用，则调用park()方法将会立即返回，否则线程会被挂起等待许可。如果许可不存在，则释放一个许可。

### 4.2、等待唤醒机制

线程中三种等待唤醒方法：

1. 使用Object中的wait让线程等待，使用Object的notify方法唤醒线程。
   - **必须持有锁块才能使用**。
   - **必须要先wait再notify。**
2. 使用JUC包中Condition的await方法让线程等待，使用signal方法唤醒线程。
   - **必须持有锁块才能使用**。
   - **必须要先await再signal。**
3. LockSupport类中的park让线程等待，使用unpark来唤醒线程。
   - **每个线程有且只有一个许可证**

### 4.3、线程中断

线程中断只是一种协商机制并不是强制的。线程中断的方法：

1. 通过volatile变量实现，线程可见性在其他线程中修改了值可以在另外一个线程中获取到，也就是线程共享变量。
2. 通过AtomicBoolean实现，实现方式跟volatile一样，只不过这个是原子类型，在第八章原子类中详细说明。
3. 通过Thread自带API，如下：
   - **[interrupt](https://www.runoob.com/manual/jdk11api/java.base/java/lang/Thread.html#interrupt())**()  中断此线程，仅仅只是设置一个中断状态为true，发起一个中断线程的协商不会立刻停止线程。线程处于被阻塞的状态（sleep、wait、join等），在别的线程中调用当前线程对象的interrupt方法那么线程将会立即退出阻塞状态并且抛出一个InterruptedException异常，并且清楚了中断状态。
   - **[interrupted](https://www.runoob.com/manual/jdk11api/java.base/java/lang/Thread.html#interrupted())**()  判断当前线程是否被中断并且清楚当前的中断状态。
   - **[isInterrupted](https://www.runoob.com/manual/jdk11api/java.base/java/lang/Thread.html#isInterrupted())**() 判断当前线程是否被中断。

## 5、JMM（Java内存模型）

### 5.1、概念及特性

JMM（Java Memory Model，Java内存模型）是Java虚拟机规范中定义的一组规则，用于描述在多线程环境中，程序中线程之间的内存访问行为和同步规则。JMM的主要目标是解决多线程并发访问共享数据时可能出现的数据不一致性问题，保证程序的正确性和可靠性。屏蔽各个硬件平台和操作系统的内存访问差异实现让Java程序在各种平台下都能达到一致的内存访问效果。

- 可见性

  当一个线程修改了某一个共享变量的值，其他线程能够立即知道该变更，JMM规定了所有变量都存储在主内存中。

  每个线程都会有一个本地内存只属于自己线程的，它会拷贝主内存中的共享变量到自己本地内存，不能直接操作主内存的变量。线程之间的变量值均需要由主内存进行传递。

- 原子性

  在多线程环境下保证了只有一个线程进行处理，不会被其他线程干扰。

- 有序性

  程序中指令的执行顺序，按照代码的先后顺序执行。如果指令的顺序可以与代码顺序不一致，此过程叫指令的重排序。

- 共享内存：定义了所有共享变量都存储在主内存中。

### 5.2、先行发生原则

 如果一个操作执行的结果需要对另一个操作可见性或者代码重排序，那么这两个操作之间必须存在happens-before（先行发生）原则。**实质上它是一种可见性**。

1.  次序规则：一个线程内，按照代码顺序，写在前面的操作先行发生于写在后面的操作。
2. 锁定规则：一个unlock操作先行发生于后面对同一个锁的Lock操作。
3. volatile变量规则：对一个volatile变量的写操作先行发生于后面对这个变量的读操作，前面的写对后面的度是可见的。
4. 传递规则：根据依赖规则的值进行先后传递。
5. 线程启动规则：线程的start方法先行发生于此线程的每一个动作，也就是start后线程的操作才会发生。
6. 线程中断规则： 对线程中断方法的调用先行发生于被中断线程的代码检测到中断事件的发生。
7. 线程终止规则：线程中的所有操作都先行发生于对此线程的终止检测，可以通过isAlive等方法检测线程是否已终止。
8. 对象终结规则：一个对象的初始化完成先行发生于它的finalized方法的开始。

JMM通过这些概念和规则，定义了线程之间的内存访问行为和同步规则，确保在多线程环境中数据的正确性和一致性。通过合理地使用JMM，可以提高程序的并发性能和可靠性。

## 6、Volatile

### 6.1、概念及特性

`volatile` 是Java中的一个关键字，它被用于声明一个变量。这个关键字有几个关键作用，主要包括确保变量的可见性和禁止指令重排序。

1. **可见性**：当一个线程修改了一个volatile变量的值后，JMM会把该线程对应的本地内存中的共享变量值立即刷新回主内存中，新值对其他线程来说是立即可见的。换句话说，当一个线程读取一个volatile变量时，JMM会把该线程对应的本地内存设置为无效，重新回到主内存中读取最新的共享变量。因此它看到其他线程所做的最新修改。
2. **禁重排**：在JVM中，为了提高执行效率，可能会对指令进行重排序。但是，有时候这种重排序可能会导致一些问题，特别是当涉及到多个线程共享数据的时候。在这种情况下，volatile关键字可以用来禁止指令重排序，从而确保代码的正确性。
3. **不保证原子性**：在多线程环境下由于volatile可见性，每个线程都可以看到和修改。JVM无法保证从主内存加载到线程工作内存的值是最新的，也仅仅是加载数据时是最新的。

### 6.2、内存屏障

​		内存屏障（Memory Barrier）是一种JVM指令，Java内存模型中的重排规则会要求Java编译器生产JVM指令时插入特定的内存屏障指令，通过这些内存屏障指令，volatile实现了Java内存模型中的可见性和有序性，但是volatile不保证原子性。

内存屏障的主要作用包括：

1. 确保内存操作的可见性：当一个线程修改了一个变量的值，内存屏障可以确保该值对其他线程立即可见。
2. 控制内存操作的顺序：内存屏障可以防止编译器和处理器对指令进行重排序，确保内存操作的顺序符合程序的预期。
3. 防止指令重排序：通过插入内存屏障，可以禁止某些指令的重排序，确保操作的正确性和一致性。

- 内存屏障之前的所有写操作都要回写到主内存。
- 内存屏障之后的所有读操作都能够获得内存屏障之前的所有写操作的最新结果。

#### 6.2.1、写屏障

​		在写指令之后插入写屏障，强制把写缓冲区的数据刷回到主内存中，也就是告诉处理器在写屏障之前将所有存储在缓存中的数据同步到主内存，也就是当看到store屏障指令，就必须把该指令之前的所有写入指令执行完毕才能继续往下执行。

#### 6.2.2、读屏障

​		在读指令之前插入读屏障。让工作内存或者CPU高速缓存当中当中的缓存数据失效，重新回到主内存中获取最新的数据。也就是处理器在读屏障之后的读操作，都在读屏障之后执行，也就是在load屏障指令之后就能够保证后面的读取数据指令一定能够读取到最新的数据。

#### 6.2.3、四大屏障

1. LoadLoad。Load1:LoadLoad:Load2。保证Load1的读操作在load2及后续读取操作之前执行。
2. StoreStore。Store1:StoreStore:Store2。在Store2及其后的写操作执行之前，保证Store1的写操作已经刷新回到主内存。
3. LoadStore。Load1:LoadStore:Store2。在Store2及其后的写操作执行前，保证Load1的读取操作已经执行完成。
4. StoreLoad。Store1:StoreLoad:Load2。保证Store1的写操作已经刷新到主内存之后，Load2及其后的读取操作才能执行。

### 6.3、 先行发生规则

1. 当第一个操作为volatile读时，无论第二个操作是什么都不能重排。保证了volatile读之后的操作都不会被重排到volatile读之前。
2. 当第二个操作为volatile写时，无论第一个操作是什么都不能重排。保证了volatile写之前的操作不会被重排到volatile写之后。
3. 当第一个操作位volatile写时，第二个操作位volatile度时，不能重排。
4. 其他情况都能够重排。

### 6.4、读写过程

read(读写)->load(加载)->use(使用)->assign(赋值)->store(存储)->write(写入)->lock(锁定)->unlock(解锁)

![](..\images\juc\volatile%E8%AF%BB%E5%86%99%E8%BF%87%E7%A8%8B.jpg)

1. read：作用于主内存，将变量的值从主内存传输到工作内存，主内存到工作内存。
2. load：作用于工作内存，将read从主内存传输的变量值放入工作内存变量副本中，即数据传输。
3. use：作用于工作内存，将工作内存变量副本的值传递给执行引擎，每当JVM遇到需要该变量的字节码指令时会执行该操作。
4. assign：作用于工作内存，将从执行引擎接收的值赋值给工作内存变量，每当JVM遇到一个变量赋值直接指令时会执行该操作。
5. store：作用于工作内存，将赋值完成的工作变量的值写回到主内存中。
6. write：作用与主内存，将store传输过来的变量值赋值给主内存中变量。
7. lock：作用与主内存，将一个变量标记为一个线程独占的状态，只是写的时候加锁，就只是锁了写变量的过程。
8. unlock：作用于主内存，将一个处于锁定状态的变量释放，然后才能被其他线程占用。

## 7、CAS

### 7.1、概念及特性

​		CAS（Compare-and-Swap）是一种无锁并发编程技术，用于实现线程间的原子性操作。它通过比较和交换两个操作数来实现原子性，避免了使用锁所带来的性能开销和死锁问题。

CAS操作包含三个操作数：**内存位置V、预期的原值A和新值B。**在多线程环境中，如果内存位置V的值与预期原值A相匹配，则将该位置的值更新为新值B。这个操作是原子的，意味着在执行过程中不会被其他线程打断。

CAS操作具有以下几个特点：

1. 原子性：CAS操作是原子的，意味着它要么成功执行，要么不执行。在执行过程中不会被其他线程打断。
2. 自旋锁：当CAS操作失败时，通常会采用自旋锁的方式等待内存位置的值发生变化，即循环检查该位置的值是否与预期原值匹配，直到成功为止。
3. 内存屏障：CAS操作通常会伴随着内存屏障的使用，以确保内存操作的可见性和顺序性。
4. 乐观锁：CAS操作是基于乐观锁思想的，即认为冲突不会经常发生。因此，它会尝试进行操作，只有在失败时才会进行同步。

CAS技术在多线程并发编程中广泛应用，例如Java中的AtomicInteger、AtomicLong等类就使用了CAS技术来实现线程安全的原子性操作。通过合理地使用CAS技术，可以提高程序的并发性能和可靠性。

### 7.2、原理

​		CAS的核心类是Unsafe，由于Java方法无法访问底层系统，需要通过本地（native）方法来访问，Unsafe相当于一个后门，基于该类可以直接操作特定内存的数据。Unsafe类存在于sun.misc包中，其内部方法操作可以像C的指针一样直接操作内存，因为Java中CAS操作的执行依赖于Unsafe方法。

- Unsafe类中的所有方法都是native修饰的，也就是说Unsafe类中的方法都直接调用操作系统底层资源执行相应任务。
- 变量valueOffset表示该变量值在内存中的偏移地址，因为Unsafe就是根据内存偏移地址获取数据的。
- 变量value用volatile修饰，保证了多线程之间的内存可见性。

CAS是靠硬件实现的，从而在硬件层面提升效率，最底层还是交给硬件来保证原子性和可见性，实现方式是基于硬件平台的汇编指令cmpxchg。

设计思想是：比较要更新的变量的值V和预期值E，相等才会将V的值设置为新值N如果不相等自旋再来。

### 7.3、CAS自旋锁

​		CAS是实现自旋锁的基础，CAS利用CPU指令保证了操作的原子性，已到达锁的效果。自旋是指尝试获取锁的线程不会立即阻塞，而是利用循环的方式去尝试获取锁。

- 优点是减少上下文切换的消耗；
- 缺点是循环会消耗CPU，循环时间长开销大；存在ABA问题：线程1将原来的值A修改为B，线程1结束之前又将B改为了A，线程2修改值进行更新的时候发现值还是A，但是中间存在了修改为B的过程。

**解决ABA问题需要使用AtomicStampedReference带戳记流水（版本号）的原子类进行CAS。**

## 8、原子类

原子类都有CAS自旋锁的支撑。

### 8.1、基本类型原子类

1. [AtomicInteger](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicInteger.html)：可以原子方式更新的 `int`值。
2. [AtomicBoolean](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicBoolean.html)： 可以原子方式更新的值 `boolean` 。
3. [AtomicLong](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicLong.html)： 可以原子方式更新的 `long`值。

### 8.2、数组类型原子类

1. [AtomicIntegerArray](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicIntegerArray.html)：一个 `int`数组，其中元素可以原子方式更新。
2. [AtomicLongArray](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicLongArray.html)：一个 `long`数组，其中元素可以原子方式更新。
3. [AtomicReferenceArray](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicReferenceArray.html)<E>： 一组对象引用，其中元素可以原子方式更新。

### 8.3、引用类型原子类

1. [AtomicReference](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicReference.html)<V>： 可以原子方式更新的对象引用。  **自旋锁的引用**
2. [AtomicStampedReference](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicStampedReference.html)<V>：`AtomicStampedReference`维护一个对象引用以及一个整数“标记”，可以原子方式更新。**解决ABA的问题**
3. [AtomicMarkableReference](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicMarkableReference.html)<V>：`AtomicMarkableReference`维护一个对象引用以及一个标记位，可以原子方式更新。根据上面一样只不过把版本号改为是否修改过。

### 8.4、对象属性修改原子类

​		以一种线程安全的方式操作非线程安全对象内的某些字段。更新的对象属性必须是public volatile修饰符，因为对象的属性修改类型原子类都是抽象类，所以每次使用都必须要使用静态方法newUpdate()创建一个更新器。

1. [AtomicLongFieldUpdater](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicLongFieldUpdater.html)<T>：基于反射的实用程序，可以对指定类的指定 `volatile long`字段进行原子更新。
2. [AtomicIntegerFieldUpdater](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicIntegerFieldUpdater.html)<T>：基于反射的实用程序，可对指定类的指定 `volatile int`字段进行原子更新。
3. [AtomicReferenceFieldUpdater](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicReferenceFieldUpdater.html)<T,V>：基于反射的实用程序，可以对指定类的指定 `volatile`引用字段进行原子更新。

### 8.5、原子操作增强类

1. [DoubleAccumulator](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/DoubleAccumulator.html)： 一个或多个变量共同维护使用提供的函数更新的运行 `double`值。
2. [DoubleAdder](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/DoubleAdder.html)： 一个或多个变量共同维持最初的零和 `double`总和。
3. [LongAccumulator](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/LongAccumulator.html)：一个或多个变量共同维护使用提供的函数更新的运行 `long`值。
4. [LongAdder](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/LongAdder.html)：一个或多个变量共同维持最初为零的总和为 `long` 。比AtomicLong性能够更好（减少乐观锁的重试次数），只能用来计算加发，并且是从零开始。

### 8.6、LongAdder

​		当多个线程更新用于收集统计信息但不用于细粒度同步控制的目的的公共和时，此类通常优于[`AtomicLong`](https://www.runoob.com/manual/jdk11api/java.base/java/util/concurrent/atomic/AtomicLong.html) 。 在低更新争用下，这两个类具有相似的特征。 但在高争用的情况下，这一类的预期吞吐量明显更高，但代价是空间消耗更高。

![](https://gitee.com/WilliamWangmy/snail-image/raw/master/juc/LongAdder%E7%B1%BB%E5%9B%BE.png)

LongAdder在无竞争的情况，跟AtomicLongAdder一样，对同一个base进行操作，当出现竞争关系时则是采用化整为零分散热点的做法，用空间换时间，用一个数组Cells，将Value拆分进这个数组Cells。多个线程需要同时对value进行操作的时候，可以对线程id进行hash得到hash值，再根据hash值映射到这个数组cells下标，再对该下标所对应的值进行自增操作。当所有线程操作完毕，将数组Cells的所有值和base都加起来作为最终的结果。
$$
value=base+∑^n_iCell[i]
$$

```java
public class LongAdder extends Striped64 implements Serializable {
    public void add(long x) {
        //as表示cells引用，b表示获取的base值，v表示期望值，m表示cells数组的长度，a表示当前线程命中的cell单元格
        Cell[] as; long b, v; int m; Cell a;
        //casBase对base进行CAS，只有一个线程的时候这里就不会进入if
        if ((as = cells) != null || !casBase(b = base, b + x)) {
            //true表示无竞争，false表示有竞争，决定是否cells扩容
            boolean uncontended = true;
            if (as == null || (m = as.length - 1) < 0 ||
                //根据hash值得到cell单元格
                (a = as[getProbe() & m]) == null ||
                //对当前单元格值进行CAS
                !(uncontended = a.cas(v = a.value, v + x)))
                longAccumulate(x, null, uncontended);
        }
    }
    
    /**
    *并发汇总情况不是原子性的
    **/
    public long sum() {
        Cell[] as = cells; Cell a;
        long sum = base;
        if (as != null) {
            //汇总cell数组的值
            for (int i = 0; i < as.length; ++i) {
                if ((a = as[i]) != null)
                    sum += a.value;
            }
        }
        return sum;
    }
}
```

```java
abstract class Striped64 extends Number {
    
    /**
    * Number of CPUS, to place bound on table size 
    * 当前计算机CPU数量，Cell数组扩容时会使用到
    **/
    static final int NCPU = Runtime.getRuntime().availableProcessors();
     /**
     * Table of cells. When non-null, size is a power of 2.
     * Cells数组长度为2的幂，2,4,8,16...方便做位运算
     */
    transient volatile Cell[] cells;

    /**
     * Base value, used mainly when there is no contention, but also as
     * a fallback during table initialization races. Updated via CAS.
     * 基础value，当并发较低时，只累加该值主要用于没有竞争的情况，通过CAS更新
     */
    transient volatile long base;

    /**
     * Spinlock (locked via CAS) used when resizing and/or creating Cells.
     * 创建或者扩容Cells数组时使用的自旋锁变量调整单元格大小（扩容），创建单元格时使用的锁。
     */
    transient volatile int cellsBusy;
    
    /**
    * @param x 需要增加的值，默认都是1
    * @param fn 默认传null
    * @param wasUncontended 竞争标识，如果是false代表有竞争，只有cells初始化之后，并且当前线程CAS竞争修改失败才会是false
    **/
    final void longAccumulate(long x, LongBinaryOperator fn,
                              boolean wasUncontended) {
        //得到hash值
        int h;
        //getProbe()获取当前线程的hash值
        if ((h = getProbe()) == 0) {
            ThreadLocalRandom.current(); // force initialization
            h = getProbe();
            wasUncontended = true;
        }
        //表示扩容意向，false不会扩容，true可能会扩容
        boolean collide = false;                // True if last slot nonempty
        for (;;) {
            Cell[] as; Cell a; int n; long v;
            //数组已经初始化
            if ((as = cells) != null && (n = as.length) > 0) {
                //当前线程的hash值运算后映射得到Cell单元为null，说明该cell没有被使用
                if ((a = as[(n - 1) & h]) == null) {
                    //锁没有被占用，cell数组没有正在扩容
                    if (cellsBusy == 0) {       // Try to attach new Cell
                        Cell r = new Cell(x);   // Optimistically create
                        //锁没有占用，尝试去占锁
                        if (cellsBusy == 0 && casCellsBusy()) {
                            boolean created = false;
                            try {               // Recheck under lock
                                Cell[] rs; int m, j;
                                if ((rs = cells) != null &&
                                    (m = rs.length) > 0 &&
                                    rs[j = (m - 1) & h] == null) {
                                    rs[j] = r;
                                    created = true;
                                }
                            } finally {
                                //释放锁
                                cellsBusy = 0;
                            }
                            //赋值成功跳出循环
                            if (created)
                                break;
                            continue;           // Slot is now non-empty
                        }
                    }
                    collide = false;
                }
                //CAS失败
                else if (!wasUncontended)       // CAS already known to fail
                    wasUncontended = true;      // Continue after rehash
                //CAS
                else if (a.cas(v = a.value, ((fn == null) ? v + x :
                                             fn.applyAsLong(v, x))))
                    break;
                //超过CPU数量
                else if (n >= NCPU || cells != as)
                    collide = false;            // At max size or stale
                else if (!collide)
                    collide = true;
                //扩容
                else if (cellsBusy == 0 && casCellsBusy()) {
                    try {
                        if (cells == as) {      // Expand table unless stale
                            //按照当前数组的长度左移一位扩容
                            Cell[] rs = new Cell[n << 1];
                            //将原来的cell数组拷贝到新的数组
                            for (int i = 0; i < n; ++i)
                                rs[i] = as[i];
                            cells = rs;
                        }
                    } finally {
                        //释放锁
                        cellsBusy = 0;
                    }
                    collide = false;
                    continue;                   // Retry with expanded table
                }
                //重置当前线程的hash值
                h = advanceProbe(h);
            }
            //cells没有加锁且没有初始化，则尝试对它进行加锁，并且初始化cells数组
            else if (cellsBusy == 0 && cells == as && casCellsBusy()) {
                boolean init = false;
                try {                           // Initialize table
                    
                    if (cells == as) {
                        //新增2个槽位
                        Cell[] rs = new Cell[2];
                        //赋值
                        rs[h & 1] = new Cell(x);
                        cells = rs;
                        init = true;
                    }
                } finally {
                    cellsBusy = 0;
                }
                //初始化完成跳出循环
                if (init)
                    break;
            }
            //cells正在进行初始化，则尝试直接在基数base上进行累加
            else if (casBase(v = base, ((fn == null) ? v + x :
                                        fn.applyAsLong(v, x))))
                break;                          // Fall back on using base
        }
    }
}
```

![](..\images\juc\LongAdder%E6%BA%90%E7%A0%81%E8%A7%A3%E6%9E%90.jpg)

LongAdder和AtomicLong：

- AtomicLong 线程安全，可允许一些性能损耗要求高精度时可使用；保证进度损失性能；针对多线程对单个热点值value进行原子操作
- LongAdder在高并发下性能表现好，对精确度不高的可以使用；保证性能，精度不高；每个线程拥有自己的槽，各个线程一般只针对自己槽中的值进行CAS。

## 9、ThreadLocal

### 9.1、概念及特性

​		ThreadLocal是Java中的一个类，它提供了线程局部变量（Thread-local Variables）的实现。线程局部变量是每个线程都有自己独立的一份变量副本，而这个变量的值对每个线程都是不同的。通过ThreadLocal类，可以创建线程局部变量，并确保每个线程都有自己独立的变量副本。

特性：

- 不能解决线程间共享数据问题
- 适用于线程隔离场景
- 在不同线程内创建独立的副本解决线程安全问题
- 每个线程都有一个只属于自己的map并维护了ThreadLocal对象，只能被它只有的线程访问
- ThreadLocalMap的entry对ThreadLocal为弱引用，避免了ThreadLocal对象无法被回收的问题
- 都会通过expungeStaleEntry、replaceStaleEntry、cleanSomeSlots方法回收建为null的entry。

### 9.2、源码分析

```java

public class Thread implements Runnable {
	 /* ThreadLocal values pertaining to this thread. This map is maintained
     * by the ThreadLocal class. */
    //每一个线程都会new一个自己的ThreadLocalMap
    ThreadLocal.ThreadLocalMap threadLocals = null;
}

public class ThreadLocal<T> {
    /*set方法**/
    public void set(T value) {
        //获取当前线程
        Thread t = Thread.currentThread();
        //获取ThreadLocalMap
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            //将当前线程作为key  调用ThreadLocalMap的set方法
            map.set(this, value);
        } else {
            //新建ThreadLocalMap当前线程为key
            createMap(t, value);
        }
    }
    
    public T get() {
        //获取当前线程
        Thread t = Thread.currentThread();
        //获取ThreadLocalMap
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            //获取当前线程对应的value，调用ThreadLocalMap中getEntry方法，其中会调用getEntryAfterMiss方法中expungeStaleEntry去清理掉key为null的脏数据
            ThreadLocalMap.Entry e = map.getEntry(this);
            if (e != null) {
                @SuppressWarnings("unchecked")
                T result = (T)e.value;
                return result;
            }
        }
        //设置初始值，默认为null
        return setInitialValue();
    }
    
     public void remove() {
         //获取当前线程的ThreadLocalMap，删除对应的值
         ThreadLocalMap m = getMap(Thread.currentThread());
         if (m != null) {
             //调用ThreadLocalMap方法
             m.remove(this);
         }
     }
    
    static class ThreadLocalMap {

            /**
             * The entries in this hash map extend WeakReference, using
             * its main ref field as the key (which is always a
             * ThreadLocal object).  Note that null keys (i.e. entry.get()
             * == null) mean that the key is no longer referenced, so the
             * entry can be expunged from table.  Such entries are referred to
             * as "stale entries" in the code that follows.
             * 弱引用对象：
             */
            static class Entry extends WeakReference<ThreadLocal<?>> {
                /** The value associated with this ThreadLocal. */
                Object value;

                Entry(ThreadLocal<?> k, Object v) {
                    super(k);
                    value = v;
                }
            }
        private void set(ThreadLocal<?> key, Object value) {

            // We don't use a fast path as with get() because it is at
            // least as common to use set() to create new entries as
            // it is to replace existing ones, in which case, a fast
            // path would fail more often than not.

            Entry[] tab = table;
            int len = tab.length;
            int i = key.threadLocalHashCode & (len-1);

            for (Entry e = tab[i];
                 e != null;
                 e = tab[i = nextIndex(i, len)]) {
                ThreadLocal<?> k = e.get();

                if (k == key) {
                    e.value = value;
                    return;
                }
			  //如果key=null，调用replaceStaleEntry方法中去调用expungeStaleEntry方法，expungeStaleEntry中会将value也置为null
                if (k == null) {
                    replaceStaleEntry(key, value, i);
                    return;
                }
            }
            
        /**上面调用remove方法**/
        private void remove(ThreadLocal<?> key) {
            Entry[] tab = table;
            int len = tab.length;
            int i = key.threadLocalHashCode & (len-1);
            for (Entry e = tab[i];
                 e != null;
                 e = tab[i = nextIndex(i, len)]) {
                if (e.get() == key) {
                    e.clear();
                    //通过此方法区清理掉key为null的脏entry
                    expungeStaleEntry(i);
                    return;
                }
            }
        }
    }
}


```

### 9.3、内存泄露问题

​		 ThreadLocalMap是以线程自己当做key进行存储，线程自己要用的时候使用get()方法去获取，在使用完毕后必须要remove，特别是在线程池环境下如果不remove会到导致内存泄露。

​		由于key使用的弱引用发生gc时就会被回收，被回收后key=null。在线程池的环境下线程是可以复用的由于key=null，value还存在引用也就不会被gc回收就会一直存放在ThreadLocalMap里面，导致了数据混乱和内存泄露，因此**在使用完成后需要将remove**。

​		为什么key要使用弱引用？key如果使用强引用就会导致key指向的ThreadLocal对象及V指向的对象不能被GC回收造成内存泄露，使用弱引用就会被gc回收解决内存泄露的问题。

## 10、对象内存布局

![](..\images\jvm\%E5%AF%B9%E8%B1%A1%E5%86%85%E5%AD%98%E5%B8%83%E5%B1%801.png)

​		对象在堆内存中的存储布局结构分为：对象头、实例数据、对齐填充，而对象头又分为对象标记和类元信息，类元信息存储的是指向该对象元数据的首地址。

- 对象头

  - 对象标记Mark Word

    对象标记里面存储了哈希码、GC标记、GC次数、同步锁标记、偏向锁持有者

    | 存储内容                             | 标志位 | 状态       |
    | ------------------------------------ | ------ | ---------- |
    | 对象哈希码、对象分代年龄             | 01     | 未锁定     |
    | 指向锁记录的指针                     | 00     | 轻量级锁定 |
    | 指向重量级锁的指针                   | 10     | 重量级锁定 |
    | 空、不需要记录                       | 11     | GC标记     |
    | 偏向线程id、偏向时间戳、对象分代年龄 | 01     | 可偏向     |

    > **在64为系统重，对象标记占了8个字节、类型指针占了8个字节，一共16个字节。也就是在Object obj = new Object（）占了16个字节。因为没有实例数据和对齐填充。**

  - 类元信息（类型指针）

    类元信息对象指向它的类元数据的指针，类元数据也就是类模板new一个对象的对象名。

- 实例数据

  存放类的属性数据信息，包括父类的属性信息。

- 对齐填充

  虚拟机要求对象的起始地址必须是8字节的整数倍，填充数据不是必须存在的，仅仅是为了字节对齐部分内存。

查看对象在JVM中的一些信息：

- 引入jol工具依赖

  ```xml
  <dependency>
  	<groupId>org.openjdk.jol</groupId>
  	<artifactId>jol-core</artifactId>
  	<version>0.17</version>
  </dependency>
  ```

- 打印信息

  这里默认开启了类型指针压缩所以类型指针压缩后就只有4个字节。

  ```java
  public static void main(String[] args) {
          Object o = new Object();
          System.out.println(ClassLayout.parseInstance(o).toPrintable());
  }
  
  java.lang.Object object internals:
   OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
        0     4        (object header)                           01 00 00 00 (00000001 00000000 00000000 00000000) (1)
        4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
        8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
       12     4        (loss due to the next object alignment)
  Instance size: 16 bytes
  Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
  ```

## 11、Synchronized

`Synchronized` 是 Java 中的一个关键字，用于控制多线程并发访问共享资源时的同步。通过 `synchronized` 关键字，可以确保同一时间只有一个线程能够访问被保护的代码块或方法，从而避免多线程并发访问导致的数据不一致性和并发修改问题。

`Synchronized` 的作用范围可以是代码块、方法或类。当一个方法或代码块被声明为 `synchronized` 时，同一时间只能有一个线程能够访问该方法或代码块。如果多个线程同时访问被 `synchronized` 保护的方法或代码块，其他线程将会被阻塞，直到当前线程释放锁并退出。

`Synchronized`是一个操作系统级别的重量级锁，效率低下，涉及到用户态（程序）和内核态质检的切换。因为监视器锁（monitor）是依赖于底层的操作系统的mutex lock（系统互斥量）来实现的。底层是使用monitorenter和monitorexit（有两个一个是正常结束退出，一个是异常退出）指令来实现的。

### 11.1、Synchronized锁升级

锁对象及标志位

![](..\images\juc\%E9%94%81%E7%8A%B6%E6%80%81%E5%8F%8A%E6%A0%87%E5%BF%97%E4%BD%8D.png)

`Synchronized`用的锁是存在Java对象头里的对象标记（Mark Work）中，锁升级主要依赖Mark Work中锁标志位和释放偏向锁标志位。

- 偏向锁：Mark Work存储的是偏向的线程id
- 轻量锁：Mark Work存储的是指向线程栈中的Lock Record的指针。
- 重量锁：Mark Work存储的是指向堆中的monitor对象的指针。

![](..\images\juc\synchronized%E9%94%81%E5%8D%87%E7%BA%A7.jpg)

#### 11.1.1、无锁

value的Mark Work是从右下角往上看也就是00 00 00 00 00 00 00 01。第一排01对照锁的标志位那么就是无锁状态。

```java
public static void main(String[] args) {
        Object o = new Object();
        System.out.println("二进制hashcode："+Integer.toBinaryString(o.hashCode()));
        System.out.println(ClassLayout.parseInstance(o).toPrintable());
    }

二进制hashcode：1110100101000010100010010000010
java.lang.Object object internals:
 OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
      0     4        (object header)                           01 82 44 a1 (00000001 10000010 01000100 10100001) (-1589345791)
      4     4        (object header)                           74 00 00 00 (01110100 00000000 00000000 00000000) (116)
      8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
     12     4        (loss due to the next object alignment)
Instance size: 16 bytes
Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
```

#### 11.1.2、偏向锁

单线程竞争，当线程A第一次竞争到锁时，通过修改Mark Word中的偏向线程id、偏向模式。如果没有其他线程竞争那么只有偏向锁的线程将永远不需要进行同步。也就是当一段代码被同一个线程多次访问，由于只有一个线程那么该线程的后续访问会自动获得锁。

偏向锁只有遇到其他线程尝试竞争偏向锁时，持有偏向锁的线程才会释放锁，线程是不会主动释放偏向锁的。

从JDK6开始默认就是开启偏向锁的。

对照锁的标志位101  偏向锁。

```java
    public static void main(String[] args) {
        try {
            //由于偏向锁在启动时默认要延时4秒所以这里睡眠五秒等待
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Object o = new Object();
        new Thread(() ->{
            synchronized (o){
                System.out.println(ClassLayout.parseInstance(o).toPrintable());
            }
        }).start();

    }
    
java.lang.Object object internals:
 OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
      0     4        (object header)                           05 c8 5a bd (00000101 11001000 01011010 10111101) (-1118124027)
      4     4        (object header)                           04 02 00 00 (00000100 00000010 00000000 00000000) (516)
      8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
     12     4        (loss due to the next object alignment)
Instance size: 16 bytes
Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
```

偏向锁的撤销，使用了一种等待竞争出现才会释放锁的机制，只有当其他线程竞争锁时，持有偏向锁的原来线程才会被撤销。**撤销需要等待全局安全点（STW，该时间点上没有字节码正在执行），同步检查只有偏向锁的线程是否还在执行。当一个对象已经计算过一次哈希码后，它就再也无法进入偏向锁状态了；当一个对象正处于偏向锁状态，又收到需要计算其一致性哈希码的请求时，它的偏向锁状态会被立即撤销，并且锁会膨胀为重量级锁**。

#### 11.1.3、轻量级锁

​		当偏向锁发生了竞争，就会尝试用CAS来替换Mark Work里面的线程id位新线程的id。如果竞争成功了，表示之前的线程不存在了，Mark Work里面的线程id位新线程id锁不会升级仍然是偏向锁。如果竞争失败了，这时候可能需要升级为轻量级锁，才能保证线程之前的公平竞争锁。

​	多线程竞争，但是在任意时刻最多只有一个线程竞争，不存在竞争激烈的情况，也就是没有线程阻塞的情况，等待时间很短。**主要通过CAS来竞争锁**。

​	轻量级锁加锁：JVM会为每个线程在当前线程的栈顺中创建用于存储锁记录的空间，官方称为Displaced Mak Word。若一个线程获得锁时发现是轻量级锁，会把锁的MarkWord复制到自己的Displaced Mark Word里面。然后线程尝试用CAS将锁的MarkWord替换为指向锁记录的指针。如果成功，当前线程获得锁，如果失败，表示Mark ord已经被替换成了其他线程的锁记录，说明在与其它线程竞争锁，当前线程就尝试使用自旋来获取锁。

​	轻量级锁释放：在释放锁时，当前线程会使用CAS操作将Displaced Mark Word的内容复制回锁的Mark Word里面。如果没有发生竞争，那么这个复制的操作会成功。如果有其他线程因为自旋多次导致轻量级锁升级成了重量级锁，那么CAS操作会失败，此时会释放锁并唤醒被阻塞的线程。

- 程序启动时关闭偏向锁直接获取轻量级锁，或者程序直接启动偏向锁还在延时时间内就会直接到轻量级锁，对比锁标志00为轻量级锁。

  ```java
  public static void main(String[] args) {
          Object o = new Object();
          new Thread(() ->{
              synchronized (o){
                  System.out.println(ClassLayout.parseInstance(o).toPrintable());
              }
          }).start();
  
      }
      
  java.lang.Object object internals:
   OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
        0     4        (object header)                           10 ed 8f 5a (00010000 11101101 10001111 01011010) (1519381776)
        4     4        (object header)                           cb 00 00 00 (11001011 00000000 00000000 00000000) (203)
        8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
       12     4        (loss due to the next object alignment)
  Instance size: 16 bytes
  Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
  ```

#### 11.1.4、重量级锁

​		jdk6之后，使用了自适应自旋锁的原理来决定是否升级到重量级锁。不是根据固定的自选次数决定的而是根据同一个锁上一次自旋时间和拥有线程的状态来决定的，比如自选十次都是成功的那么下一次大概率都会成功，如果自选十次都失败下次大概率也是失败那么久要升级为重量级锁了。

​		Java中synchronized的重量级锁，是基于进入和退出Monitor对象实现的。在编译时会将同步块的开始位置插入monitor enter指令，在结束位置插入monitor exit指令。当线程执行到monitor enter指今时，会尝试获取对象所对应的Monitor所有权，如果获取到了，即获取到了锁，会在Monitor的owner中存放当前线程的id，这样它将处于锁定状态，除非退出同步块，否则其他线程无法获取到这个Monitor。

### 11.2、锁消除

​		也就是在多线程调用的方法类重新新建了一个锁对象使用synchronized进行加锁，相当于没有一个线程就新建了一个对象这样加锁是一个无用功。这个锁对象并没有被公用扩散到其他线程使用，底层的机器码根本没有加这个锁标识，清除了锁的使用。

### 11.3、锁粗化

​		同一个锁对象使用了多次synchronized块来处理业务，那么JIT编译器会把这些synchronized代码块合并成一个，加粗加大范围一次申请锁使用即可，避免多次申请和释放锁从而提升性能。

## 12、AQS

### 12.1、概率及特性

AQS（AbstractQueuedSynchronizer）是Java中的一个抽象类，用于实现基于FIFO（先进先出）等待队列的同步器。它是Java并发编程中非常重要的一个组件，提供了构建锁、信号量等同步器的基础框架。

AQS的主要作用是实现线程之间的同步，通过维护一个等待队列来管理等待线程。当一个线程需要获取同步状态时，如果当前同步状态不可用，则该线程会被放入等待队列中等待。当同步状态变为可用时，等待队列中的一个线程将被唤醒并获得同步状态。

AQS的主要特点是基于维护一个状态来控制同步状态的行为。这个状态是一个整型变量，可以通过setState()和getState()方法进行读写。在AQS中，通过继承和重写一些方法来实现具体的同步器，例如获取和释放同步状态等操作。

AQS的实现原理是通过维护一个等待队列和一个节点池来实现线程的等待和唤醒。当一个线程需要获取同步状态时，如果当前同步状态不可用，则该线程会被加入到等待队列中，并被放入节点池中等待被唤醒。当同步状态变为可用时，节点池中的一个线程将被唤醒并获得同步状态。

总的来说，AQS是一个非常有用的框架，它提供了一种构建线程同步器的通用机制。通过继承和重写AQS中的方法，可以方便地实现各种不同的同步器，例如ReentrantLock、CountDownLatch等。

### 12.2、源码分析

```java
public abstract class AbstractQueuedSynchronizer
    extends AbstractOwnableSynchronizer
    implements java.io.Serializable {

   
    static final class Node {
        /** 表示锁以共享的模式等待锁 */
        static final Node SHARED = new Node();
        /** 表示锁以独占的模式等待锁 */
        static final Node EXCLUSIVE = null;

        /** waitStatus=1表示线程获取锁的请求已经取消 */
        static final int CANCELLED =  1;
        /** waitStatus=-1表示线程已经准备好，等待资源的释放 */
        static final int SIGNAL    = -1;
        /** waitStatus=-2表示节点在等待队列中，节点线程等待唤醒*/
        static final int CONDITION = -2;
        /**
         * waitStatus=-3表示线程处在Shared情况下该字段才会使用
         */
        static final int PROPAGATE = -3;

        /**
         * 当前节点在队列中的状态
         */
        volatile int waitStatus;

        /**
         * 前驱指针
         */
        volatile Node prev;

        /**
         * 后继指针
         */
        volatile Node next;

        /**
         * 处于该节点的线程
         */
        volatile Thread thread;

        /**
         * 指向下一个处于Condition状态的节点
         */
        Node nextWaiter;
    }

    /**
     * 初始化使用的节点头指针
     */
    private transient volatile Node head;

    /**
     * 初始化使用的节点尾指针
     */
    private transient volatile Node tail;

    /**
     * 锁状态，表示是否有线程占用锁
     */
    private volatile int state;
}
```

- 以ReentrantLock为例分析AQS源码，ReentrantLock可以设置成公平锁或者分公平锁。UML类图如下：

![](..\images\juc\ReentrantLock%20UML%E7%B1%BB%E5%9B%BE.png)

![](..\images\juc\AQS%E6%BA%90%E7%A0%81%E5%88%86%E6%9E%90%EF%BC%88ReentrantLock%EF%BC%89.jpg)

## 13、StampedLock

### 13.1、读写锁

​		读写锁（ReadWriteLock）是一种用于多线程并发访问共享资源的同步机制。读写锁分为读锁和写锁，允许多个线程同时读取共享资源（读锁），但在写操作时则需要独占式的访问（写锁）。

读写锁的主要特点是能够提高并发性能，特别是在读多写少的情况下。多个线程可以同时读取共享资源，避免了读操作的互斥访问，从而提高了并发性能。在写操作时，需要独占式的访问，保证了对共享资源的互斥访问，避免了并发修改导致的数据不一致性问题。

读写锁的实现通常包含两个锁：一个读锁和一个写锁。读锁是共享的，允许多个线程同时持有，而写锁是独占的，只能由一个线程持有。当一个线程想要读取共享资源时，它可以获取读锁，而当一个线程想要写入共享资源时，它需要获取写锁。如果一个线程已经获取了写锁，则其他线程无法获取读锁或写锁，直到该线程释放锁。

读写锁的使用可以提高并发性能，**但需要注意避免死锁和饥饿问题**。在使用读写锁时，需要合理地控制读和写的顺序和时间，避免长时间持有写锁导致其他线程饿死或者死锁的情况发生。

总的来说，读写锁是一种用于多线程并发访问共享资源的同步机制，能够提高并发性能，特别是在读多写少的情况下。但需要注意避免死锁和饥饿问题，合理地使用读写锁可以提高程序的性能和可靠性。

### 13.2、锁降级

​		锁降级是指在使用读写锁时，先获取写锁，然后获取读锁，最后释放写锁的过程。通过锁降级，可以实现在获取写锁之后，其他线程可以获取读锁进行读取操作，从而提高并发性能。

在Java的ReentrantReadWriteLock类中，锁降级是通过tryAcquireShared方法和fullTryAcquireShared方法实现的。当一个线程获取写锁后，如果其他线程想要获取读锁，它们可以尝试获取读锁，如果成功获取读锁，则实现了锁降级。

需要注意的是，锁降级并不适用于所有情况。如果一个线程在获取写锁后长时间持有该锁，会导致其他线程无法获取读锁或写锁，从而导致死锁或饥饿问题。因此，在使用锁降级时，需要合理地控制锁的持有时间和顺序，避免长时间持有写锁导致的问题。

总的来说，锁降级是一种提高并发性能的同步机制，但需要**注意避免死锁和饥饿问题**。合理地使用锁降级可以提高程序的性能和可靠性。

- ReentrantReadWriteLock支持公平性的选择是公平锁还是非公平锁；并且是可重入锁获取读锁之后可以再次获取读锁。而写线程获取写锁之后能够再次获取写锁，也可以获取读锁。
- ReentrantReadWriteLock锁降级：遵循获取写锁、获取读锁再是否写锁的次序，写锁能够降级成为读锁。也就是同一个线程持有写锁可以从写锁降级为了读锁。
- 锁降级策略：
  1. 如果一个线程持有写锁，没有释放写锁的情况下，他可以继续获取读锁。也就是从写锁降级成了读锁。
  2. 先获取写锁，再获取读锁，再释放写锁的顺序。
  3. 如果释放了写锁，那么就完全转换成了读锁。

```java
    public static void main(String[] args) {
        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();
        ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();

        writeLock.lock();
        System.out.println("获取写锁：写入...............");
        readLock.lock();
        System.out.println("获取读锁：读取................");
        writeLock.unlock();
        System.out.println("释放写锁，读写完成..............");

    }
```

- **读锁未释放是不能获取写锁，也就是读锁无法升级为读锁。**

### 13.3、锁饥饿

​		饥饿是指线程因无法访问所需资源而无法执行下去的情况。由于存在大量的读锁，导致读锁无法释放，写锁一直获取不到就会出现写锁的锁饥饿问题。

解决锁饥问题：

1. 使用公平锁可以缓解锁饥饿问题。但是吞吐量和性能就会降低。
2. 使用StampedLock邮戳锁，采用乐观获取锁之后，其他线程尝试获取写锁时不会被阻塞。

### 13.4、StampedLock

​		StampedLock是Java并发包中新增的一个锁，**它对ReentrantReadWriteLock读写锁进行了改进**。StampedLock提供了三种模式的读写控制，包括写锁、读锁和乐观读模式。

StampedLock的主要特点是支持在读操作时无阻塞地读取共享资源，即在没有写操作的情况下，多个线程可以同时进行读操作，最大程度地提升读的效率。当发生写操作时，需要独占式的访问共享资源，这时需要获取写锁。同时，StampedLock还支持锁降级和锁升级。

StampedLock的实现原理是基于乐观锁的思想和CAS（Compare-and-Swap）操作。在读取共享资源时，如果发生了写操作，应该通过重试的方式来获取新的值，而不是阻塞写操作。这种操作方式决定了StampedLock在读线程非常多而写线程非常少的场景下非常适用，同时还避免了写饥饿情况的发生。

StampedLock使用了一种称为**票据（stamp）**的概念来标识锁的状态。当调用获取锁的系列函数时，会返回一个long型的变量，即票据。这个票据代表了锁的状态。在释放锁和转换锁的操作中，需要传入获取锁时返回的票据值。

总的来说，StampedLock是一种改进的读写锁实现，它通过乐观锁和CAS操作来提高并发性能和避免死锁和饥饿问题。合理地使用StampedLock可以提高程序的性能和可靠性。

#### 13.4.1、特点

- 所有获取锁的方法都会返回一个邮戳/票据（stamp），stamp为零表示获取失败。
- 所有释放锁的方法都需要一个邮戳/票据（stamp），必须要和获取的一致。
- 三种访问方式：写锁、读锁和乐观读模式

缺点：

- 不支持重入，有可能会到死锁的风险。
- 悲观读锁和写锁都不支持条件变量
- 不能调用线程中断方法

