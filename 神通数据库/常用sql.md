```sql
--创建表空间语句：
create tablespace snail DATAFILE 'D:\software\shentong\tableSpace.dbf' SIZE 1M autoextend on next 128M;
--创建用户语句：
create user snail password 'snail' default tablespace snail;
--给用户授权语句：
grant role sysdba to user snail;
```