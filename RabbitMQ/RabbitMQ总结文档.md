# RabbitMQ

## 1、概念及特点

​		RabbitMQ是一个由Erlang语言开发的开源消息代理和队列服务器，它基于AMQP（高级消息队列协议）进行通信。RabbitMQ被广泛用于分布式系统中，以进行应用程序之间的通信。

**RabbitMQ的主要特点和优势包括：**

1. **跨平台与跨语言支持**：RabbitMQ可以在多种操作系统和平台上运行，并支持多种编程语言，如Java、Python、Ruby、PHP、C/C++等，这使其非常灵活且易于集成到各种应用环境中。
2. **高并发性能**：Erlang语言的特点使得RabbitMQ在高并发场景下表现出色，能够处理大量的消息和连接。
3. **消息持久化**：RabbitMQ支持消息的持久化存储，确保即使在服务器重启或故障的情况下，消息也不会丢失。
4. **灵活的路由和队列机制**：RabbitMQ提供了灵活的路由和队列机制，允许用户根据业务需求定义消息的流向和处理方式。
5. **易于扩展和管理**：RabbitMQ支持集群部署，可以轻松地扩展消息处理能力。同时，它也提供了丰富的管理工具和API，方便用户对RabbitMQ进行监控和管理。

**在分布式系统中，RabbitMQ通常用于以下场景：**

1. **任务异步处理**：对于不需要同步处理且耗时较长的操作，可以将这些操作放入RabbitMQ队列中，由后台服务异步处理，从而提高系统的响应速度。
2. **应用程序解耦合**：RabbitMQ可以作为应用程序之间的中介，实现生产者和消费者之间的解耦合，降低系统之间的依赖关系。

## 2、核心概念

1. **Message（消息）**：这是RabbitMQ中传递的数据单元。消息由消息头和消息体组成。消息头包含一系列可选参数，如routing-key（路由键）、priority（消息优先级）和delivery-mode（是否需要持久化存储）等。消息体则是实际发送的内容，通常为JSON字符串形式。
2. **Producer（生产者）**：这是产生数据并发送消息的程序。生产者将消息发送到交换机，由交换机根据路由规则将消息路由到相应的队列中。
3. **Exchange（交换机）**：交换机是RabbitMQ中的一个重要部件，负责接收生产者发送的消息，并根据其类型（如direct、fanout、topic等）和配置的路由规则，将消息路由到一个或多个队列中。
4. **Queue（队列）**：队列是RabbitMQ中保存消息直到它们被消费者取走的数据结构。它是消息的容器，并遵循FIFO（先入先出）的原则。生产者发送的消息会进入队列，等待消费者进行消费。
5. **Consumer（消费者）**：消费者是等待接收并处理消息的程序。它从队列中取出消息，并进行相应的处理。
6. **Binding（绑定）**：也是RabbitMQ中的一个核心概念，它用于建立交换机和队列之间的关联，确定消息如何从交换机路由到队列。

## 3、工作原理

![RabbitMQ工作原理](../images/rabbitmq/RabbitMQ工作原理.png)

- **Broker：**接收和分发消息的应用，RabbitMQ Server 就是 Message Broker
- **Virtual host：**出于多租户和安全因素设计的，把 AMQP 的基本组件划分到一个虚拟的分组中，类似于网络中的 namespace 概念。当多个不同的用户使用同一个 RabbitMQ server 提供的服务时，可以划分出多个 vhost，每个用户在自己的 vhost 创建 exchange／queue 等
- **Connection：**publisher／consumer 和 broker 之间的 TCP 连接Channel：如果每一次访问 RabbitMQ 都建立一个Connection，在消息量大的时候建立 TCPConnection 的开销将是巨大的，效率也较低。Channel 是在 connection 内部建立的逻辑连接，如果应用程序支持多线程，通常每个 thread 创建单独的 channel 进行通讯，AMQP method 包含了 channel id 帮助客户端和 message broker 识别 channel，所以 channel 之间是完全隔离的。 l Channel  作为轻量级的Connection 极大减少了操作系统建立 TCP connection 的开销
- **Exchange ：** message 到达 broker 的第一站，根据分发规则，匹配查询表中的 routing key，分发消息到 queue 中去。常用的类型有：direct (point-to-point), topic (publish-subscribe) and fanout(multicast)
- Queue ： 消息最终被送到这里等待 consumer 取走
- **Binding ：** exchange 和 queue 之间的虚拟连接，binding 中可以包含 routing key，Binding 信息被保存到 exchange 中的查询表中，用于 message 的分发依据

**工作原理：**

1. 消息发布：
   - 生产者（Producer）连接到RabbitMQ服务器（Broker），并创建一个新的连接（Connection）和通道（Channel）。
   - 生产者通过通道将消息发送到交换机（Exchange）。在发送消息时，生产者可以指定一个路由键（Routing Key），该路由键用于决定消息应该路由到哪个队列。
2. 消息路由：
   - 交换机根据接收到的消息的路由键和自身的类型（如Direct、Fanout、Topic等），以及交换机与队列之间的绑定关系，将消息路由到一个或多个队列中。
   - 对于Direct类型的交换机，它会根据消息的路由键完全匹配一个队列的绑定键，然后将消息发送到该队列。
   - 对于Fanout类型的交换机，它会忽略消息的路由键，直接将消息广播到所有与之绑定的队列。
   - 对于Topic类型的交换机，它根据消息的路由键和队列的绑定键的模式匹配规则，将消息路由到匹配的队列。
3. 消息存储：
   - 消息被路由到队列后，会存储在队列中等待消费者进行消费。队列可以配置为持久化存储，以确保在服务器重启或故障时消息不会丢失。
4. 消息消费：
   - 消费者（Consumer）也连接到RabbitMQ服务器，并创建连接和通道。
   - 消费者通过通道订阅指定的队列，并等待接收队列中的消息。
   - 当队列中有消息时，RabbitMQ服务器会根据消费者的订阅情况，将消息推送给消费者。
   - 消费者接收到消息后，进行相应的处理逻辑。