# Kafka

## 1、概述

### 1.1、定义

​	Kafka是一个分布式的基于发布/订阅模式的消息队列，主要用于大数据实时处理领域。最新的定义为是一个开源的分布式事件流平台 ，用于数据管道、流分析、数据集成和关键任务的应用。

### 1.2、应用场景

​	**缓冲/消峰：**有助于控制和优化数据流经过系统的速度，解决生产消息和消费消息的处理速度不一致的情况。

​	**解耦：**允许独立扩展或修改两边的处理逻辑，只要确保他们遵守同样的接口约束。

​	**异步通信：**允许用户把消息放入队列，但不需要等待消息的处理完成。只需要在需要处理的时候去处理即可。

### 1.3、两种模式

​	**点对点：**消费者主动拉取数据，消息收到后清除消息。

​	**发布/订阅模式：**可以有多个topic主题；消费者消费数据后不会删除数据；每个消费者相互独立，都可以消费到数据。

### 1.4、基础架构

​	为了方便扩展，提交吞吐量，一个topic分为了多个partition；搭配分区（partition）设计提出了消费者组的概念，组内每个消费者并行消费；提高可用性，为每个partition增加若干副本；基本上都会搭配Zookeeper使用，zk中记录了谁是Leader，Kafka2.8.0后可以不采用ZK。

![基础架构](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E5%9F%BA%E7%A1%80%E6%9E%B6%E6%9E%84.png)

1. **Producer：**消息生产者，向Kafka broker发生消息的客户端。
2. **Consumer：**消息消费者，向Kafka broker取消息的客户端。
3. **Consumer Group（CG）：**消费者组，由多个Consumer组成。消费者组内每个消费者负责消费不同分区的数据，一个分区只能由组内一个消费者消费；消费者组内互不影响。所有消费者都属于某个消费者组，即消费者组是逻辑上的一个订阅者。
4. **Broker：**一台Kafka服务器就是一个broker。一个集群由多个broker组成。一个broker可以容纳多个topic。
5. **Topic：**可以理解为一个队列，生产者和消费者面向的都是一个topic
6. **Parition：**一个topic可以分为多个partition，每个partition是一个有序的队列。
7. **Replica：**副本。一个topic的每个分区都有若干个副本，一个leader和若干个follower。
8. **Leader：**每个分区多个副本的“领导”，生产者发送数据的对象，以及消费者消费数据的对象都是Leader。
9. **Follower：**每个分区多个副本中的“员工”，实时从Leader中同步数据，保持和Leader数据的同步。Leader出现故障时，某个follower会成为新的Leader。

## 2、生产者

### 2.1、工作原理

​	在消息发送的过程中，涉及到两个线程—main线程和sender线程。在main线程中创建了一个双端队列RecordAccumulator。main线程发送消息给RecordAccumulator，Sender线程不断从RecordAccumulator中拉取消息发送到Kafka Broker。

![发送流程](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E5%8F%91%E9%80%81%E6%B5%81%E7%A8%8B.png)

### 2.2、重要参数

| 参数名称                              | 描述                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| bootstrap.servers                     | 生产者连接集群所需的 broker 地址清单。例如<br/>hadoop102:9092,hadoop103:9092,hadoop104:9092，<br />可以设置 1 个或者多个，中间用逗号隔开。注意这里并非需要所有的 broker 地址，<br />因为生产者从给定的 broker里查找到其他 broker信息。 |
| key.serializer<br />value.serializer  | 指定发送消息的 key 和 value 的序列化类型。一定要写全类名。   |
| buffer.memory                         | RecordAccumulator 缓冲区总大小，**默认 32m**                 |
| batch.size                            | 缓冲区一批数据最大值，**默认 16k**。适当增加该值，可以提高吞吐量，但是如果该值设置太大，会导致数据传输延迟增加。 |
| linger.ms                             | 如果数据迟迟未达到 batch.size，sender 等待 linger.time之后就会发送数据。单位 ms，默认值是 0ms，表示没有延迟。生产环境建议该值大小为 5-100ms之间。 |
| acks                                  | 0：生产者发送过来的数据，不需要等数据落盘应答。<br/>1：生产者发送过来的数据，Leader收到数据后应答。<br/>-1（all）：生产者发送过来的数据，Leader+和 isr 队列里面的所有节点收齐数据后应答。默认值是-1，-1 和all是等价的。 |
| max.in.flight.requests.per.connection | 允许最多没有返回 ack 的次数，默认为 5，开启幂等性<br/>要保证该值是 1-5 的数字。 |
| retries                               | 当消息发送出现错误的时候，系统会重发消息。retries表示重试次数。默认是 int最大值，2147483647。如果设置了重试，还想保证消息的有序性，需要设置MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION=1否则在重试此失败消息的时候，其他的消息可能发送成功了 |
| retry.backoff.ms                      | 两次重试之间的时间间隔，默认是 100ms。                       |
| enable.idempotence                    | 是否开启幂等性，默认 true，开启幂等性。                      |
| compression.type                      | 生产者发送的所有数据的压缩方式。默认是 none，也就是不压缩。<br/>支持压缩类型：none、gzip、snappy、lz4 和 zstd。 |

### 2.3、分区好处

**便于合理使用存储资源**，每个partition在一个Broker上存储，可以把海量的数据按照分区切割成一块一块的数据存储在多台Broker上。合理控制分区的任务，可以实现负载均衡的效果。

**提高并行度：**生产者可以以分区为单位发送数据；消费者可以以分区为单位进行消费数据。

### 2.4、分区策略

1. **指定分区：**按照指定的分区将消息数据存储到分区里面。

2. **按照key的HashCode值%分区数：**将key的hash值与topic的partition数进行取余得到partition值；例如key1的hash值=5，key2 的hash值=6，topic 的partition数=2，那么key1对应的value1写入1号分区，key2对应的value2写入0号分区 。

3. **黏性分区：**既没有partition值又没有key值的情况下，Kafka采用Sticky Partition（ 黏性分区器 ），会随机选择一个分区 ，并尽可能一直使用该分区 ， 待该分区的batch已满或者已完成，Kafka再随机一个分区进行使用（ 和上一次的分区不同 ） 。例如：第一次随机选择0号分区 ，等0号分区当前批次满了（ 默认16k） 或者linger.ms 设置的时间到 ，Kafka 再随机一个分区进行使用（ 如果还是0 会继续随机 ） 。

4. **自定义分区器：**定义类实现 Partitioner 接口重写partition()方法。然后再配置里面将自定义分区器配置上去。

   ```java
   /**
   * 1. 实现接口 Partitioner
   * 2. 实现 3 个方法:partition,close,configure
   * 3. 编写 partition 方法,返回分区号
   */
   public class MyPartitioner implements Partitioner {
       /**
       * 返回信息对应的分区
       * @param topic 主题
       * @param key 消息的 key
       * @param keyBytes 消息的 key 序列化后的字节数组
       * @param value 消息的 value
       * @param valueBytes 消息的 value 序列化后的字节数组
       * @param cluster 集群元数据可以查看分区信息
       * @return 分区号
       */
   	@Override
   	public  int  partition(String  topic,  Object  key,  byte[] keyBytes, Object value, byte[] 		valueBytes, Cluster cluster) {
   	// 返回分区号
   	return 0;
   	}
   	// 关闭资源
   	@Override
   	public void close() {
   	}
   	// 配置方法
   	@Override
   	public void configure(Map<String, ?> configs) {
   	}
   }
   ```

### 2.5、提供吞吐量

​	提高生产者的吞吐量修改以下几个参数：

1. **batch.size：**批次大小，默认16k
2. **linger.ms：**等待时间，修改为5-100ms
3. **compression.type：**压缩snappy
4. **RecordAccumulator：**缓冲区大小，修改为64m

### 2.6、数据可靠性

ACK应答级别：

acks=0：生产者发送数据后就不管了，可靠性差，效率高。

acks=1：生产者发送过来数据Leader应答，可靠性中等，效率中等。一般用于普通日志传送可以接受部分丢失数据。

acks=-1：生产者发送过来数据Leader和ISR队列里面所有follower应答，可靠性高，效率低。一般用于数据比较重要不能丢失的情况。

Leader会维护一个动态的in-sync replica set（ISR），意为和Leader保持同步的Follower+Leader集合（Leader：0，isr：0,1,2）如果Follower超过了（replica.lag.time.max.ms）30s没有和Leader通信那么将会被踢出ISR。

如果分区副本设置为1个，或者ISR里应答的最小副本数量（ min.insync.replicas）设置1，和acks=1的效果是一样的也会丢失数据的风险。

> **数据完全可靠条件=ACK级别设置为-1+分区副本大于等于2+ISR里应答的最小副本数量大于等于2；**

### 2.7、幂等性

​	Producer无论想Broker发送多少次重复数据，Broker端都只会持久化一条，保证了不重复。

​	至少一次：ACK级别设置为-1 + 分区副本大于等于2 + ISR里应答的最小副本数量大于等于2

​	最多一次： ACK级别设置为0

​	精确一次：  幂等性 +  至少一次（ ack=-1 +  分区副本数>=2 + ISR 最小副本数量>=2）

​	重复数据的判断标准：具有<PID, Partition, SeqNumber>相同主键的消息提交时，Broker只会持久化一条。其
中**PID是Kafka每次重启都会分配一个新的；Partition 表示分区号；Sequence Number是单调自增的**。所以幂等性**只能保证的是在单分区单会话内不重复**。

enable.idempotence开启幂等性，默认是开启true

### 2.8、事务

![事务原理](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E4%BA%8B%E5%8A%A1.png)

## 3、Broker

### 3.1、工作原理

![Broker工作原理](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%20Broker%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86.png)

Broker在启动的时候会在zk中注册，谁先注册controller就谁说了算，controller可以监听Broker节点和决定Leader的选举，并且将Broker节点信息和Leader信息上传到zk进行存储。

**Leader的选举规则：首先得在ISR里面存活，按照AR（分区中所有副本统称）的排序优先。**

### 3.2、分区副本

#### 3.2.1、副本

1. Kafka副本的作用是提高数据可靠性；
2. Kafka默认副本为1个，生产环境一般配置为2个，保证数据可靠性，副本太多会消耗更多的磁盘空间并且还是增加网络数据传送，降低了效率；
3. Kafka中副本分为：Leader和Follower，Kafka只会把数据发给Leader，然后follower找Leader进行同步。
4. Kafka分区中的所有副本成为AR(ISR+OSR)，ISR标识和Leader保持同步的Follower集合，如果Follower长时间未向Leader发送通信请求或同步数据，则该Follower将会被踢出ISR，该时间的阈值有 replica.lag.time.max.ms设定，默认30s。Leader发生故障时就会从ISR中选举新的Leader。OSR标识Follower与Leader同步是，延迟过多的副本。

#### 2.2.2、Leader选举流程

​	Kafka集群中一个Broker的controller会被选举为Controller Leader，负责管理集群Broker的上下线，所有topic的分区副本分配和Leader选举等工作。Controller信息同步工作时依赖于Zookeeper。

![选举流程](https://gitee.com/WilliamWangmy/snail-image/raw/77fc6ca5a006dd70547d49021d79e4e54a50a05d/kafka/Kafka%20Leader%E9%80%89%E4%B8%BE%E6%B5%81%E7%A8%8B.png)

#### 2.2.3、Leader和Follower故障处理

**LEO（Log End Offset）**：每个副本的最后一个offset，LEO其实就是最新的offset + 1。

**HW（High Watermark）**：所有副本中最小的LEO 。

- Follower故障

  1. Follower发生故障后会被临时踢出ISR
  2. 被踢出后这个期间Leader和Follower继续接收数据
  3. 待该Follower恢复后，Follower会读取本地磁盘记录的上次的HW，并将log文件高于HW的部分截取掉，从HW开始向Leader同步。
  4. 等该Follower的LEO大于等于该partition的HW，即Follower追上Leader之后就可以重新加入ISR。

- Leader故障

  1. Leader发生故障后，会从ISR中选出一个新的Leader

  2. 为保证多个副本之间的数据一致性，其余的Follower会先将各自的log文件高于HW的部分截取掉，然后从新的leader同步数据。

     这只能保证数据的一致性不能保证数据的丢失或者不重复。

#### 2.2.4、分区副本分配

Kafka会根据自己的规则创建对应的分区副本，但是由于每台服务器的性能配置存在不一致的情况，就会导致个别服务器存储压力较大。所以需要手动调整分区副本的存储。

- Leader Partition自动平衡

  正常情况下，Kafka本身会自动把Leader Partition均匀分散在各个机器上，来保证每台机器的读写吞吐量都是均匀的。但是如果某些Broker宕机，会导致Leader Partition过于集中在其他少部分的几台Broker上，这会导致少数几台Broker的读写请求压力过高，其他宕机的Broker重启之后都是Follower Partition读写请求低，造成集群负载不均衡。

  ![自动平衡](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%20Leader%20Partition%E8%87%AA%E5%8A%A8%E5%B9%B3%E8%A1%A1.png)

### 3.3、重要参数

| 参数名称                                | 描述                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| replica.lag.time.max.ms                 | ISR 中，如果 Follower 长时间未向 Leader 发送通信请求或同步数据，则该 Follower 将被踢出 ISR。该时间阈值，默认 30s。 |
| auto.leader.rebalance.enable            | 默认是 true。 自动 Leader Partition 平衡。                   |
| leader.imbalance.per.broker.percentage  | 默认是 10%。每个 broker允许的不平衡的 leader的比率。如果每个 broker 超过了这个值，控制器会触发 leader的平衡。 |
| leader.imbalance.check.interval.seconds | 默认值 300 秒。检查 leader 负载是否平衡的间隔时间。          |
| log.segment.bytes                       | Kafka 中 log 日志是分成一块块存储的，此配置是指 log日志划分 成块的大小，默认值 1G。 |
| log.index.interval.bytes                | 默认 4kb，kafka 里面每当写入了 4kb 大小的日志（.log），然后就往 index文件里面记录一个索引。 |
| log.retention.hours                     | Kafka 中数据保存的时间，默认 7 天。                          |
| log.retention.minutes                   | Kafka 中数据保存的时间，分钟级别，默认关闭。                 |
| log.retention.ms                        | Kafka 中数据保存的时间，毫秒级别，默认关闭。                 |
| log.retention.check.interval.ms         | 检查数据是否保存超时的间隔，默认是 5 分钟。                  |
| log.retention.bytes                     | 默认等于-1，表示无穷大。超过设置的所有日志总大小，删除最早的 segment。 |
| log.cleanup.policy                      | 默认是 delete，表示所有数据启用删除策略；如果设置值为 compact，表示所有数据启用压缩策略。 |
| num.io.threads                          | 默认是 8。负责写磁盘的线程数。整个参数值要占总核数的 50%。   |
| num.replica.fetchers                    | 副本拉取线程数，这个参数占总核数的 50%的 1/3                 |
| num.network.threads                     | 默认是 3。数据传输线程数，这个参数占总核数的50%的 2/3 。     |
| log.flush.interval.messages             | 强制页缓存刷写到磁盘的条数，默认是 long 的最大值，9223372036854775807。一般不建议修改，交给系统自己管理。 |
| log.flush.interval.ms                   | 每隔多久，刷数据到磁盘，默认是 null。一般不建议修改，交给系统自己管理。 |

### 3.4、文件存储

#### 3.4.1、文件存储机制

​	![文件存储机制](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%96%87%E4%BB%B6%E5%AD%98%E5%82%A8%E6%9C%BA%E5%88%B6.png)

#### 3.4.2、如何查找数据

![定位数据](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%9F%A5%E6%89%BElog%E4%B8%AD%E7%9A%84%E6%95%B0%E6%8D%AE.png)

#### 3.4.3、文件清理策略

Kafka 中默认的日志保存时间为 7 天，可以通过调整如下参数修改保存时间。

- log.retention.hours，最低优先级小时，默认 7天。
- log.retention.minutes，分钟。
- log.retention.ms，最高优先级毫秒。
- log.retention.check.interval.ms，负责设置检查周期，默认 5 分钟。

日志一旦超过了设置的时间，Kafka 中提供的日志清理策略有 delete 和 compact两种。

1. delete日志删除：将过期数据删除
   -  log.cleanup.policy = delete 所有数据启用删除策略
     
     （1）基于时间：默认打开。以 segment 中所有记录中的最大时间戳作为该文件时间戳。
     

  （2）基于大小：默认关闭。超过设置的所有日志总大小，删除最早的 segment。
     
   - log.retention.bytes，默认等于-1，表示无穷大。

2. ompact日志压缩：对于相同key的不同value值，只保留最后一个版本。这种情况只适合于key为主键信息，可以保留最新的数据。

### 3.5、高效读写

1. Kafka本身是分布式集群，可以采用分区技术，并行度高
2. 读写数据采用稀疏索引，可以快速定位要消费的数据
3. 顺序写磁盘。写入log文件是一直宅文件末端进行追加，而不是随机的。

#### 3.5.1、页缓存和零拷贝

零拷贝：Kafka的数据加工处理操作由Kafka消费者处理。Kafka Broker应用层不关心存储的数据，所以就不走应用层，传输效率高。

页缓存：Kafka重度依赖底层操作系统提供的PageCache功能，当上层有写操作时，操作系统只是将数据写入PageCache，当读操作发生时，先从PageCache中查找，如果找不到再去磁盘读取，实际上PageCache是把尽可能多的空间内存当做了磁盘缓存来使用。

## 4、消费者

### 4.1、总体工作流程

![总体流程](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E6%80%BB%E4%BD%93%E6%B5%81%E7%A8%8B.png)

### 4.2、消费者组工作流程

- 初始化流程

![初始化流程](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E7%BB%84%E5%88%9D%E5%A7%8B%E5%8C%96%E6%B5%81%E7%A8%8B.png)

- 消费流程

![消费流程](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E7%BB%84%E6%B6%88%E8%B4%B9%E6%B5%81%E7%A8%8B.png)

### 4.3、重要参数

| 参数名称                                | 描述                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| bootstrap.servers                       | 向 Kafka集群建立初始连接用到的 host/port列表。               |
| key.deserializer<br/>value.deserializer | 指定接收消息的 key 和 value 的反序列化类型。一定要写全类名。 |
| group.id                                | 标记消费者所属的消费者组。                                   |
| enable.auto.commit                      | 默认值为 true，消费者会自动周期性地向服务器提交偏移量。      |
| auto.commit.interval.ms                 | 如果设置了 enable.auto.commit 的值为 true， 则该值定义了消费者偏移量向 Kafka提交的频率，默认 5s。 |
| auto.offset.reset                       | 当 Kafka 中没有初始偏移量或当前偏移量在服务器中不存在（如，数据被删除了），该如何处理？ earliest：自动重置偏移量到最早的偏移量。 latest：默认，自动重置偏移量为最新的偏移量。 none：如果消费组原来的（previous）偏移量不存在，则向消费者抛异常。 anything：向消费者抛异常。 |
| offsets.topic.num.partitions            | __consumer_offsets 的分区数，默认是 50 个分区。              |
| heartbeat.interval.ms                   | Kafka 消费者和 coordinator 之间的心跳时间，默认 3s。该条目的值必须小于 session.timeout.ms ，也不应该高于session.timeout.ms 的 1/3。 |
| session.timeout.ms                      | Kafka 消费者和 coordinator 之间连接超时时间，默认 45s。超过该值，该消费者被移除，消费者组执行再平衡。 |
| max.poll.interval.ms                    | 消费者处理消息的最大时长，默认是 5 分钟。超过该值，该消费者被移除，消费者组执行再平衡。 |
| fetch.min.bytes                         | 默认 1 个字节。消费者获取服务器端一批消息最小的字节数。      |
| fetch.max.wait.ms                       | 默认 500ms。如果没有从服务器端获取到一批数据的最小字节数。该时间到，仍然会返回数据。 |
| fetch.max.bytes                         | 默认 Default: 52428800（50 m）。消费者获取服务器端一批消息最大的字节数。如果服务器端一批次的数据大于该值（50m）仍然可以拉取回来这批数据，因此，这不是一个绝对最大值。一批次的大小受 message.max.bytes （brokerconfig）or max.message.bytes （topic config）影响。 |
| max.poll.records                        | 一次 poll拉取数据返回消息的最大条数，默认是 500 条。         |
| partition.assignment.strategy           | 消费者分区分配策略，默认策略是Range +CooperativeSticky。Kafka 可以同时使用多个分区分配策略。可 以 选 择 的 策 略 包 括 ： Range 、 RoundRobin 、 Sticky 、CooperativeSticky |

### 4.4、分区分配及再平衡

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E5%88%86%E5%8C%BA%E5%88%86%E9%85%8D%E5%8F%8A%E5%86%8D%E5%B9%B3%E8%A1%A1.png)

#### 4.4.1、Range+CooperativeSticky

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%20Range%E5%88%86%E5%8C%BA%E5%88%86%E9%85%8D%E7%AD%96%E7%95%A5.png)

如果消费者0宕机被踢出消费者组，会按照range方式重新分配。

#### 4.4.2、RoundRobin

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%20RoundRobin%E5%88%86%E5%8C%BA%E5%88%86%E9%85%8D%E7%AD%96%E7%95%A5.png)

如果消费者0宕机被踢出消费者组，会按照RoundRobin方式重新分配。

#### 4.4.3、Sticky

​	分配结果带有“粘性”，即在执行一次新的分配之前会考虑上一次分配的结果，尽量少的调整分配变动，可以节省大量的开销。kafk在分配时首先会尽量保持均衡分配分区到消费者上面，在出现同一消费组内消费者出现问题的时候，会尽量保持原有分配分区不变化。

​	比如一号消费者分配0、1分区；二号消费者分配2、3、5分区；三号消费分配4、6分区；当一号消费者出现问题后，会将0、1号分区分别分配到二号和三号上面，原来二号和三号分配的分区在重新分配的时候不会变。

### 4.5、offset位移

offset是记录消费者（Consumer）消费到那个位置的一个标识。并且保存在Kafka一个内置的topic中，该topic为_consumer_offsets。__consumer_offsets 主题里面采用 key 和 value 的方式存储数据。key 是 group.id+topic+
分区号，value 就是当前 offset 的值。每隔一段时间，kafka 内部会对这个 topic 进行compact，也就是每个 group.id+topic+分区号就保留最新数据。

#### 4.5.1、自动提交offset

​	Kafka提供了自动提交offset的功能：

- enable.auto.commit ：是否开启自动提交offset功能，默认是true
- auto.commit.interval.ms ：自动提交offset的时间间隔，默认是5s

#### 4.5.2、手动提交offset

​	Kafka提供了两种手动提交方式分别是commitSync（同步提交）和commitAsync（异步提交）。两者的相同点是，都会将本次提交的一批数据最高的偏移量提交；不同点是，同步提交阻塞当前线程，一直到提交成功，并且会自动失败重试（由不可控因素导致，也会出现提交失败）；而异步提交则没有失败重试机制，故有可能提交失败。

- commitSync（同步提交）：必须等待offset提交完毕，再去消费下一批数据。
- commitAsync（异步提交） ：发送完提交offset请求后，就开始消费下一批数据了。

#### 4.5.3、指定offset消费

auto.offset.reset = earliest | latest | none 默认是 latest。

- earliest：自动将偏移量重置为最早的偏移量，--from-beginning。
- latest（默认值）：自动将偏移量重置为最新偏移量。
- none：如果未找到消费者组的先前偏移量，则向消费者抛出异常。
- 可以指定offset位移开始消费，比如可以指定offset从600位置开始消费

#### 4.5.4、指定时间消费

​	在消费时可以指定消费者从什么时间开始消费。

#### 4.5.5、重复消费与漏消费



解决重复消费与漏消费的方式就利用消费者事务。

### 4.6、消费者事务



### 4.7、数据积压

数据积压只有提交消费者的吞吐量。

## 5、源码分析

### 5.1、main线程初始化

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E7%94%9F%E4%BA%A7%E8%80%85main%E7%BA%BF%E7%A8%8B%E5%88%9D%E5%A7%8B%E5%8C%96.png)

### 5.2、Sender线程初始化

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E7%94%9F%E4%BA%A7%E8%80%85Sender%E7%BA%BF%E7%A8%8B%E5%88%9D%E5%A7%8B%E5%8C%96.png)

### 5.3、发送数据到缓冲区

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E5%8F%91%E9%80%81%E6%95%B0%E6%8D%AE%E5%88%B0%E7%BC%93%E5%86%B2%E5%8C%BA.png)

### 5.4、Sender线程发送数据

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%20Sender%E7%BA%BF%E7%A8%8B%E5%8F%91%E9%80%81%E6%95%B0%E6%8D%AE.png)

### 5.5、消费者初始化

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E5%88%9D%E5%A7%8B%E5%8C%96.png)

### 5.6、消费者订阅主题

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E8%AE%A2%E9%98%85%E4%B8%BB%E9%A2%98.png)

### 5.7、消费者拉取处理数据

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/kafka/Kafka%E6%B6%88%E8%B4%B9%E8%80%85%E6%8B%89%E5%8F%96%E5%92%8C%E5%A4%84%E7%90%86%E6%95%B0%E6%8D%AE.png)



