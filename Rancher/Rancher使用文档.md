# 1、安装Rancher

## 1.1、建立映射文件夹

​		为了防止Rancher所在服务器重启后数据丢失，那么就需要将Rancher的数据实例化到磁盘上，所以要建立对应的映射文件夹。

```shell
[root@node10 mydata]# mkdir -p /mydata/rancher/{data,log}
```

## 1.2、Docker启动Rancher

```shell
[root@node10 mydata]# docker run -d --restart=unless-stopped \
  -p 8080:80 -p 8443:443 \
  -v /mydata/rancher/data:/var/lib/rancher \
  -v /mydata/rancher/log:/var/lib/auditlog \
  --privileged \
  rancher/rancher:latest
```

## 1.3、访问Rancher

​		启动成功后在浏览器页面访问地址：http://192.168.56.10:8080。这里的ip换成自己服务器的ip。

![Rancher登录界面](../images/rancher/Rancher登录界面.png)

- 根据页面提示找到容器默认的密码    docker logs  容器id  2>&1 | grep "Bootstrap Password:"
- 设置自己的登录密码,至少12个字符

![Rancher设置登录密码](../images/rancher/Rancher设置登录密码.png)

- 登录成功后就会进入主界面

![Rancher主界面](../images/rancher/Rancher主界面.png)

# 2、NFS

​		NFS（Network File System）即网络文件系统，它允许网络中的计算机之间通过网络共享资源。将NFS主机分享的目录，挂载到本地客户端当中，本地NFS的客户端应用可以透明地读写位于远端NFS服务器上的文件，在客户端端看起来，就像访问本地文件一样。

​		在用Rancher容器化部署的时候需要将数据实例化到磁盘上防止数据丢失。

## 2.1、准备NFS服务器

​		防止Mysql数据丢失那么就需要将数据持久化，需要单独准备一台独立的NFS服务器。不在Rancher集群机器里面。

## 2.2、安装nfs

```shell
# 安装nfs
[root@localhost ~]# yum install -y nfs-utils
# 建立挂在目录
[root@localhost mydata]# mkdir nfs
# 设置挂载路径
[root@localhost ~]# vi /etc/exports

#将以下内容配置到/etc/exports, /mydata/nfs就是前面建立的挂在目录
/mydata/nfs  *(rw,no_root_squash)

#执行下面命令不报错即可
[root@localhost nfs]# exportfs -r
#创建mysql的挂在目录，我挂在的目录是/mydata/nfs/mysql
[root@localhost nfs]# mkdir mysql
#设置开机启动
[root@k8s-node2 nfs]# chkconfig nfs on
```

## 2.3、启动NFS服务

```shell
#启动nfs
[root@localhost mysql]# systemctl start nfs
#检查NFS是否启动
[root@localhost mysql]# ps -ef | grep nfs
root     12625     2  0 13:18 ?        00:00:00 [nfsd4_callbacks]
root     12631     2  0 13:18 ?        00:00:00 [nfsd]
root     12632     2  0 13:18 ?        00:00:00 [nfsd]
root     12633     2  0 13:18 ?        00:00:00 [nfsd]
root     12634     2  0 13:18 ?        00:00:00 [nfsd]
root     12635     2  0 13:18 ?        00:00:00 [nfsd]
root     12636     2  0 13:18 ?        00:00:00 [nfsd]
root     12637     2  0 13:18 ?        00:00:00 [nfsd]
root     12638     2  0 13:18 ?        00:00:00 [nfsd]
root     12650  5180  0 13:18 pts/0    00:00:00 grep --color=auto nfs

```

## 2.4、集群节点安装NFS

```shell
# 安装nfs
[root@localhost ~]# yum install -y nfs-utils
```

# 3、集群

​		Rancher是管理容器集群化，所以需要建立一个集群在负载容器化的运行。

## 3.1、创建集群

![创建集群1](../images/rancher/创建集群1.png)

![创建集群2](../images/rancher/创建集群2.png)

![创建集群3](../images/rancher/创建集群3.png)

![创建集群4](../images/rancher/创建集群4.png)

![创建集群5](../images/rancher/创建集群5.png)

![创建集群6](../images/rancher/创建集群6.png)

![创建集群7](../images/rancher/创建集群7.png)

安装集群可能会出现一些问题，请参考：https://www.bookstack.cn/read/rancher-2.5.11-zh/10e38c8aca9911df.md

## 3.2、添加集群节点

### ![image-20240814093610901](../images/rancher/添加集群节点)

# 4、Rancher部署Mysql

## 4.1、创建PV

![创建pv1](../images/rancher/创建pv1.png)

## 4.2、创建PVC

![创建pvc1](../images/rancher/创建pvc1.png)

这里访问模式要跟PV的访问模式一样，不然PVC启动不起来

![创建pvc2](../images/rancher/创建pvc2.png)

![创建pvc3](../images/rancher/创建pvc3.png)

## 4.3、创建服务

![创建服务1](../images/rancher/创建服务1.png)

![创建服务2](../images/rancher/创建服务2.png)

![创建服务3](../images/rancher/创建服务3.png)

![创建服务4](../images/rancher/创建服务4.png)

## 4.4、部署Mysql

这里挂在了磁盘路径所以对应的mysql配置文件夹及文件也需要创建

```shell
# logs为日志目录
# conf为my.cnf配置文件目录
# data为数据文件存放路径
[root@k8s-node2 conf]# mkdir -p /mydata/nfs/mysql/{logs,conf,data}

[root@k8s-node2 conf]# vi my.cnf 
[mysqld]
#mysql8.0部署配置文件
user=mysql
character-set-server=utf8
default_authentication_plugin=mysql_native_password
secure_file_priv=/var/lib/mysql
expire_logs_days=7
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,ONLY_FULL_GROUP_BY
max_connections=5000
#skip-grant-tables
skip-name-resolve
lower_case_table_names=1
[client]
default-character-set=utf8
#innodb_directories=”/data/mysqldata;”
```

- Docker启动Mysql命令

```shell
docker run -p 3306:3306 -d --name mysql-snail  \
--privileged=true   \
-v /mydata/nfs/mysql/data:/var/lib/mysql \
-v /mydata/nfs/mysql/conf:/etc/mysql \
-v /mydata/nfs/mysql/logs:/var/log/mysql  \
-v /etc/localtime:/etc/localtime  \
-e MYSQL_ROOT_PASSWORD=123456  \
mysql:8.0.25

参数说明： 
-p 代表端口映射（-p 3306:3306表示将容器3306端口映射到主机3306端口）
-d 在后台运行容器，并且打印容器id
--name 标识容器名称
-v 给容器挂载存储卷，挂载到容器的某个目录（目录映射）
-privileged=true 表示容器具有主机权限
-e 表示配置环境变量
```

注意：挂在持久化的服务器上的NFS要启动后，Mysql  Pod才能顺利启动。

![部署Mysql1](../images/rancher/部署Mysql1.png)

![部署Mysql2](../images/rancher/部署Mysql2.png)

![部署Mysql3](../images/rancher/部署Mysql3.png)

![部署Mysql4](../images/rancher/部署Mysql4.png)

![部署Mysql5](../images/rancher/部署Mysql5.png)

![部署Mysql6](../images/rancher/部署Mysql6.png)

## 4.5、测试Mysql

我用的idea自带数据库连接工具进行测试，也可以用其他连接工具进行测试。

![Mysql测试连接](../images/rancher/Mysql测试连接.png)

# 5、Rancher部署Nacos

跟部署Mysql基本相似，Nacos持久化到数据库中的，不需要持久化到磁盘上所以不需要配置NFS。

## 5.1、创建服务

![部署Nacos1](../images/rancher/部署Nacos1.png)

## 5.2、新建Nacos配置库

在上面部署的mysql里面，新建一个Nacos的配置数据库snail-config，然后将Nacos的相关建表语句执行。

```sql
/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = config_info   */
/******************************************/
CREATE TABLE `config_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) DEFAULT NULL,
  `content` longtext NOT NULL COMMENT 'content',
  `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COMMENT 'source user',
  `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) DEFAULT NULL,
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) DEFAULT NULL,
  `c_use` varchar(64) DEFAULT NULL,
  `effect` varchar(64) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `c_schema` text,
  `encrypted_data_key` text NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfo_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info';

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = config_info_aggr   */
/******************************************/
CREATE TABLE `config_info_aggr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) NOT NULL COMMENT 'datum_id',
  `content` longtext NOT NULL COMMENT '内容',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) DEFAULT NULL,
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfoaggr_datagrouptenantdatum` (`data_id`,`group_id`,`tenant_id`,`datum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='增加租户字段';


/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = config_info_beta   */
/******************************************/
CREATE TABLE `config_info_beta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
  `content` longtext NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COMMENT 'source user',
  `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfobeta_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_beta';

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = config_info_tag   */
/******************************************/
CREATE TABLE `config_info_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
  `content` longtext NOT NULL COMMENT 'content',
  `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COMMENT 'source user',
  `src_ip` varchar(50) DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfotag_datagrouptenanttag` (`data_id`,`group_id`,`tenant_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_tag';

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = config_tags_relation   */
/******************************************/
CREATE TABLE `config_tags_relation` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`),
  UNIQUE KEY `uk_configtagrelation_configidtag` (`id`,`tag_name`,`tag_type`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_tag_relation';

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = group_capacity   */
/******************************************/
CREATE TABLE `group_capacity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
  `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
  `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='集群、各Group容量信息表';

/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = his_config_info   */
/******************************************/
CREATE TABLE `his_config_info` (
  `id` bigint(64) unsigned NOT NULL,
  `nid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) NOT NULL,
  `group_id` varchar(128) NOT NULL,
  `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
  `content` longtext NOT NULL,
  `md5` varchar(32) DEFAULT NULL,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `src_user` text,
  `src_ip` varchar(50) DEFAULT NULL,
  `op_type` char(10) DEFAULT NULL,
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`nid`),
  KEY `idx_gmt_create` (`gmt_create`),
  KEY `idx_gmt_modified` (`gmt_modified`),
  KEY `idx_did` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='多租户改造';


/******************************************/
/*   数据库全名 = nacos_config   */
/*   表名称 = tenant_capacity   */
/******************************************/
CREATE TABLE `tenant_capacity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
  `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
  `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='租户容量信息表';


CREATE TABLE `tenant_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) default '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) default '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info';

CREATE TABLE `users` (
	`username` varchar(50) NOT NULL PRIMARY KEY,
	`password` varchar(500) NOT NULL,
	`enabled` boolean NOT NULL
);

CREATE TABLE `roles` (
	`username` varchar(50) NOT NULL,
	`role` varchar(50) NOT NULL,
	UNIQUE INDEX `idx_user_role` (`username` ASC, `role` ASC) USING BTREE
);

CREATE TABLE `permissions` (
    `role` varchar(50) NOT NULL,
    `resource` varchar(255) NOT NULL,
    `action` varchar(8) NOT NULL,
    UNIQUE INDEX `uk_role_permission` (`role`,`resource`,`action`) USING BTREE
);

INSERT INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE);

INSERT INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN');

```

## 5.2、部署Nacos

docker启动的命令，相关配置参数

```shell
docker run --name nacos-snail -d -p 8848:8848 -p 9848:9848 -p 9948:9948 --privileged=true --restart=always \
-e JVM_XMS=256m  \
-e JVM_XMX=256m  \
-e MODE=standalone  \
-e PREFER_HOST_MODE=hostname  \
-e SPRING_DATASOURCE_PLATFORM=mysql  \
-e MYSQL_SERVICE_HOST=192.168.56.11  \
-e MYSQL_SERVICE_PORT=3306  \
-e MYSQL_SERVICE_USER=root  \
-e MYSQL_SERVICE_PASSWORD=123456  \
-e MYSQL_SERVICE_DB_NAME=snail-config  \
nacos/nacos-server:v2.1.0 
```

![部署Nacos2](../images/rancher/部署Nacos2.png)

![部署Nacos3](../images/rancher/部署Nacos3.png)

![部署Nacos4](../images/rancher/部署Nacos4.png)

![部署Nacos5](../images/rancher/部署Nacos5.png)

![部署Nacos6](../images/rancher/部署Nacos6.png)

## 5.3、测试Nacos

登录账号nacos，密码nacos

![部署Nacos6](../images/rancher/部署Nacos7.png)

# 6、Rancher部署Redis

## 6.1、创建PV

​		由于Redis数据是存在内存中，服务重启后数据会丢失所以需要将数据持久化到磁盘上。这里也采用的是NFS文件目录共享方式。

![Redis部署创建pv1](../images/rancher/Redis部署创建pv1.png)

![Redis部署创建pv2](../images/rancher/Redis部署创建pv2.png)

![Redis部署创建pv3](../images/rancher/Redis部署创建pv3.png)

## 6.2、创建PVC

![Redis部署创建pvc1](../images/rancher/Redis部署创建pvc1.png)

![Redis部署创建pvc2](../images/rancher/Redis部署创建pvc2.png)

![Redis部署创建pvc3](../images/rancher/Redis部署创建pvc3.png)

## 6.3、创建服务

![Redis部署创建svc1](../images/rancher/Redis部署创建svc1.png)

![Redis部署创建svc2](../images/rancher/Redis部署创建svc2.png)

![Redis部署创建svc3](../images/rancher/Redis部署创建svc3.png)

## 6.4、部署Redis

- 需要再nfs挂在的目录下建立data和conf目录：

```shell
[root@localhost ~]# mkdir -p /mydata/nfs/redis/{conf,data}
```

- 将redis的配置文件配置到/mydata/nfs/redis/conf/redis.conf

  ```shell
  [root@localhost conf]# vi /mydata/nfs/redis/conf/redis.conf
  # 设置 redis 连接密码
  requirepass 123456
  
  daemonize no
  #端口
  port 6379
  protected-mode no
  save ""
  cluster-enabled no
  cluster-config-file nodes.conf
  cluster-node-timeout 5000
  bind 0.0.0.0
  
  protected-mode no
  # 数据持久化 - 开始
  # 开启 AOF 持久化
  appendonly yes
  
  # AOF文件刷新的方式
  # always 每提交一个修改命令都调用fsync刷新到AOF文件，非常非常慢，但也非常安全。
  # everysec 每秒钟都调用fsync刷新到AOF文件，很快，但可能会丢失一秒以内的数据。
  # no 依靠OS进行刷新，redis不主动刷新AOF，这样最快，但安全性就差。
  appendfsync everysec
  
  # 随着持久化的不断增多，AOF文件会越来越大，这个时候就需要AOF文件重写了。AOF文件重写
  # 如果该参数取值为yes，那么在重写AOF文件时能提升性能，但可能在重写AOF文件时丢失数据。
  # 如果取值为no，则不会丢失数据，但较取值为yes的性能可能会降低。默认取值是no。
  no-appendfsync-on-rewrite no
  
  # AOF文件重写
  # 参数能指定重写的条件，默认是100，
  # 即如果当前的AOF文件比上次执行重写时的文件大一倍时会再次触发重写操作。
  # 如果该参数取值为0，则不会触发重写操作。
  auto-aof-rewrite-percentage 100
  
  # AOF文件重写
  # 指定触发重写时AOF文件的大小，默认是64MB。
  auto-aof-rewrite-min-size 64mb
  
  # auto-aof-rewrite-percentage 和 auto-aof-rewrite-min-size 两个参数指定的重写条件是“And”的关系。
  # 即只有当同时满足这两个条件时才会触发重写操作。
  
  # 数据持久化 - 结束
  
  # 绑定redis服务器网卡IP，默认为127.0.0.1,即本地回环地址。
  # 这样的话，访问redis服务只能通过本机的客户端连接，而无法通过远程连接。
  # 如果bind选项为空的话，那会接受所有来自于可用网络接口的连接。
  bind 127.0.0.1
  
  # Redis key 过期事件监听
  notify-keyspace-events Ex
  # 内存不够后的淘汰策略
  maxmemory 6442450944
  maxmemory-policy allkeys-lru
  ```

```shell
docker run -itd  -p 6379:6379  \
-v /mydata/redis/conf/redis.conf:/usr/local/etc/redis/redis.conf   \
-v /mydata/redis/data/:/data   \
--name redis-snail   \
redis   \
redis-server /usr/local/etc/redis/redis.conf
```

需要配置命令来启用挂在的配置文件：redis-server /usr/local/etc/redis/redis.conf

![Redis部署创建pod1](../images/rancher/Redis部署创建pod1.png)

![Redis部署创建pod2](../images/rancher/Redis部署创建pod2.png)

![Redis部署创建pod3](../images/rancher/Redis部署创建pod3.png)

![Redis部署创建pod4](../images/rancher/Redis部署创建pod4.png)

## 6.5、测试Redis

![Redis部署测试](../images/rancher/Redis部署测试.png)

# 7、Rancher部署Nginx

## 7.1、创建PV

​		由于Nginx需要挂载目录来映射静态文件，部署前端项目。

```shell
[root@k8s-node2 nginx]# mkdir /mydata/nfs/nginx/{html,conf,logs}
```

![Nginx部署创建pv1](../images/rancher/Nginx部署创建pv1.png)

![Nginx部署创建pv2](../images/rancher/Nginx部署创建pv2.png)

![Nginx部署创建pv3](../images/rancher/Nginx部署创建pv3.png)

## 7.2、创建PVC

![Nginx部署创建pvc1](../images/rancher/Nginx部署创建pvc1.png)

![Nginx部署创建pvc2](../images/rancher/Nginx部署创建pvc2.png)

![Nginx部署创建pvc3](../images/rancher/Nginx部署创建pvc3.png)

## 7.3、创建服务

![Redis部署创建svc1](../images/rancher/Redis部署创建svc1.png)

![Redis部署创建svc2](../images/rancher/Redis部署创建svc2.png)

## 7.4、部署Nginx

Nginx部署前需要将Nginx默认的conf文件夹拷贝到挂载目录conf下。conf文件夹在当前目录的Nginx下面。

![Nginx部署pod1](../images/rancher/Nginx部署pod1.png)

![Nginx部署pod2](../images/rancher/Nginx部署pod2.png)

![Nginx部署pod3](../images/rancher/Nginx部署pod3.png)

![Nginx部署pod4](../images/rancher/Nginx部署pod4.png)

![Nginx部署pod5](../images/rancher/Nginx部署pod5.png)

![Nginx部署pod6](../images/rancher/Nginx部署pod6.png)

## 7.5、测试Nginx

出现如下画面就表示部署成功。

![Nginx部署测试](../images/rancher/Nginx部署测试.png)

# 8、Rancher配置Harbor

​		由于业务需要镜像文件不能上传到网上，所以需要自己搭建Harbor镜像仓库，将业务镜像推送到Harbor，然后Rancher部署业务镜像时从Harbor仓库中拉取镜像部署。

## 8.1、安装Harbor

安装Harbor的教程：[https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Docker/%E5%AE%89%E8%A3%85Harbor.md](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Docker/%E5%AE%89%E8%A3%85Harbor.md)

## 8.2、配置Harbor

![Harbor仓库配置1](../images/rancher/Harbor仓库配置1.png)

![Harbor仓库配置2](../images/rancher/Harbor仓库配置2.png)

![Harbor仓库配置3](../images/rancher/Harbor仓库配置3.png)

## 8.3、部署业务镜像

### 8.3.1、创建服务

![业务镜像部署svc1](../images/rancher/业务镜像部署svc1.png)

![业务镜像部署svc2](../images/rancher/业务镜像部署svc2.png)

![业务镜像部署svc3](../images/rancher/业务镜像部署svc3.png)

### 8.3.2、部署业务镜像

![业务镜像部署1](../images/rancher/业务镜像部署1.png)

![业务镜像部署2](../images/rancher/业务镜像部署2.png)

![业务镜像部署3](../images/rancher/业务镜像部署3.png)

![业务镜像部署4](../images/rancher/业务镜像部署4.png)

### 8.3.3、测试镜像

![业务镜像部署5](../images/rancher/业务镜像部署5.png)

# 9、Rancher部署Minio

​		MinIO是一款高性能、分布式的对象存储系统，它基于 Go 语言开发，并实现了与云存储服务兼容的 API。MinIO 适用于存储大容量非结构化的数据，如图片、视频、日志文件、备份数据和容器/虚拟机镜像等。它的对象文件可以是任意大小，从几 KB 到最大 5TB 不等。

## 9.1、创建PV

​		由于minio管理的文件，所以文件需要存放到服务器，因此需要NFS来挂载共享目录，存放文件。这里需要再nfs共享目录下建立文件minio，minio文件夹建立一个data用于存放文件数据。

![minio部署创建pv1](../images/rancher/minio部署创建pv1.png)

![minio部署创建pv2](../images/rancher/minio部署创建pv2.png)



## 9.2、创建PVC

![minio部署创建pvc1](../images/rancher/minio部署创建pvc1.png)

![minio部署创建pvc2](../images/rancher/minio部署创建pvc2.png)

## 9.3、创建服务

![minio部署创建svc1](../images/rancher/minio部署创建svc1.png)

## 9.4、部署Minio

docker部署Minio

```shell
docker run --name minio -d \
  -p 9000:9000 -p 9090:9090
  -e MINIO_ROOT_USER=admin \
  -e MINIO_ROOT_PASSWORD=admin... \
  -v /opt/minio/data:/data \
  -v /opt/minio/.minio:/root/.minio \
  minio/minio server /data --console-address ":9090" --address ":9000"
```

![minio部署创建pod1](../images/rancher/minio部署创建pod1.png)

![minio部署创建pod2](../images/rancher/minio部署创建pod2.png)

![minio部署创建pod3](../images/rancher/minio部署创建pod3.png)

## 9.5、测试Minio

![minio部署测试](../images/rancher/minio部署测试.png)
