# 1、命令

## 1.1、启动Oracle

```shell
#切换到Oracle用户
[root@boco ~]# su oracle 
#如果没有到Oracle用户目录，需要进入oracle用户目录
[oracle@boco root]$ cd /home/oracle/ 
[oracle@boco ~]$ source .bash_profile 
#利用sysdba进入oracle
[oracle@boco ~]$ sqlplus "/as sysdba" 
#启动数据
SQL> startup 
SQL> exit;
#启动数据库监听
[oracle@boco ~]$ lsnrctl start
```

# 2、错误码

## 2.1、ora-28040和ora-01017

```shell
找到文件/usr/oracle/product/network/admin/sqlnet.ora
#添加以下内容
SQLNET.ALLOWED_LOGON_VERSION_SERVER=8
SQLNET.ALLOWED_LOGON_VERSION_CLIENT=8

重启监听
[oracle@oracle oraInventory]$ lsnrctl stop 
[oracle@oracle oraInventory]$ lsnrctl start
修改密码
[oracle@oracle oraInventory]$ sqlplus / as sysdba
ALTER USER ttscms IDENTIFIED BY ttscms;
```

