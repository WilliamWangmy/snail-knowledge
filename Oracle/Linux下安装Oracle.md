# Linux下安装Oracle



## 1、建立用户和组

```shell
----创建oracle用户组
[root@root ~]# groupadd oinstall
----创建oracle用户组
[root@root ~]# groupadd dba
----oracle加入新建的2个用户组   
[root@root ~]# useradd -g oinstall -G dba oracle 
设置oracle用户的密码
[root@root ~]# passwd oracle 
设置密码：oracle123456
```

## 2、创建安装目录

```shell
----创建oracle安装目录  
[root@root ~]# mkdir -p /usr/oracle
---创建racle安装文件所在目录
[root@root ~]# mkdir -p /opt/oracle/oracinstall
----更改oracle目录用户组
[root@root ~]# chown -R oracle:oinstall /usr/oracle
----更改oracle目录权限
[root@root ~]# chmod -R 775 /usr/oracle
----更改oracle安装文件所在目录的用户组
[root@root ~]# chown -R oracle:oinstall /opt/oracle/oracinstall
----更改oracleracle安装文件所在目录的操作权限
[root@root ~]# chmod -R 755 /opt/oracle/oracinstall
---后面安装的时需要用到的oraInventory
[root@root ~]# mkdir -p /opt/oracle/oraInventory
[root@root ~]# chown -R oracle:oinstall /opt/oracle/oraInventory
[root@root ~]# chmod -R 755 /opt/oracle/oraInventory
```

## 3、安装依赖包

```shell
[root@root ~]# yum -y install binutils compat-libcap1 compat-libstdc++-33 compat-libstdc++-33*.i686 elfutils-libelf-devel gcc gcc-c++ glibc*.i686 glibc glibc-devel glibc-devel*.i686 ksh libgcc*.i686 libgcc libstdc++ libstdc++*.i686 libstdc++-devel libstdc++-devel*.i686 libaio libaio*.i686 libaio-devel libaio-devel*.i686 make sysstat unixODBC unixODBC*.i686 unixODBC-devel unixODBC-devel*.i686 libXp 
```

## 4、配置系统内核参数

1)共享内存内核参数

- kernel.shmmax:每个内存段的最大值，等于或大于sga_max_size，不小于物理内存1/2或2G
- kernel.shmall:指定任意时刻，系统可以分配的所有共享内存段总和的最大值，不小于shmmax/page_size,如果服务器运行的SGA<8G，可用默认值
- kernel.shmmni:系统可分配的共享内存段的最大数量，默认是4096

2)信号量

一种控制资源访问的方法，Oracle实例主要使用信号量来控制共享内存的访问。使用processes初始化参数分配信号量，其值至少等于processes

信号量内核参数

- kernel.sem:如：kernel.sem = 250 32000 100 128，其中等号右边4部分分别为semmsl、semmns、semopm、semmni值。
  1. semmsl指每个信号量集合中的最大信号量个数，其值或取其最小值100，或者为所有数据库中最大的processes+10，选择其中较大值者
  2. semmns值是指整个系统范围内信号量总数的最大值，默认是32000
  3. semopm用于指定每个semop()系统调用可以设置的信号量操作的最大数量，默认为100
  4. semmni用于指定信号量集合的最大数量，最小为100，Oracle建议取值128.
- net.core.rmem_default：表示套接字接收缓冲区大小的缺省值。
- net.core.rmem_max：表示套接字接收缓冲区大小的最大值。
- net.core.wmem_default：表示套接字发送缓冲区大小的缺省值。
- net.core.wmem_max：表示套接字发送缓冲区大小的最大值。

4个参数用于设置socket数据发送缓冲区及接收缓冲区的默认大小与最大大小。

```shell
[root@root ~]# vi /etc/sysctl.conf
# sysctl settings are defined through files in
# /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
#
# Vendors settings live in /usr/lib/sysctl.d/.
# To override a whole file, create a new file with the same in
# /etc/sysctl.d/ and put new settings there. To override
# only specific settings, add a file with a lexically later
# name in /etc/sysctl.d/ and put new settings there.
#
# For more information, see sysctl.conf(5) and sysctl.d(5).
kernel.shmall = 6592382
kernel.shmmax = 33752999936 
kernel.shmmni = 4096 
kernel.sem = 250 32000 100 128 
net.ipv4.ip_local_port_range = 9000 65500 
net.core.rmem_default = 4194304 
net.core.rmem_max = 4194304 
net.core.wmem_default = 262144 
net.core.wmem_max = 1048586 
fs.file-max = 6815744
```

## 5、将内核配置参数生效

```shell
---- 生效新配置的系统内核参数
[root@root ~]# sysctl -p
kernel.shmall = 2097152
kernel.shmmax = 2147483648
kernel.shmmni = 4096
kernel.sem = 250 32000 100 128
net.ipv4.ip_local_port_range = 9000 65500
net.core.rmem_default = 4194304
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048586
fs.file-max = 6815744
```

## 6、配置Oracle用户shell limit

为了提高在linux系统上运行软件的性能，必须对oracle用户设置下列限定。

- noproc - 进程的最大数目
- stack - 最大栈大小
- nofile - 打开文件的最大数目
- soft 指的是当前系统生效的设置值
- hard 表明系统中所能设定的最大值
- soft 的限制不能比har 限制高。

用 - 就表明同时设置了 soft 和 hard 的值。

oracle：被限制的用户名，组名前面加@和用户名区别

1. oracle soft nproc 2047
2. oracle hard nproc 16384
3. oracle soft nofile 1024
4. oracle hard nofile 65536
5. oracle soft stack 10240
6. oracle hard stack 10240

```shell
[root@root ~]# vi /etc/security/limits.conf
# /etc/security/limits.conf
#
#This file sets the resource limits for the users logged in via PAM.
#It does not affect resource limits of the system services.
#
#Also note that configuration files in /etc/security/limits.d directory,
#which are read in alphabetical order, override the settings in this
#file in case the domain is the same or more specific.
#That means for example that setting a limit for wildcard domain here
#can be overriden with a wildcard setting in a config file in the
#subdirectory, but a user specific setting here can be overriden only
#with a user specific setting in the subdirectory.
#
#Each line describes a limit for a user in the form:
#
#<domain>        <type>  <item>  <value>
#
#Where:
#<domain> can be:
#        - a user name
#        - a group name, with @group syntax
#        - the wildcard *, for default entry
#        - the wildcard %, can be also used with %group syntax,
#                 for maxlogin limit
#
#<type> can have the two values:
#        - "soft" for enforcing the soft limits
#        - "hard" for enforcing hard limits
#
#<item> can be one of the following:
#        - core - limits the core file size (KB)
#        - data - max data size (KB)
#        - fsize - maximum filesize (KB)
#        - memlock - max locked-in-memory address space (KB)
#        - nofile - max number of open file descriptors
#        - rss - max resident set size (KB)
#        - stack - max stack size (KB)
#        - cpu - max CPU time (MIN)
#        - nproc - max number of processes
#        - as - address space limit (KB)
#        - maxlogins - max number of logins for this user
#        - maxsyslogins - max number of logins on the system
#        - priority - the priority to run user process with
#        - locks - max number of file locks the user can hold
#        - sigpending - max number of pending signals
#        - msgqueue - max memory used by POSIX message queues (bytes)
#        - nice - max nice priority allowed to raise to values: [-20, 19]
#        - rtprio - max realtime priority
#
#<domain>      <type>  <item>         <value>
#

#*               soft    core            0
#*               hard    rss             10000
#@student        hard    nproc           20
#@faculty        soft    nproc           20
#@faculty        hard    nproc           50
#ftp             hard    nproc           0
#@student        -       maxlogins       4
oracle soft nproc 2047
oracle hard nproc 16384
oracle soft nofile 1024
oracle hard nofile 65536
oracle soft stack 10240
oracle hard stack 10240

# End of file
```

## 7、编辑登录配置文件

进行登录配置文件的编辑，在文本最后添加：session required pam_limits.so或者session required /lib/security/pam_limits.so使shell limit生效。

```shell
[root@root ~]# vi /etc/pam.d/login

#%PAM-1.0
auth [user_unknown=ignore success=ok ignore=ignore default=bad] pam_securetty.so
auth       substack     system-auth
auth       include      postlogin
account    required     pam_nologin.so
account    include      system-auth
password   include      system-auth
# pam_selinux.so close should be the first session rule
session    required     pam_selinux.so close
session    required     pam_loginuid.so
session    optional     pam_console.so
# pam_selinux.so open should only be followed by sessions to be executed in the user context
session    required     pam_selinux.so open
session    required     pam_namespace.so
session    optional     pam_keyinit.so force revoke
session    include      system-auth
session    include      postlogin
-session   optional     pam_ck_connector.so
session required pam_limits.so
```

## 8、配置oracle用户环境变量

要成功安装并使用Oracle数据库软件，必须在Oracle用户的.bash_profile文件中设置ORACLE_BASE、ORACLE_HOME、ORACLE_SID和PATH环境变量，其他的根据需要来设置。ORACLE_HOME可以在安装前手动配置，另外，Oracle安装过程中会根据ORACLE_BASE的值自动指定的ORACLE_HOME，所以也可以在安装后将这个ORACLE_HOME写入.bash_profile。

```shell
[root@root ~]# su - oracle
[oracle@root ~]$ vi .bash_profile

# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
# use for oracle 用户环境变量如下：  
export  ORACLE_BASE=/usr/oracle   
export  ORACLE_HOME=$ORACLE_BASE/product   
export  ORACLE_SID=orcl   
export PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin   
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib  
#防止Oracle安装界面乱码，先把语言环境改为英文
export LANG=en_US   
if [ $USER = "oracle" ];then     
        if [ $SHELL = "/bin/ksh" ];then         
                ulimit -p 16384         
                ulimit -n 65536       
        else        
                ulimit -u 16384 -n 65536    
        fi     
fi
```

## 9、生效配置文件

```shell
[oracle@root ~]$ source .bash_profile
```

## 10、修改hosts文件

添加IP地址和域名的映射关系，进入hosts文件，在文件末尾加上本机实际IP和主机用户名

```shell
[oracle@root ~]$ su
Password: 
[root@root oracle]# vi /etc/hosts                        

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.56.11 root
```

## 11、VNC安装（桌面可视化安装Oracle）

### 11.1、查看桌面启动方式并安装VNC

```shell
-- 查看桌面启动方式
[root@root oracle]# systemctl get-default
graphical.target
-- 安装VNC
[root@root oracle]# yum install tigervnc-server
```

### 11.2、设置VNC用systemctl来管理

```shell
[root@root oracle]# cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service
```

### 11.3、修改VNC用户ROOT

```shell
vi /etc/systemd/system/vncserver@:1.service
#修改为以下内容
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
#Type=simple
Type=forking

# Clean any existing files in /tmp/.X11-unix environment
ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'
#ExecStart=/usr/bin/vncserver_wrapper <USER> %i

ExecStart=/usr/sbin/runuser -l root -c "/usr/bin/vncserver %i"
PIDFile=/root/.vnc/%H%i.pid

ExecStop=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'

[Install]
WantedBy=multi-user.target
```

### 11.4、刷新systemctl

```shell
[root@root oracle]$ systemctl daemon-reload
```

### 11.5、设置VNC密码

```shell
[root@root oracle]$ vncpasswd
123456
```

### 11.6、修改vncservers

```shell
[root@root oracle]$ vi /etc/sysconfig/vncservers
#修改或添加如下内容
VNCSERVERS="1:root"
VNCSERVERARGS[1]="-geometry 1024x768 -BlacklistTimeout 0"
```

### 11.7、VNC相关命令

```shell
--启动
[root@root oracle]$ systemctl start vncserver@:1.service
--关闭
[root@root oracle]$ systemctl stop vncserver@:1.service
--重启
[root@root oracle]$ systemctl restart vncserver@:1.service
--查看装填
[root@root oracle]$ systemctl status vncserver@:1.service
--开启启动
[root@root oracle]$ systemctl enable vncserver@:1.service
```

## 12、安装Oracle

将Oracle安装软件上传到安装目录/opt/oracle/oracinstall，也就是第二部创建的安装目录

```shell
[root@root oracle]$ su oracle
[oracle@root ~]$ cd /opt/oracle/oracinstall
[oracle@root oracinstall]$ unzip V839960-01.zip 
[oracle@root oracinstall]$ cd database/
[oracle@root database]$ chmod +x runInstaller
[oracle@root database]$ chmod +x ./install/*.sh
[oracle@root database]$ export LANG="en_US"
[oracle@root database]$ ./runInstaller
Starting Oracle Universal Installer...

Checking Temp space: must be greater than 500 MB.   Actual 36830 MB    Passed
Checking swap space: must be greater than 150 MB.   Actual 32191 MB    Passed
Checking monitor: must be configured to display at least 256 colors.    Actual 16777216    Passed
Preparing to launch Oracle Universal Installer from /tmp/OraInstall2023-04
```

### 12.1、安装

![步骤1](../images/oracle/步骤1.png)

![步骤2](../images/oracle/步骤2.png)

![步骤3](../images/oracle/步骤3.png)

![步骤4](../images/oracle/步骤4.png)

![步骤5](../images/oracle/步骤5.png)

## 13、相关问题

### 13.1、.oui无权限

```shell
#在root用户下执行
chown -R oracle:oinstall /mesdata/orcl/app
chmod -R 775 /mesdata/orcl/app
```

### 13.2、can not connect to X11 window server using 1或using 0

```shell
#在root用户下
export DISPLAY=:1或export DISPLAY=:0
xhost +
```

### 13.3、没有安装的弹窗

必须要配合xmanager使用才可以了。

```shell
[root@root oracle]# xhost +
xhost:  unable to open display ""
[root@root oracle]# export DISPLAY=localhost:1
[root@root oracle]# xhost +
access control disabled, clients can connect from any host
//切换到oracle用户下
[root@root oracle]# su oracle
//查看当前DISPLAY环境变量的值
bash-4.2$ echo $DISPLAY
localhost:1
//允许视图界面投影到设定的ip下
bash-4.2$ export DISPLAY=192.168.101.4:0.0
//再次查看DISPLAY环境变量的值
bash-4.2$ echo $DISPLAY
192.168.101.4:0.0
//启动命令
bash-4.2$ xhost +
xhost:  unable to open display "192.168.101.4:0.0"
[root@root oracle]# yum install xterm
#这里的ip就是本机的ip,会在本机弹出桌面进行安装
```

如果还是不行就需要安装**xstart**

/usr/bin/xterm -ls -display $DISPLAY

![xstart1](../images/oracle/xstart1.png)

![xstart2](../images/oracle/xstart2.png)

- **弹出这界面再执行**./runInstaller