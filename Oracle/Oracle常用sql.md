# 1、表空间相关sql

```sql
----创建表空间
CREATE tablespace snail
logging -- 开启日志
datafile '/data/oracledata/Oracle.DBF' -- 设置存放位置，必须为.dbf文件
SIZE 50m -- 分配初始大小
autoextend ON -- 开启自动扩增
NEXT 500m maxsize 30G -- 设置当空间不够时每次增加的大小和最大值
extent management LOCAL;
---- 查询表空间
SELECT tablespace_name,
file_id,
file_name,
round(bytes / (1024 * 1024), 0) total_space
FROM dba_data_files
ORDER BY tablespace_name;
---- 添加表空间文件
Alter tablespace SCMS add datafile '/data/oracledata/ttscms2.DBF' size 1024M autoextend on next 500M maxsize 30G;
---- 创建临时表空间
CREATE TEMPORARY tablespace snail_temp
tempfile '/data/oracledata/snail_temp.DBF' -- 设置存放位置，必须为.dbf文件
SIZE 50m -- 分配初始大小
autoextend ON -- 开启自动扩增
NEXT 50m maxsize 30G -- 设置当空间不够时每次增加的大小和最大值
extent management LOCAL;
---- 查询表空间使用情况
SELECT a.tablespace_name "表空间名",
       total/ (1024 * 1024 * 1024) "表空间大小",
       free/ (1024 * 1024 * 1024) "表空间剩余大小",
       (total - free)/ (1024 * 1024 * 1024) "表空间使用大小",
       total / (1024 * 1024 * 1024) "表空间大小(G)",
       free / (1024 * 1024 * 1024) "表空间剩余大小(G)",
       (total - free) / (1024 * 1024 * 1024) "表空间使用大小(G)",
       round((total - free) / total, 4) * 100 "使用率 %"
FROM (SELECT tablespace_name, SUM(bytes) free
      FROM dba_free_space
      GROUP BY tablespace_name) a,
     (SELECT tablespace_name, SUM(bytes) total
      FROM dba_data_files
      GROUP BY tablespace_name) b
WHERE a.tablespace_name = b.tablespace_name;
```

### 1.2、查询表使用空间大小

```sql
SELECT owner,
       segment_name,
       segment_type,
       sum(bytes) / 1024 / 1024 "占用空间（MB）"
  FROM dba_segments
 WHERE owner = '表所有者' -- 用户名
 GROUP BY owner, segment_name, segment_type;
```



# 2、用户相关sql

```sql
--- 创建用户
CREATE USER snail IDENTIFIED BY snail
DEFAULT tablespace snail
TEMPORARY tablespace snail_temp;
--- 查询用户
select * from dba_users;
--- 赋权
GRANT CONNECT,resource,dba TO snail;
--- 修改用户密码
ALTER USER snail identified by snail;
---删除用户和表空间及文件
drop user snail cascade;
drop tablespace snail including contents and datafiles;
drop tablespace snail_temp including contents and datafiles;
```

# 3、数据导入相关sql

```sql
--- 创建导入directory
create or replace directory dump_dir as '/data/Oracle';
--- 数据泵导入命令
impdp snail/snail@192.168.56.11:1522/orcl rows=y ignore=y directory=dump_dir dumpfile=Oracle.dmp logfile=dp.log  fromuser=oldUser  touser=snail CLUSTER=N exclude=table:\"like \'TMP_%\'\",table:\"like \'BAK_%\'\",table:\"like \'%_log\'\"    

impdp 用户名/密码@ip:端口/数据库实例 rows=y ignore=y directory=directory dumpfile=dmp文件
logfile=日志文件  fromuser=原dmp文件的用户名  touser=新建的用户名 CLUSTER=N exclude=排除相关表

--- 导出数据，排除某些表
expdp snail/snail@192.168.56.11:1522/orcl directory=expdp_dir dumpfile=snail.dmp logfile=expdp.log exclude=table:\"like \'%_BAK\'\",table:\"like \'BAK_%\'\",table:\"like \'TMP_%\'\",table:\"like \'TEMP_%\'\",table:\"like \'%_LOG\'\"

```

## 4、查询锁表记录

```sql
SELECT dob.OBJECT_NAME Table_Name,
       lo.SESSION_ID || ', ' || vss.SERIAL# 删除号,
       lo.locked_mode,
       lo.SESSION_ID,
       vss.SERIAL#,
       vss.action Action,
       vss.osuser OSUSER,
       vss.LOGON_TIME,
       vss.process AP_PID,
       VPS.SPID DB_PID,
       vss.*
  From v$locked_object lo, dba_objects dob, v$session vss, V$PROCESS VPS
 Where lo.OBJECT_ID = dob.OBJECT_ID
   and lo.SESSION_ID = vss.SID
   AND VSS.paddr = VPS.addr
 order by 2, 3, DOB.object_name
 
 alter system kill session '16,15439'; --sid, serial#
```

