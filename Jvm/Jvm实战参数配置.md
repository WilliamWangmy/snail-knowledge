## 1、启动项目配置JVM参数

```shell
java -Xms512m -Xmx1024m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/path/to/heapdump.hprof \
     -XX:+ExitOnOutOfMemoryError \
     -XX:+UseConcMarkSweepGC \
     -XX:+PrintGCDetails \
     -XX:+PrintGCTimeStamps \
     -XX:+PrintHeapAtGC \
     -jar your-application.jar
-Xms512m -Xmx1024m 设置JVM初始和最大堆内存大小。
-XX:+HeapDumpOnOutOfMemoryError 当出现OutOfMemoryError时产生堆dump。
-XX:HeapDumpPath=/path/to/heapdump.hprof 指定堆dump文件的路径。
-XX:+ExitOnOutOfMemoryError 出现OutOfMemoryError时退出JVM。
-XX:+UseConcMarkSweepGC 指定使用并发标记清除（CMS）垃圾收集器。
-XX:+PrintGCDetails 打印GC详细信息。
-XX:+PrintGCTimeStamps 打印GC时间戳。
-XX:+PrintHeapAtGC 在GC时打印堆信息。
-jar your-application.jar 指定要运行的jar文件。
```

