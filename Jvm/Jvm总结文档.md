# Jvm与GC调优

## 1、JVM

### 1.1、JVM概述

#### 1.1.2、什么是JVM

​	JVM是Java Virtual Machine（Java虚拟机）的缩写，它是一种用于计算设备的规范，是一个虚构出来的计算机，通过在实际的计算机上仿真模拟各种计算机功能来实现。它包括一套字节码指令集、一组寄存器、一个栈、一个垃圾回收堆和一个存储方法域。

JVM屏蔽了与具体操作系统平台相关的信息，使Java程序只需生成在Java虚拟机上运行的目标代码**（字节码）**，就可以在多种平台上不加修改地运行。在执行字节码时，JVM实际上最终还是把字节码解释成具体平台上的机器指令执行。

使用Java虚拟机是实现**Java语言与平台无关性**的关键。一般的高级语言如果要在不同的平台上运行，至少需要编译成不同的目标代码，而使用Java虚拟机后，Java语言在不同平台上运行时不需要重新编译。

#### 1.1.3、Jvm的生命周期

​	Java虚拟机的启动：Java虚拟机的启动时通过**引导类加载器(bootstrap class loader)**创建一个**初始类(initial class)**来完成的，这个类是由虚拟机的具体实现指定的。

​	Java虚拟机的退出：某个线程调用Runtime类或System类的exit方法，或Runtime类的halt方法，并且Java安全管理器也允许这次exit或者halt操作；程序正常执行结束；程序在执行过程中遇到了异常或错误而异常终止；由于操作系统出现异常导致Java虚拟机进程终止。

#### 1.1.4、常见的Jvm有哪些

​	主要有这几种常见的：Sun Classic VM（解释型）、Exact VM（Solaris）、HotSpot VM（SUN公司）、JRockit （BEA；不包含解释器，服务器端，JMC）、J9（IBM）、Apache Harmony、Microsoft JVM、TaobaoJVM等等。

​	目前用的最多的是HotSpot VM。

- Hotspot是目前比较新的Java虚拟机，使用**JIT(Just in Time)**编译器，可以很大的提高Java运行的性能。
- Java原先是把源代码编译为字节码在虚拟机上运行，这样执行速度慢。而hotspot是将常用的部分代码编译为**本地(原生，native)**代码，这样很明显的提高了性能。
- HotSpot JVM 参数可以分为规则参数(standard options)和非规则参数(non-standard options)。 规则参数相对稳定，在JDK未来的版本里不会有太大的改动。 非规则参数则有因升级JDK而改动的可能。

#### 1.1.5、Jvm架构图

Jvm的架构可以分为三层进行理解：

1. 最上层：javac编译器将编译好的字节码class文件，通过java类装载器执行机制，把对象或者class文件存放在jvm划分的内存区域。
2. 中间层：运行时数据区（Runtime Data Area），主要是在Java代码运行时用于存放数据的，从左至右为**方法区（永久代、元数据区）、堆（共享、GC回收对象区域）、栈、程序计数器、寄存器、本地方法栈（私有）**
3. 最下层：解释器、JIT(just in time)编译器和GC（垃圾回收器）

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/jvm%E6%9E%B6%E6%9E%84%E5%9B%BE.png)

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/jvm%E6%9E%B6%E6%9E%84%E5%9B%BE2.png)

### 1.2、字节码

#### 1.2.1、什么是字节码

​	源代码经过编译器编译后会生成一个字节码文件，字节码是一种二进制的类文件，她内容是**JVM指令（操作码+操作数）**，而C和C++经过编译器编译后直接生成了机器码。

包装类缓存问题，不在缓存中就需要装箱。

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%8C%85%E8%A3%85%E7%B1%BB%E7%9A%84%E7%BC%93%E5%AD%98%E9%97%AE%E9%A2%98.png)

#### 1.2.2、编译器的种类

- javac编译器（前端编译器）：将java源码编译为字节码
- ECJ编译器（前端编译器）：是Eclipse内置的一种增量式编译器；Tomcat中也同样使用了ECJ编译器来编译jsp文件。

后端编译器是指直接将源代码编译为机器指令（机器语言）的编译器，比较常见的有GCC、Clang、Microsoft Visual C++等等。

前端编译器并不会直接涉及编译优化等方面的技术，而是将这些具体优化细节移交给HotSpot的JIT编译器负责。jdk9引入了AOT编译器(静态提前编译器，Ahead Of Time Compiler)。Java 9 引入了实验性 AOT 编译工具jaotc。它借助了 Graal 编译器，将所输入的 Java 类文件转换为机器码，并存放至生成的动态共享库之中。

所谓 AOT 编译，是与即时编译相对立的一个概念。我们知道，即时编译指的是在程序的运行过程中，将字节码转换为可在硬件上直接运行的机器码，并部署至托管环境中的过程。而 AOT 编译指的则是，在程序运行之前，便将字节码转换为机器码的过程。
.java -> .class -> .so

优点：Java虚拟机加载已经预编译成二进制库，可以直接执行。不必等待即时编译器的预热，减少Java应用给人带来“第一次运行慢”的不良体验。

缺点：
破坏了java“一次编译，到处运行”，必须为每个不同硬件、OS编译对应的发行包。
降低了Java链接过程的动态性，加载的代码在编译期就必须全部已知。

- Javac编译器

​	javac的编译步骤分别是词法解析、语法解析、语义解析、生成字节码。

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/java%E5%87%BA%E7%BC%96%E8%AF%91%E5%99%A8%E6%AD%A5%E9%AA%A43.png)

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/java%E5%87%BA%E7%BC%96%E8%AF%91%E5%99%A8%E6%AD%A5%E9%AA%A4.png)

#### 1.2.4、class文件结构

​	Class文件的总体结构：

1. 魔术：class文件的标志（是class文件的标识符），每个class文件开头的4个字节的无符号整数称为魔数。它的唯一作用是确定这个文件是否为一个能被虚拟机接受的有效合法的class文件；魔数固定值0xCAFEBABE，不是这个固定值就不会被虚拟机抛出异常。

2. Class文件版本：魔数后面的4个字节存储的是Class版本号。第5、6个字节代表的含义就是编译的服版本号minor_version，第7、8个字节就是编译的主版本号minor_version。

3. 常量池：存放所有的常量，它是class文件中的资源仓库，她是class文件结构中与其他项目关联最多的数据类型，也是占用class文件空间最大的数据项目之一；常量池表中用于存放编译时期生成的各种**字面量（文本字符串、声明为final的字符常量）和符号引用（类和接口的全限定名、字段/方法名称和描述符）**，这部分内容在类加载后进入付费区的运行时常量池存放。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B%E6%8F%8F%E8%BF%B0%E7%AC%A6.png)

4. 访问标识（标志）：在常量池后面就是访问标记，该标记使用两个字节表示，用于识别一些类或者接口层次的访问信息，包括这个class是类还是接口、是否定义public类型、是否为abstract类型、是否是声明final的类等等。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E8%AE%BF%E9%97%AE%E6%A0%87%E8%AF%86.png)

5. 类索引、父类索引、接口索引集合：在访问标识后面会指定类的类别、父类类别和实现的接口。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E7%B1%BB%E3%80%81%E7%88%B6%E7%B1%BB%E3%80%81%E6%8E%A5%E5%8F%A3%E7%B4%A2%E5%BC%95.png)

   - this_class类索引：类索引确定这个类的全限定名。2字节无符号整数，指向常量池的索引；

   - this_class父类索引：父类索引确定这个类的父类全限定名（只有一个）。2字节无符号整数，指向常量池的索引；

   - interfaces接：指向常量池索引集合，它提供了一个符号引用到所有已实现的接口。类可以实现多个接口所以这里是以数组的方式进行保存多个接口索引，每个索引都指向了常量池的CONSTANT_Class 

   - interfaces_count (接口计数器)：表示当前类或接口的直接接口数量

   - interfaces []接口索引集合：interfaces []中每个成员的值必须是对常量池表中某项的有效索引值，它的长度为 interfaces_count。 

6. 字段表集合：用于描述接口或类中声明的变量。它包括了类级变量以及实例级变量，不包括方法内部、代码块内部声明变量；它指向常量池中索引集合，描述了每个字段的完整信息。

   - 字段计数器：标识当前class文件的fields表中的成员个数，两个字节表示。

   - 字段表：fields表中每个成员都必须是一个fields_info结构的数据项，用于标识当前类或者接口中某个字段的完整描述。不会列出父类和实现父接口中继承而来的字段。
     1. ​	作用域（public、protected、private）
     2. 是实例变量还是类变量（static修饰符）
     3. 可变性（final）
     4. 并发可见性（volatile修饰符，是否强制从主内存读写）
     5. 可否序列化（transient修饰符）
     6. 字段数据类型（基本数据类型、对象、数组）
     7. 字段名称

7. 方法表集合：指向常量池索引集合，它完整描述了每个方法的签名。每一个method_info项都对应着一个类或者接口中的方法信息。跟字段一样只描述当前类或者接口中的方法，不会从父类或者父接口中继承方法。

   - 方法计数器：methods_count的值表示当前class文件methods表的成员个数。使用两个字节来表示。

   - 方法表：methods 表中每个成员都是一个method_info结构，用于表示当前类或者接口中某个方法的完整描述，这个结构中包括了实例方法、类方法、实例初始化方法和类或者接口初始化方法。

     ​		方法表结构：

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%96%B9%E6%B3%95%E8%A1%A8%E7%BB%93%E6%9E%84.png)

   ​				方法表访问标志

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%96%B9%E6%B3%95%E8%A1%A8%E8%AE%BF%E9%97%AE%E6%A0%87%E8%AF%86.png)

   ​				

8. 属性表集合：指的是class文件所携带的辅助信息。

- 有哪些类型有对应的class对象

1. class：外部类，成员(成员内部类，静态内部类)，局部内部类，匿名内部类
2. interface：接口
3. []：数组
4. enum：枚举
5. annotation：注解@interface
6. primitive type：基本数据类型
7. void

- Class文件结构解析表

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/class%E6%96%87%E4%BB%B6%E8%A7%A3%E6%9E%90%E8%A1%A8.png)

#### 1.2.5、字节码指令

1. 加载与存储指令：用于将数据从栈帧的局部变量表和操作数栈质检来回传递。

   - 局部变量压栈指令有xload、xload_n（其中x为i、l、f、d、a，n 为 0 到 3）
   - 常量入栈指令有bipush、sipush、ldc、ldc_w、ldc2_w、aconst_null、iconst_m1、iconst_<i>、lconst_<l>、fconst_<f>、dconst_<d>
   - 出栈装入局部变量表指令有xstore、xstore_<n>（其中x为i、l、f、d、a，n 为 0 到 3）

   1）操作数栈是java虚拟机开辟的一块额外的空间，来存放计算的操作数和返回结果。

   2）局部变量表是方法栈桢的另外一个重要部分，字节码程序可以将计算的结果缓存在局部变量表中。局部变量表可以理解为一个数组，如果是非静态方法依次放入的this、传入的参数、以及字节码中的局部变量。

   **局部变量表和操作数栈中，long类型以及double类型的值占据两个单元，其余都是占一个单元。**

2. 算术指令：用于对两个操作数栈上的值进行某个特定的运算，并把结果重新压入操作数栈。

   - 加法指令：iadd、ladd、fadd、dadd
   - 减法指令：isub、lsub、fsub、dsub
   - 乘法指令:imul、lmul、 fmul、dmul
   - 除法指令：idiv、ldiv、fdiv、ddiv
   - 求余指令：irem、lrem、frem、drem    //remainder:余数
   - 取反指令：ineg、lneg、fneg、dneg    //negation:取反
   - 自增指令：iinc
   - 位运算指令，又可分为：
     - 位移指令：ishl、ishr、iushr、lshl、lshr、lushr
     - 按位或指令：ior、lor
     - 按位与指令：iand、land

3. 类型转换指令：将两种不同的数值类型进行相互转换。一般用于实现用户代码中的显式类型转换操作，或者用来处理字节码指令集中数据类型相关指令无法与数据类型一一对应的问题。

4. 对象的创建与访问指令：用于对象操作的一系列指令，分为创建指令、字段访问指令、数组操作指令、类型检查指令。

5. 方法调用与返回指令

6. 操作数栈管理指令

7. 控制转移指令

8. 异常处理指令

9. 同步控制指令

### 1.3、类的加载

#### 1.3.1、类的生命周期

​	类的生命周期分为七个阶段：加载（装载）、验证、准备、解析、初始化、使用、卸载。也可以理解为五个阶段加载（装载）、链接（Linking，包括验证、准备、解析）、初始化（Initializatio）、使用（Using）、卸载（unloading）

- loading装载：将java类的字节码文件加载到机器内存中，并在内存中构建出Java类的原型类模板对象。加载类时必须要完成以下3件事情：

  1. 通过类的全名，获取类的二进制数据流。
  2. 解析类的二进制数据流为方法区内的数据结构（Java类模板）
  3. 创建java.lang.class类的实例，标识该类型。作为方法区这个类的各种数据访问入口。

  **类模板对象就是Java类在JVM内存中的一个快照，JVM将从字节码文件中解析出常量池、类字段、类方法等信息存储到类模板中，存储在方法区（jdk1.8之前成为永久代，1.8之后成为元空间）**

  数组类的加载：数组类本身并不是由类加载器负责创建，而是由JVM在运行时根据需要而直接创建的，但是数据的元素类型仍然需要依靠类记载器区创建。

- Linking链接
  1. 验证：主要目的是保证加载的字节码是合法、合理并符合规范的。验证的内容涵盖了格式验证（魔数是否正确）、语义验证、字节码验证（验证字节码指定是否存在等等）、符号引用验证（检查类或者方法是否存在）等。
  2. 准备：为类的静态变量分配内存，并将其初始化为默认值。
  3. 解析：将类、接口、字段和方法的符号引用转为直接引用。

- Initialzation初始化：为类的静态变量赋予正确的初始值(显式初始化)。类的初始化是类装载的最后一个阶段，执行类中定义的初始化方法<clinit>()方法。

  1. 加载一个类之前会先加载该类的父类，因此父类的clinit方法会先被调用，父类的static块优先级高于子类。由父及子，静态先行。
  2. 并不是所有类会产生clinit初始化方法。以下类编译为字节码后**不会产生clinit方法**：
     - 一个类中并没有生命任何的类变量，也没有静态代码块。
     - 一个类中声明类变量，但是没有明确使用类变量的初始化语句以及静态代码块来执行初始化操作时。
     - 一个类中包含了static final修饰的基本数据类型的字段，这些类字段初始化语句采用编译时常量表达式。
     - 对于非静态的字段，不管是否进行了显式赋值，都不会生成<clinit>()方法
     - 静态的字段，没有显式的赋值，不会生成<clinit>()方法
     - 对于声明为static final的基本数据类型的字段，不管是否进行了显式赋值，都不会生成<clinit>()方法

  **普通基本数据类和引用类型（常量）的静态变量，是需要额外调用putstatic等JVM指令的，这些都需要在显示初始化阶段执行。而基本数据类型常量（非调用方法的显示赋值）、String类型字面量的定义方式的常量，是在准备阶段完成。总之static+final修饰的成员变量并不涉及到方法或者构造器调用就是在准备阶段赋值，其余都是在初始化阶段赋值。**

- Using使用
- unloading卸载：当类不再被引用时就会被卸载。

#### 1.3.2、类加载器

- 什么是类加载器？

  ​		类加载器是JVM中的一个重要组件，负责将字节码文件加载到内存中，并转换为可执行的类。在jvm运行时，会根据需要动态创建类，而类加载器就是负责加载这些类。

- 类加载器有哪些？

  ​		JVM支持两种类型的类加载器，引导类加载器（Bootstrap ClassLoader）、自定义类加载器（User-Defined ClassLoader）。

  ​		在java中默认使用了三种类加载器，ootstrap类加载器、Extension类加载器和System类加载器（或称Application类加载器）。这些类加载器都有设定好从哪里加载类。

  1. Bootstrap类加载器是初始类加载器，负责加载核心类库。
  2. Extension类加载器是Bootstrap类加载器的子加载器，负责从Java的扩展目录（java_home/lib/ext）或系统属性java.ext.dirs所指定的目录中加载类库。
  3. System类加载器（或Application类加载器）是Extension类加载器的子加载器，负责从classpath环境变量中加载某些应用相关的类，classpath环境变量通常由-classpath或-cp命令行选项来定义，或者是JAR中的Manifest的classpath属性。
  4. 自定义类加载是由开发人员自定义的一种类加载器。

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E7%B1%BB%E5%8A%A0%E8%BD%BD%E5%99%A8%E7%A7%8D%E7%B1%BB.png)

- 类记载器的作用

  ​		在类加载过程中，为了避免类的重复定义和冲突，**Java采用双亲委派模型（Parent Delegation Model）**。在这种模型中，当一个类加载器收到类加载请求时，它首先不会自己去尝试加载，而是把这个**请求委派给父类加载器**去完成，每一个层次的类加载器都是如此，因此所有的加载请求最终都应该传送到顶层的启动类加载器中，只有当父类加载器无法完成这个加载请求（它的搜索范围中没有找到所需的类）时，子加载器才会尝试自己去加载。

  总的来说，类加载器是Java虚拟机的重要组成部分，它负责将字节码文件转换为可执行的类，并采用**双亲委派模型**来避免类的重复定义和冲突。

#### 1.3.3、Classloader源码分析

- Classloader与现有类加载器的关系

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Classloader%E4%B8%8E%E7%8E%B0%E6%9C%89%E7%B1%BB%E5%8A%A0%E8%BD%BD%E7%9A%84%E5%85%B3%E7%B3%BB.png)

- Classloader的主要方法

  1. public final ClassLoader getParent()  返回该类的超类加载器

  2. public Class<?> loadClass(String name) throws ClassNotFoundException 加载名称为name的类，返回class实例，该方法中的逻辑就是**双亲委派模式**的实现。

     ```java
     protected Class<?> loadClass(String name, boolean resolve) //resolve:true-加载class的同时进行解析操作。
         throws ClassNotFoundException{
         synchronized (getClassLoadingLock(name)) { //同步操作，保证只能加载一次。
             //首先，在缓存中判断是否已经加载同名的类。
             Class<?> c = findLoadedClass(name);
             if (c == null) {
                 long t0 = System.nanoTime();
                 try {
                    //获取当前类加载器的父类加载器。
                     if (parent != null) {
                         //如果存在父类加载器，则调用父类加载器进行类的加载
                         c = parent.loadClass(name, false);
                     } else { //parent为null:父类加载器是引导类加载器
                         c = findBootstrapClassOrNull(name);
                     }
                 } catch (ClassNotFoundException e) {
                     // ClassNotFoundException thrown if class not found
                     // from the non-null parent class loader
                 }
     
                 if (c == null) { //当前类的加载器的父类加载器未加载此类 or 当前类的加载器未加载此类
                     // 调用当前ClassLoader的findClass()
                     long t1 = System.nanoTime();
                     c = findClass(name);
     
                     // this is the defining class loader; record the stats
                     sun.misc.PerfCounter.getParentDelegationTime().addTime(t1 - t0);
                     sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                     sun.misc.PerfCounter.getFindClasses().increment();
                 }
             }
             if (resolve) {//是否进行解析操作
                 resolveClass(c);
             }
             return c;
         }
     }
      
     ```

     

  3. protected Class<?> findClass(String name) throws ClassNotFoundException查找二进制名称为name的类，返回class实例。在jdk1.2之后建议将类加载的逻辑卸载这个方法里面，并且要符合双亲委派模式。

- SecureClassLoader 与 URLClassLoader

  ​		SecureClassLoader 是一个自定义的类加载器扩展了classloader，并且新增了对代码源的位置及证书的验证、权限定义类验证（class源码的访问权限）。

  ​		URLClassLoader是SecureClassLoader 的子类，它对classloader没有具体实现的方法提供了具体的实现，并且新增了URLClassPath类协助取得Class字节码流等功能

- ExtClassLoader 与 AppClassLoader

  ​		ExtClassLoader 与 AppClassLoader都属于拓展类加载，这两个类都继承URLClassLoader，是一个静态的内部类。

- Class.forName() 与 ClassLoader.loadClass()

  Class.forName()：是一个静态方法。该方法在将 Class 文件加载到内存的同时,会执行类的初始化。

  ClassLoader.loadClass()：这是一个实例方法,需要一个 ClassLoader 对象来调用该方法。该方法将 Class 文件加载到内存时,并不会执行类的初始化,直到这个类第一次使用时才进行初始化。

#### 1.3.4、双亲委派机制

- 什么是双亲委派机制？

  ​		双亲委派机制是Java中的一种类加载机制。在Java中，类加载器负责加载类的字节码并创建对应的Class对象。双亲委派机制是指当一个类加载器收到类加载请求时，它会先将该请求委派给它的父类加载器去尝试加载。只有当父类加载器无法加载该类时，子类加载器才会尝试加载。

- 双亲委派机制的好处

  ​		目的是为了保证类的加载是有序的，避免重复加载同一个类，确保一个类的全局唯一性。

- 双亲委派机制的破坏
  1. 在jdk1.2引入双亲委派机制的时候，loadclass方法就可以被覆盖，导致双亲委派机制可以被破坏，但是在1.2之后就新加了findClass方法，让用户编写加载逻辑到这个方法里面。
  2. 线程上下问类加载器可以通过父类加载器去亲戚子类加载器完成类加载的行为，这种行为也是破坏了双亲委派模型的机制。Java中涉及SPI的加载基本上都采用这种方式来完成，例如JNDI、JDBC、JCE、JAXB和JBI等。
  3. 代码热替换、模块热部署这种程序动态性的操作也会破坏双亲委派机制。IBM公司主导的JSR-291（即OSGi R4.2）就是一种实现模块化热部署的类加载器。

- Tomcat不遵循双亲委派机制

  ​		Tomcat只是自定义了classloader顺序不同，但是顶层还是相同的，还是要去顶层请求classloader。因为Tomcat的WebAppClassLoader类加载器会优先加载，如果加载不到再交给commonClassLoader走双亲委派。

  ```java
  @Override
      public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
  
          synchronized (JreCompat.isGraalAvailable() ? this : getClassLoadingLock(name)) {
          	// (0) 先从自己的缓存中查找，有则返回，无则继续
              clazz = findLoadedClass0(name);
              if (clazz != null) {
                  if (log.isDebugEnabled()) {
                      log.debug("  Returning class from cache");
                  }
                  if (resolve) {
                      resolveClass(clazz);
                  }
                  return clazz;
              }
              // (0.1) 检查我们之前的类缓存，有就返回
              ......
              // (0.2) 尝试使用引导类加载器（bootstrap class loader）来加载这个类
              ......
              //判断是否需要先让parent代理
              boolean delegateLoad = delegate || filter(name, true);
  
              // (1) 先让parent加载，通常delegateLoad == false，即这一步不会执行
              if (delegateLoad) {
                  if (log.isDebugEnabled()) {
                      log.debug("  Delegating to parent classloader1 " + parent);
                  }
                  try {
                      clazz = Class.forName(name, false, parent);
                      if (clazz != null) {
                          if (log.isDebugEnabled()) {
                              log.debug("  Loading class from parent");
                          }
                          if (resolve) {
                              resolveClass(clazz);
                          }
                          return clazz;
                      }
                  } catch (ClassNotFoundException e) {
                      // Ignore
                  }
              }
          }
  ```

### 1.4、JVM内存

#### 1.4.1、JVM的内存结构

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/hotspot%20jvm%E5%86%85%E5%AD%98%E7%BB%93%E6%9E%84.png)

#### 1.4.2、程序计数器

​		程序计数器（Program Counter）是计算机处理器中的一个寄存器，用于存储下一条指令的地址。在计算机执行程序时，程序计数器会不断地更新，指向下一条要执行的指令。

程序计数器的工作原理如下：

1. 程序计数器存储的是下一条要执行的指令的地址。在执行指令之前，程序计数器会被更新为下一条指令的地址。
2. 当执行一条指令时，处理器会从程序计数器中取出指令地址，并加载指令到指令寄存器中。
3. 指令被加载到指令寄存器后，处理器会解析该指令，并执行相应的操作。
4. 在执行完一条指令后，程序计数器会被更新为下一条指令的地址。
5. 这个过程会不断重复，直到程序执行完毕。

程序计数器是计算机处理器中的一个重要组成部分，它保证了程序的正确执行顺序。

#### 1.4.3、栈和堆的区别

- 栈管运行；堆管存储
- 栈不会发生GC；堆会发生GC
- 栈的空间较小，在编译时就确定了大小，超过了空间大小就会发生OOM；堆空间较大，它的内存大小与系统的虚拟内存有关，相对灵活，超过了可用的空间大小也会发生OOM。
- 栈属于系统自动分配和释放空间速度快；堆是需要申请和释放，并且容易产生内存碎片，速度较慢。

#### 1.4.3、栈

##### 1.4.3.1、什么是栈？

​		每个线程都会创建一个虚拟机栈，其内部保存一个个的栈桢，对应每次方法的调用。是线程私有的，生命周期和线程一致。它的特点是一种快速有效的分配存储方式，访问速度仅次于程序计数器。栈遵循先进后出/后进先出（FILO）原则。

##### 1.4.3.2、设置大小

-XX:ThreadStackSize

##### 1.4.3.3、栈桢

栈的单位成为栈桢。在线程上正在执行的**每个方法都各自对应一个栈桢**；栈桢是一个内存区块，一个数据集，维系着方法执行过程中的各种数据信息。

##### 1.4.3.4、栈的内部结构

栈的内部结构包括局部变量表、操作数栈、动态链接、方法返回地址和一些附加信息。

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%A0%88%E7%9A%84%E5%86%85%E9%83%A8%E7%BB%93%E6%9E%84.png)

##### 1.4.3.5、局部变量表

- 局部变量表也被称为局部变量数组或者本都变量表
- 定义为一个数字数组，主要用于存储方法参数和定义在方法体内的局部变量。
- 局部变量表的容量是在编译时期就确定下来的。
- 方法嵌套调用的次数由栈的大小决定的。栈越大，方法嵌套调用此数据越多。
- 局部变量表中的变量只在当前方法调用中有效。方法调用接收后，随着方法栈桢的销毁，局部变量表也会随之销毁。
- **局部变量表是建立在线程的栈上，属于线程私有不存在现场安全问题**
- 局部变量表中最小的存储单元是slot（变量槽），64位类型（long和double）占用两个slot，32位类型都只占用一个。并且slot是可以重复利用的。
- **局部变量表中的变量是重要的垃圾回收根节点，只要被局部变量表中直接或者间接引用的对象都不会被回收。**

##### 1.4.3.6、操作数栈

- Java虚拟机的引擎是基于栈的执行引擎，栈就是操作数栈。
- 操作数栈遵循FILO(先进后出)
- 操作数据栈64位的类型占用两个栈单位，32位占用一个。
- **主要用于保存计算过程的中间结果，同时作为计算过程中变量临时的存储空间。**

> **栈顶缓存技术：由于操作数是存储在内种，频繁的执行内存读写会影响执行速度，因此将栈顶元素全部缓存在物理CPU的寄存器中，来降低对内存的读写次数，提升执行引擎的执行效率。**

3. 动态链接

   也被叫做指向运行时常量池的方法引用。每一个栈桢内部都包含了一个指向运行时常量池中该栈桢所属方法的引用。

   在源代码被编译成class文件时，所有的变量和方法都作为符号引用保存在class文件的常量池中（提供符号和常量，便于指定的识别），而动态链接就是为了将这些符号引用转换为调用方法的直接引用。

   - 

4. 方法返回地址 

   存放调用该方法的pc寄存器的值，调用者的pc计数器的值作为返回地址，即调用该方法的指令的下一条指令的地址。

#### 1.4.4、堆

1. 概述

   ​		JVM实例只存在一个堆内存，对也是Java内存管理的核心区域。在JVM启动的时候就被创建。堆是GC执行垃圾回收的重点区域。

2. 对象都分配在堆上？

   在Java中对象通常都是分配在堆上，基本数据类型是在栈上分配内存的；引用类型则是在堆上分配内存，基本数据类型的变量直接存储其值，而引用类型的变量则存储一个指向堆中对象的指针。

##### 1.4.4.1、堆的内部结构

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%A0%86%E7%9A%84%E5%86%85%E9%83%A8%E7%BB%93%E6%9E%841.7.png)

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%A0%86%E7%9A%84%E5%86%85%E9%83%A8%E7%BB%93%E6%9E%841.8.png)

##### 1.4.4.2、分配担保策略

老年代最大连续使用空间是否大于新生代所有对象总空间，如果小于，要先查看-XX:HandlePromotionFailure设置是否允许担保失败，如设置为false，那么会提前进行Full GC来为新生代腾出空间；如果设置为true，也要进行一次Full GC，如果大于需要进行Minor GC。

当老年代有足够空间时，检测-XX:HandlePromotionFailure=true开始进行分配担保。

##### 1.4.4.3、内存分配策略

分配原则：

- 首先分配到Eden；
- 大对象直接分配到老年代；
- 长期存活的对象分配到老年代；如果Survivor区中相同年龄的所有对象大小总和大于Survivor空间的一半，年龄大于或者等于该年龄的对象可以直接进入老年代，无须等到阈值年龄。
- 空间分配担保策略

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%A0%86%E5%86%85%E5%AD%98%E5%88%86%E9%85%8D%E7%AD%96%E7%95%A5.png)



##### 1.4.4.4、Minor GC触发机制

- 当Eden元空间满了就会触发Minor GC，Survivor满了不会触发GC。每次Minor GC会清理年轻代的内存。
- 因为Java大多数对象都是朝生夕灭，所以Minor GC非常频繁，回收速度也比较快。
- Minor GC会引发STW，暂停其他用户线程，等待垃圾回收结束。用户线程才回复运行。

##### 1.4.4.5、Major GC触发机制

​		是指发生在老年代的GC。出现MajorGC经常会至少一次的MinorGC（并不是每次都是）；当老年代空间不足时，会尝试触发Minor GC，如果之后空间还不足则会触发MajorGC

​		Major GC速度比MinorGC慢10被以上，STW的时间更长，如果MajorGC后内存还不足就会发生OOM。

##### 1.4.4.6、Full GC触发机制

- 调用system.gc()是，系统会执行Full GC
- 老年代空间不足
- 方法区空间不足
- 通过MinorGC后进入老年代的平均大小大于老年代可用内存
- 对象从Survivor的to到老年代时，老年代的空间小于对象的大小。

##### 1.4.4.7、快速分配策略TLAB

- 从内存模型而不是垃圾收集的角度，对Eden区域继续进行划分，JVM为每个线程分配了一个私有缓存区域，它包含在Eden空间内。
- 由于堆去时线程共享区域，任何线程都可以访问到堆区中的共享数据；并且对象实例创建非常频繁，因此在并发环境下从堆区中划分内存空间时线程不安全的；为了避免多个线程操作同一地址，需要使用加锁等机制，进而会影响分配速度。所以为了避免非现场安全问题，同时还能够提升内存分配的吞吐量就使用TLAB这种快速分配策略。

#### 1.4.5、方法区

- 栈、堆、方法区的关系

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%A0%88%E3%80%81%E5%A0%86%E3%80%81%E6%96%B9%E6%B3%95%E5%8C%BA%E5%85%B3%E7%B3%BB.png)

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E6%A0%88%E3%80%81%E5%A0%86%E3%80%81%E6%96%B9%E6%B3%95%E5%8C%BA%E5%85%B3%E7%B3%BB1.png)

- 方法区

  1. 方法区在逻辑上是属于堆的一部分，hotspot中的另一个别名叫做非堆，目的就是要和堆分开。
  2. 方法区和堆一样是各个线程共享的区域；方法区大小决定了可以保存多少各类，如果定义的类太多会导致付费区溢出OOM：PermGen space/ Metaspace
  3. 关闭JVM就会是否这个区域的内存

- 在JDK7之前把方法区称为永久代，jdk8使用了元空间取代了永久代。**元空间和永久代的区别在于元空间不在虚拟机设置的内存中，而是使用本地内存**。

- jdk7及以前使用-XX:PermSize设置永久代初始分配空间大小，默认是20.75M。-XX:MaxPermSize来设定永久代最大可分配空间，32为默认64M，64位默认82M。jdk8及以后使用-XX:MetaspaceSize和-XX:MaxMetaspaceSize指定元空间的初始换分配空间和最大可分配空间大小，在Windows下-XX:MetaspaceSize默认是21M，-XX:MaxMetaspaceSize 没有限制。

- 方法区存了哪些东西？

  1. 类型信息：对于每个加载的类型（class、interface、enum、annotation），jvm必须在方法区存储这些信息：完整的有效名称（包名.类名）、直接父类的完整有效名、类型修饰符、直接接口的有序列表。
  2. 域信息：域名称、域类型、域修饰符(public, private, protected, static, final, volatile, transient的某个子集)
  3. 方法信息：方法名、返回类型、方法修饰符、方法字节码、操作数栈、局部变量表及大小、异常表。
  4. non-final类变量：静态变量和类关联在一起，随着类的加载而加载,它们成为类数据在逻辑上的一部分
  5. 运行时常量池：常量池表是用于存放编译期生成的各种字面量与符号引用，这部分内容将在类加载后存放到方法区的运行时常量池中。

- 静态引用对应的对象实体始终都存在堆空间
- **方法区的垃圾收集主要回收两部分内容：常量池中废弃的常量（如没有被任何地方引用的字面量）和不再使用的类型。**当一个类要同时满足三个条件它可能被标记无用的类被回收：
  1. 该类所有的实例都已经被回收，也就是Java堆中不存在该类及其任何派生子类的实例。
  2. 加载该类的类加载器已经被回收，
  3. 该类对应的java.lang.Class对象没有任何地方被引用，且无法在任何地方通过反射访问该类的方法。

- 直接内存
  1. 不是Java虚拟机运行时数据区的一部分，也不是定义的内存区域。是Java堆外、直接向系统申请的内存区间。
  2. 读写性能高于Java堆，读写频繁的场合可能会考虑使用直接内存。Java的NIO库允许Java程序使用直接内存，用于数据缓冲区。

### 1.5、JVM对象

#### 1.5.1、对象实例化

- 对象创建几种方式
  1. **使用new关键字**：这是最常见和最简单的创建对象的方式。通过这种方式，我们可以调用任意的构造函数（无参的和有参的）来创建对象。
  2. **使用反射机制**：反射是对于任意一个正在运行的类，都能动态获取到它的属性和方法。反射创建对象分为两种方式，一是使用Class类的newInstance()方法，二是使用Constructor类的newInstance()方法。
  3. **使用clone方法**：如果要拷贝的对象需要实现Cloneable类，并重写clone()方法。
  4. **使用反序列化方式**：当序列化和反序列化一个对象时，jvm会给我们创建一个单独的对象。在反序列化时，jvm创建对象并不会调用任何构造函数。为了反序列化一个对象，需要让类实现Serializable接口。然后在使用new ObjectInputStream().readObject()来创建对象。

- 创建对象的步骤
  1. **判断对象对应的类是否加载、链接（链接又分为校验、准备、解析）、初始化**。
  2. **为对象分配内存**：如果内存规整，指针碰撞（假设java堆中内存是绝对规整的，所有用过的内存放一边，未使用过的放一边，中间有一个指针作为临界点，如果新创建了一个对象则是把指针往未分配的内存挪动与对象内存大小相同距离，这个称为指针碰撞。）；如果内存不规整，虚拟机维护一个列表、然后通过空闲列表分配内存。
  3. **处理并发安全问题**。
  4. **初始化分配到的空间**：给所有属性设置默认值。
  5. **设置对象的对象头**。
  6. **执行init方法进行初始化**。

#### 1.5.2、对象内存布局

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%AF%B9%E8%B1%A1%E5%86%85%E5%AD%98%E5%B8%83%E5%B1%80.png)

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%AF%B9%E8%B1%A1%E5%86%85%E5%AD%98%E5%B8%83%E5%B1%801.png)

- 对象头
  1. **对象自身的运行时数据**：这部分数据包括哈希码（HashCode）、GC分代年龄、锁状态标志、线程持有的锁、偏向线程ID、偏向时间戳等。这部分数据的长度在32位和64位的虚拟机中分别为32个和64个Bits。官方称这部分数据为“Mark Word”。
  2. **类型指针**：即对象指向它的类的元数据的指针，虚拟机通过这个指针来确定这个对象是哪个类的实例。
- 实例数据：它是对象真正存储的有效信息，包括程序代码中定义的各种类型的字段。
- 对齐填充

#### 1.5.3、对象的访问定位

1. 通过句柄访问时，Java堆将会划分出一部分内存来作为句柄池，reference中存储的是对象的句柄地址。而句柄中包含了对象实例数据与类型数据各自具体的地址信息。这种方式的好处在于，当对象被移动时，只需要改变句柄中的实例数据指针，而reference本身不需要被修改。
2. 直接指针访问时，对象的内存布局就必须考虑如何放置访问类型数据的相关信息，而reference中存储的就是对象的地址。这种方式的好处在于速度快，因为reference直接存储的是对象实例数据的地址，节省了一次对象实例指针定位的时间开销。

### 1.6、执行引擎

#### 1.6.1、概述

​		执行引擎是JVM中负责执行字节码的组件，它负责将字节码转换为特定平台的机器码并执行。执行引擎是JVM中与平台相关的部分，不同的操作系统和处理器架构需要不同的执行引擎。

​		执行引擎的主要职责是加载类文件并执行其中的字节码。它通过解释执行字节码或通过即时编译（JIT）将字节码转换为本地机器代码来执行。解释执行是指将字节码逐条解释为本地机器代码并执行，而JIT编译是将字节码转换为本地机器代码，并在需要时直接执行这些本地代码。

执行引擎还包括垃圾回收器，用于自动管理内存并回收不再使用的对象。垃圾回收器通过跟踪对象的生命周期和引用关系来确定哪些对象不再被引用，并在适当的时候释放这些对象的内存。

#### 1.6.2、解释器

1. 概述

   ​		当Java虚拟机启动时会根据预定义的规范对字节码采用逐行解释的方式执行，将每条字节码文件中的内容“翻译”为对应平台的本地机器指令执行。

2. 工作机制

   ​	将字节码文件“翻译”成对应平台的本地机器指令执行。当一条字节码指令被解释执行完成后，接着再根据PC寄存器中记录的吓一跳需要被执行的字节码指令执行解释操作 。

3. 解释器分类

   - 字节码解释器：纯软件代码模拟字节码的执行，效率非常低。
   - 模板解释器：将每一条字节码和一个模板函数相关联，模板函数中直接产生这条字节码执行时的机器码。

### 1.7、垃圾回收

#### 1.7.1、概述

​		垃圾是指在运行程序中没有任何指针指向的对象，这个对象就是需要被回收的垃圾。

​		对于虚拟机内存而言是有上限的，迟早都会被消耗完，因此就需要垃圾回收来是清理垃圾释放内存空间，除了释放没有对象还可以清楚内存中的记录碎片，碎片整理将所占用的堆内存移到堆的一端。

​		GC的重点区域是堆和方法区

#### 1.7.2、垃圾回收算法

1. **引用计数算法**：该算法通过增加和减少引用计数来判断对象是否为垃圾对象。当一个对象的引用计数为0时，即表示该对象已经不再被引用，可以被回收。但该算法无法处理循环引用的情况，且每次对对象赋值时均要维护引用计数器，存在一定的消耗。
2. **标记-清除算法**：该算法分为“标记”和“清除”两个阶段，标记阶段遍历所有存活对象，并进行标记。清除阶段将未被标记的对象回收。该算法的缺点是会产生大量不连续的内存碎片，导致内存利用率低。
3. **复制算法**：该算法将内存空间分为两个区域，一部分为使用中的区域，另一部分为空闲的区域。当使用区域满时，将存活对象复制到空闲区域中，再将使用区域清空。该算法的优点是简单高效，但缺点是内存利用率低，只有一半的内存空间被利用。
4. **标记-整理算法**：该算法与标记-清除算法类似，但后续步骤不是直接对可回收对象进行清理，而是让所有存活的对象都向一端移动，然后直接清理掉端边界意外的内存。该算法可以解决内存碎片化问题，但移动对象需要额外的时间和空间。
5. **分代收集算法**：根据对象的存活周期的不同，将内存划分为几块。一般是把java堆分成新生代和老年代，这样就可以根据各个年代的特点采取最适当的收集算法。例如在新生代中主要采用复制算法，老年代中主要采用标记-清除或标记-整理算法。

#### 1.7.3、内存泄漏

​		内存泄漏是指在程序运行过程中，动态分配的内存空间未被及时释放，导致内存的浪费和程序性能的下降。内存泄漏的原因有很多，例如内存分配错误、程序逻辑错误或处理大量数据等。内存泄漏会导致程序性能下降，甚至程序崩溃，因此必须重视并及时处理。

内存泄漏的几种情况：

1. 静态集合类：如HashMap、LinkedList等等，如果这些为静态的那么他们的生命周期与JVM一致，则容器中的对象在程序结束之前不能被释放，从而造成内存泄漏。**长生命周期的对象持有短生命周期对象的引用，尽管短生命周期的对象不再使用，但是因为长生命周期对象持有它的引用而导致不能被回收**。
2. 单例模式：和静态集合类的原因类似，因为单例的静态特性。
3. 内部类持有外部类：一个外部类的实例对象的方法返回了一个内部类的实例对象，
4. 数据库、IO、网络等链接：如果数据、网络、IO链接等在操作完后没有进行关闭来释放连接的话就不会被垃圾回收器回收。
5. 变量不合理的作用域：一个变量的定义的作用范围大于其使用范围；没有及时地把对象设置为null。
6. 改变哈希值：当一个对象存储进HashSet集合以后就不能修改这个对象中的那些参与计算哈希值的字段了，修改了就会导致找不到对应的对象那么就无法从HashSet集合中单独删除对象。
7. 缓存泄漏：将大量数据对象放入缓存中。
8. 监听器和回调：如果客户端在你实现的API中注册回调，却没有显式的取消，那么就会积聚。

#### 1.7.4、内存溢出

​		内存溢出是指程序在运行过程中，向操作系统申请的内存空间已经用完，再次申请内存空间时无法获得需要的内存空间而导致程序崩溃的现象。内存溢出通常发生在动态分配内存空间的情况下，例如使用malloc()函数、new运算符等。

内存不够的原因：

1. Java虚拟机的堆内存设置不够。堆内存设置不合理需要手动设置堆内存。
2. 代码中创建了大量的大对象，并且长时间不能被垃圾收集器收集。				

#### 1.7.5、STW

​		Stop-the-World ，简称STW，指的是GC事件发生过程中，会产生应用程序的停顿。停顿产生时整个应用程序线程都会被暂停，没有任何响应，有点像卡死的感觉，这个停顿称为STW。

​		STW事件和采用哪款GC无关，所有的GC都有这个事件。

#### 1.7.6、并行与并发

并发，指的是多个事情，在同一时间段内同时发生了。 
并行，指的是多个事情，在同一时间点上同时发生了。

并发的多个任务之间是互相抢占资源的。 
并行的多个任务之间是不互相抢占资源的。

#### 1.7.7、安全点与安全区域

- 安全点

  ​		程序执行时并非在所有地方都能停顿下来开始 GC，只有在特定的位置才能停顿下来开始GC，这些位置称为“安全点（Safepoint）”。

  ​		Safe Point的选择很重要，如果太少可能导致GC等待的时间太长，如果太频繁可能导致运行时的性能问题。大部分指令的执行时间都非常短暂，通常会根据“是否具有让程序长时间执行的特征”为标准。比如：选择一些执行时间较长的指令作为Safe Point，如方法调用、循环跳转和异常跳转等。

- 安全区域

  ​		安全区域是指在一段代码片段中，对象的引用关系不会发生变化，在这个区域中的任何位置开始GC都是安全的。我们也可以把 Safe Region 看做是被扩展了的 Safepoint。

  ​		1、当线程运行到Safe Region的代码时，首先标识已经进入了Safe Region，如果这段时间内发生GC，JVM会忽略标识为Safe Region状态的线程；

  ​		2、当线程即将离开Safe Region时，会检查JVM是否已经完成GC，如果完成了，则继续运行，否则线程必须等待直到收到可以安全离开Safe Region的信号为止；

#### 1.7.8、强引用、弱引用、软引用、虚引用、终结器引用

- 强引用

  ​		在Java中最常见的引用类型就是强引用；当使用new创建一个新的对象，并赋值给一个变量的时候，这个变量就成为指向该对象的一个强引用。强引用的对象是可触及的，**垃圾收集器就永远不会回收掉被引用的对象**。

- 软引用

  ​		软引用是用来描述一些还有用，但非必需的对象。只被软引用关联着的对象，在系统将要发生内存溢出异常前，会把这些对象列进回收范围之中进行第二次回收，如果这次回收还没有足够的内存，才会抛出内存溢出异常。**内存不够才会被GC回收。**

- 弱引用

  ​		弱引用也是用来描述那些非必需对象，只被弱引用关联的对象只能生存到下一次垃圾收集发生为止。在系统GC时，只要发现弱引用，**不管系统堆空间使用是否充足，都会回收掉只被弱引用关联的对象**。

- 虚引用

  ​		一个对象是否有虚引用的存在，完全不会决定对象的生命周期。如果一个对象仅持有虚引用，那么它和没有引用几乎是一样的，**随时都可能被垃圾回收器回收；必须要和引用队列联合使用**。

- 终结器引用

  ​		它用以实现对象的finalize()方法，也可以称为终结器引用。

#### 1.7.9、垃圾回收器

##### 1.7.9.1、GC分类

- 按线程数分，可以分为串行垃圾回收器和并行垃圾回收器。
  1. 串行回收是指在同一时间段内只允许有一个CPU用于执行垃圾回收操作，此时工作线程暂停直到垃圾回收结束。
  2. 并行回收是指可以运行多个CPU同时执行垃圾回收，因此提升了吞吐量，不过并行回收和串行回收一样采用独占式，使用了STW机制。

- 按工作模式，可以分为并发式垃圾回收器和独占式垃圾回收器。
  1. 并发式垃圾回收器与应用程序线程交替工作，以尽可能减少应用程序的停顿时间。
  2. 独占式垃圾回收一旦运行，就停止应用程序中的所有用户线程，直到垃圾回收过程完全结束。
- 按碎片处理方式，分为压缩式垃圾回收器和非压缩式垃圾回收器
  1. 压缩式垃圾回收器会在回收完成后，对存活对象进行压缩整理，消除回收后的碎片。再分配对象空间使用指针碰撞。
  2. 非压缩式的垃圾回收器根据压缩式相关不进行压缩整理和消除碎片。再分配对象空间使用空闲的空间。
- 按工作的内存空间，分为年轻代垃圾回收器和老年代垃圾回收器。

##### 1.7.9.2、GC评估指标

- 吞吐量

  吞吐量是CPU用于运行用户代码的时间与CPU总消耗的时间的比值。**吞吐量=运行用户代码时间/（运行用户代码时间+垃圾收集时间）**

- 暂停时间

  暂停时间是指一个时间段内应用程序线程暂停，让GC线程执行运行状态。

这两个指标是互斥的，如果要吞吐量高那么久需要降低内存回收的执行频率，但是这样就会导致GC需要更长的暂停时间来执行内存回收，相反如果要暂停时间短，为了降低每次GC暂停时间只能频繁执行GC，这会引起年轻代内存缩减导致线程吞吐量下降。

##### 1.7.9.3、垃圾回收器种类

- 串行回收器：Serial、Serial Old
- 并行回收器：ParNew、Parallel、Scavenge、Parallel Old
- 并发回收器：CMS、G1

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/%E5%9E%83%E5%9C%BE%E5%9B%9E%E6%94%B6%E5%99%A8%E4%B9%8B%E9%97%B4%E7%9A%84%E5%85%B3%E7%B3%BB.png)

1. 串行回收Serial GC和Serial Old GC

   - Serial GC是最早的垃圾收集器。它采用复制算法、串行回收和STW机制的方式执行内存回收，主要回收年轻代的垃圾。
   - Serial Old收集器用于执行老年代的垃圾收集，它跟Serial同样采用了串行回收和STW机制，但是它使用的是标记-压缩算法。它是运行在Client模式下默认的老年代的垃圾回收器，主要有两个用途：第一与新生代的Parallel Scavenge配合使用；第二作为老年代CMS收集器的后备垃圾收集方案。
   - 主要是在单核CPU中发挥的作用很大。使用 -XX:+UseSerialGC 参数可以指定年轻代和老年代都使用串行收集器。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Serial%20GC.png)

2. 并行回收ParNew GC

   - ParNew GC是Serial GC的多线程版本，只能处理新生代。它除了采用并行回收方式外同样也采用了复制算法和STW机制。
   - 目前只有ParNew GC和CMS收集器配合
   - 使用-XX:+UseParNewGC"手动指定使用ParNew收集器执行内存回收任务。它表示年轻代使用并行收集器，不影响老年代。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/ParNew%20GC.png)

3. 吞吐量优先 Parallel GC

   - Parallel Scavenge收集器同样也采用了复制算法、并行回收和STW机制；与ParNew GC不同，它的目标是达到一个可控制的吞吐量，也就是自适应调节策略。
   - 主要适合在后台运算的不需要太多交互的任务。
   - Parallel Old收集器采用了标记-压缩算法，但同样也是基于并行回收和STW机制。
   - Parallel GC和Parallel Old搭配使用
   - Java 8中默认就是Parallel GC，下面两个只用开启一个，另一个也被启动。
   - -XX:+UseParallelGC  手动指定年轻代使用Parallel并行收集器执行内存回收任务。
   - -XX:+UseParallelOldGC  手动指定老年代都是使用并行回收收集器。
   - -XX:ParallelGCThreads 设置年轻代并行收集器的线程数。最好与CPU数量相等，以避免过多的线程数影响垃圾收集性能。当CPU 数量小于8个， ParallelGCThreads 的值等于CPU 数量。当CPU数量大于8个，ParallelGCThreads 的值等于3+[5*CPU_Count]/8] 。
   - -XX:MaxGCPauseMillis 设置垃圾收集器最大停顿时间(即STW的时间)。单位是毫秒。
   - -XX:GCTimeRatio 垃圾收集时间占总时间的比例（= 1 / (N + 1))。用于衡量吞吐量的大小。取值范围（0,100）。默认值99，也就是垃圾回收时间不超过1%。与上面参数有矛盾，暂停时间越长这个参数就越容易超过设定的比例。
   - -XX:+UseAdaptiveSizePolicy  设置Parallel Scavenge收集器具有自适应调节策略。在这种模式下，年轻代的大小、Eden和Survivor的比例、晋升老年代的对象年龄等参数会被自动调整，已达到在堆大小、吞吐量和停顿时间之间的平衡点。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Parallel%20GC.png)

4. 低延迟CMS

   - 在jdk1.5时由Hotspot推出的强交互应用中最有意义的收集器CMS，它是第一个真正意义上的并发收集器，实现了垃圾收集线程和用户线程同时工作。
   - CMS的垃圾收集算法采用标记-清楚算法和采用了STW机制。
   - 它只能选择ParNew和Serial收集器中的一个配合使用。
   - 目前CMS GC是使用非常广泛的，直到G1的出现。
   - 并发清理和并发标记是最耗时的，初始标记和重新标记会STW。

   ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/CMS%20GC.png)

   - CMS收集过程

     1. 初始标记阶段：在这个阶段会**执行STW出现短暂的暂停**，这个阶段仅仅是标记处GC Roots能直接关联到的对象，一旦标记完成就会恢复被暂停的应用线程，由于**关联对象比较小所以速度非常快**。
     2. 并发标记阶段：从GC Roots的直接关联对象开始遍历整个对象的过程，这个过程耗时比较长但是**不需要暂停用户线程**，可以与垃圾收集线程一起并发运行。
     3. 重新标记阶段：由于在并发标记阶段中，程序的工作线程会和垃圾收集线程同时运行，因此为了修正并发标记期间，应用户线程继续运行导致标记产生变动的那一部分对象的标记记录。这个**阶段的暂停时间比初始标记时间要长一点**。
     4. 并发清理阶段：此阶段清理删除掉标记阶段判断已经死亡的对象，释放内存空间。

   - 优缺点

     优：并发收集、低延迟

     缺：会产生内存碎片、CMS收集对CPU资源非常敏感、CMS收集器无法处理浮动的垃圾。

   - 参数

     ​		-XX:+UseConcMarkSweepGC 手动指定使用CMS 收集器执行内存回收任务。开启该参数后会自动将-XX:+UseParNewGC打开。即：ParNew(Young区用)+CMS(Old区用)+Serial Old的组合。

     ​		-XX:CMSlnitiatingOccupanyFraction 设置堆内存使用率的阈值，一旦达到该阈值，便开始进行回收。jdk5及以前默认68到达（68%）会执行一次CMS，jdk6及以后默认92。

     ​		-XX:+UseCMSCompactAtFullCollection 用于指定在执行完Full GC后对内存空间进行压缩整理，以此避免内存碎片的产生。

     ​		-XX:CMSFullGCsBeforeCompaction 设置在执行多少次Full GC后对内存空间进行压缩整理。

     ​		-XX:ParallelCMSThreads 设置CMS的线程数量。
     CMS 默认启动的线程数是（ParallelGCThreads+3)/4，ParallelGCThreads 是年轻代并行收集器的线程数。当

   > **想要最小化地使用内存和并行开销，请选Serial GC**
   >
   > **想要最大化应用程序的吞吐量，请选Parallel GC**
   >
   > **想要最小化GC的中断或停顿时间，请选CMS GC**
   >
   > jdk9中CMS被弃用，14被删除。

5. 区域化分代G1 GC

   ​		G1是一个并行回收器，它吧堆内存分割为很多不相关的区域，使用不同的Region来表示Eden、幸存者0区、幸存者1区、老年代等；G1是跟踪各个Region里面的垃圾堆积的价值大小在后台维护一个优先列表，每次根据允许的收集时间，优先回收价值最大的Region；这个方式侧重于回收垃圾最大量的区间，因此也叫垃圾优先 （Garbage First）。

   - G1是一块面向服务端应用的垃圾收集器，主要正对多核CPU及大量内存的机器，以极高概率满足GC停顿时间的同时，还具备高吞吐量。
   - 特点
     1. 并行性：G1在回收期间，可以多个GC线程同时工作，此时用户线程STW。
     2. 并发性：G1拥有与应用线程交替执行的能力，部分工作可以喝应用程序同时执行。
     3. 分代收集：G1也是属于分代型垃圾收集器，它会分年轻代喝老年代，老年代也有Eden区和Survivor区。将堆空间分为若干区域，这些区域包含了逻辑上的年轻代和老年代；它同时兼顾年轻代喝老年代。
     4. 空间整合：G1将内存划分为若干Region，内存的回收是以Region为基本单位。R**egion之间是复制算法**，但是整体实际上可看作**标记-压缩算法**，两种算法都可以避免内存碎片。这种有利于程序长时间运行，分配大对象时不会因为找不到连续的内存空间而触发GC。
     5. 可预测的停顿时间：**能让使用者明确指定在一个长度为M毫秒的时间片段内，消耗在垃圾收集上的时间不得超过N毫秒**，由于是选分区进行内存回收，缩小了范围全局的停顿时间也得到比较好的控制。
   - 缺点：在用户程序运行过程中无论是垃圾收集产生的内存占用还是程序运行时的额外执行负载都要比CMS高。

   > **在小内存应用上CMS的表现大概率会优于G1，而G1在大内存应用上则发挥其优势。平衡点在6-8GB之间。**

   - 参数

     -XX：+UseG1GC   手动指定使用G1收集器执行内存回收任务。

     -XX:G1HeapRegionSize  设置每个Region的大小。值是2的幂，范围是1MB到32MB之间，目标是根据最小的Java堆大小划分出约2048个区域。默认是堆内存的1/2000。

     -XX:MaxGCPauseMillis   设置期望达到的最大GC停顿时间指标(JVM会尽力实现，但不保证达到)。默认值是200ms

     -XX:ParallelGCThread   设置STW时GC线程数的值。最多设置为8

     -XX:ConcGCThreads   设置并发标记的线程数。将n设置为并行垃圾回收线程数(ParallelGCThreads)的1/4左右。

     -XX:InitiatingHeapOccupancyPercent  设置触发并发GC周期的Java堆占用率阈值。超过此值，就触发GC。默认值是45。

   - 开启步骤

     1. 开启G1垃圾收集器
     2. 设置堆的最大内存
     3. 设置最大的停顿时间

   - 使用场景：面向服务器、针对大内存、多处理器的机器

   - 分区Region

     ​		使用 G1 收集器时，它将整个Java堆划分成约2048个大小相同的独立Region块，每个Region块大小根据堆空间的实际大小而定，整体被控制在1MB到32MB之间，且为2的N次幂，即1MB,2MB,4MB,8MB,16MB,32MB。可以通过-XX:G1HeapRegionSize设定。所有的Region大小相同，且在JVM生命周期内不会被改变。

     ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/G1%20GC.png)

     ​		一个 region 有可能属于 Eden，Survivor 或者 Old/Tenured 内存区域。图中的 E 表示该region属于Eden内存区域，S表示属于Survivor内存区域，O表示属于Old内存区域。图中空白的表示未使用的内存空间。
     G1 垃圾收集器还增加了一种新的内存区域，叫做 Humongous 内存区域，如图中的 H 块。主要用于存储大对象，如果超过1.5个region，就放到H。

     ​		G1划分了一个Humongous区，它用来专门存放大对象。如果一个H区装不下一个大对象，那么G1会寻找连续的H区来存储。为了能找到连续的H区，有时候不得不启动Full GC。G1的大多数行为都把H区作为老年代的一部分来看待。

   - G1垃圾回收过程

     1. 年轻代GC：程序在运行过程中不断创建对象到Eden区，**当Eden空间耗尽时，G1会启动一次年轻代垃圾回收过程。**年轻代垃圾回收只会回收Eden区和Survivor区。

        首先G1停止应用程序STW，G1创建回收集，回收集是指需要被回收的内存分段的集合，年轻代回收过程的回收集包含年轻代Eden区和Survivor区所有的内存分段。

        - **第一阶段，扫描根**。根是指static变量指向的对象，正在执行的方法调用链条上的局部变量等。根引用连同RSet记录的外部引用作为扫描存活对象的入口。
        - **第二阶段，更新RSet。**此阶段完成后，RSet可以准确的反映老年代对所在的内存分段中对象的引用。
        - **第三阶段，处理RSet。**识别被老年代对象指向的Eden中的对象，这些被指向的Eden中的对象被认为是存活的对象
        - **第四阶段，复制对象。**对象树被遍历，Eden区内存段中存活的对象会被复制到Survivor区中空的内存分段，Survivor区内存段中存活的对象如果年龄未达阈值，年龄会加1，达到阀值会被会被复制到Old区中空的内存分段。如果Survivor空间不够，Eden空间的部分数据会直接晋升到老年代空间。
        - **第五阶段，处理引用。**处理Soft，Weak，Phantom，Final，JNI Weak 等引用。最终Eden空间的数据为空，GC停止工作，而目标内存中的对象都是连续存储的，没有碎片，所以复制过程可以达到内存整理的效果，减少碎片。

     2. 并发标记过程：

        - **初始标记阶段**：标记从根节点直接可达的对象。这个阶段是STW的，并且会触发一次年轻代GC。
        - **根区域扫描**：G1 GC扫描Survivor区直接可达的老年代区域对象，并标记被引用的对象。这一过程必须在young GC之前完成。
        - **并发标记**：在并发标记阶段，若发现区域对象中的所有对象都是垃圾，那这个区域会被立即回收。
        - **再次标记**：于应用程序持续进行，需要修正上一次的标记结果。是STW的。G1中采用了比CMS更快的初始快照算法:snapshot-at-the-beginning (SATB)。
        - **独占清理**：计算各个区域的存活对象和GC回收比例，并进行排序，识别可以混合回收的区域。为下阶段做铺垫。是STW的。这个阶段不会去做垃圾的收集。
        - **并发清理阶段**：识别并清理完全空闲的区域。

     3. 混合回收

        ​		当越来越多的对象晋升到老年代old region时，为了避免堆内存被耗尽，虚拟机会触发一个混合的垃圾收集器，即Mixed GC（并不是Full GC），该算法并不是一个Old GC，除了回收整个Young Region，还会回收一部分的Old Region。**是一部分老年代，而不是全部老年代**。

        ​		**混合回收的回收集（Collection Set）包括八分之一的老年代内存分段，Eden区内存分段，Survivor区内存分段**。混合回收的算法和年轻代回收的算法完全一样，只是回收集多了老年代的内存分段。

        ​		由于老年代中的内存分段默认分8次回收，G1会优先回收垃圾多的内存分段。垃圾占内存分段比例越高的，越会被先回收。并且有一个阈值会决定内存分段是否被回收，-XX:G1MixedGCLiveThresholdPercent，默认为65%，意思是垃圾占内存分段比例要达到65%才会被回收。如果垃圾占比太低，意味着存活的对象占比高，在复制的时候会花费更多的时间。
        ​		混合回收并不一定要进行8次。有一个阈值-XX:G1HeapWastePercent，默认值为10%，意思是允许整个堆内存中有10%的空间被浪费，意味着如果发现可以回收的垃圾占堆内存的比例低于10%，则不再进行混合回收。因为GC会花费很多的时间但是回收到的内存却很少。

     4. Full GC

        ​		如果上面都不能正常工作了，G1为停止应用程序STW，**使用单线程的内存回收算法进行垃圾回收，性能会非常差，应用程序停顿时间会很长。**

        导致G1Full GC的原因可能有两个：

        - Evacuation（回收阶段）的时候没有足够的to-space来存放晋升的对象；

        - 并发处理过程完成之前空间耗尽。

   - G1优化

     1. 避免使用-Xmn或-XX:NewRatio等相关选项显式设置年轻代大小
     2. 定年轻代的大小会覆盖暂停时间目标
     3. G1 GC的吞吐量目标是90%的应用程序时间和10%的垃圾回收时间
     4. 评估G1 GC的吞吐量时，暂停时间目标不要太严苛。目标太过严苛表示你愿意承受更多的垃圾回收开销，而这些会直接影响到吞吐量

#### 1.7.10、GC使用场景

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/GC%E4%BD%BF%E7%94%A8%E5%9C%BA%E6%99%AF.png)



## 2、JVM性能调优

### 2.1、JVM性能监控

#### 2.1.1、常用命令

##### 2.1.1.1、**jps**

jps [options] [hostid]     查看正在运行的java进程

options参数：

-q :仅仅显示LVMID (local virtual machine id)，即本地虚拟机唯一id。不显示主类的名称等

-l: 输出应用程序主类的全类名 或 如果进程执行的是jar包，则输出jar完整路径

-m: 输出虚拟机进程启动时传递给主类main()的参数

-v: 列出虚拟机进程启动时的JVM参数。 比如：-Xms20m -Xmx50m是启动程序指定的jvm参数。

hostid参数：RMI注册表中注册的主机名。

##### 2.1.1.2、**jstat**

jstat(JVM Statistics Monitoring Tool)：用于监视虚拟机各种运行状态信息的命令行工具。它可以显示本地或者远程虚拟机进程中的类装载、内存、垃圾收集、JIT编译等运行数据。

jstat -<option> [-t] [-h<lines>] <vmid> [<interval> [<count>]]       查看JVM统计信息

- option参数：

  类装载相关，-class：显示ClassLoader的相关信息：类的装载、卸载数量、总空间、类装载所消耗的时间等。

  垃圾回收相关：

  -gc：显示与GC相关的堆信息。包括Eden区、两个Survivor区、老年代、永久代等的容量、已用空间、GC时间合计等信息。

  - S0C是第一个幸存者区的大小（字节）；

  - S1C是第二个幸存者区的大小（字节）；

  - S0U是第一个幸存者区已使用的大小（字节）；

  - S1U是第二个幸存者区已使用的大小（字节）；

  - EC是Eden空间的大小（字节）；

  - EU是Eden空间已使用大小（字节）；

  - OC是老年代的大小（字节）；

  - OU是老年代已使用的大小（字节）；

  - MC是方法区的大小；

  - MU是方法区已使用的大小；

  - CCSC是压缩类空间的大小；

  - CCSU是压缩类空间已使用的大小；

  - YGC是指从应用程序启动到采样时young gc次数；

  - YGCT是指从应用程序启动到采样时young gc消耗的时间（秒）；

  - FGC是指从应用程序启动到采样时full gc次数；

  - FGCT是指从应用程序启动到采样时full gc消耗的时间（秒）；

  - GCT是指从应用程序启动到采样时gc的总时间。

    -gccapacity：显示内容与-gc基本相同，但输出主要关注Java堆各个区域使用到的最大、最小空间。
    -gcutil：显示内容与-gc基本相同，但输出主要关注已使用空间占总空间的百分比。
    -gccause：与-gcutil功能一样，但是会额外输出导致最后一次或当前正在发生。

- interval参数：用于指定输出统计数据的周期，单位为毫秒。即：查询间隔

- count参数：用于指定查询的总次数

- -t参数：可以在输出信息前加上一个Timestamp列，显示程序的运行时间。单位：秒

- -h参数：可以在周期性数据输岀时，输出多少行数据后输出一个表头信息

> jstat还可以用来判断是否出现内存泄漏。
>
> 第1步：
> 在长时间运行的 Java 程序中，我们可以运行jstat命令连续获取多行性能数据，并取这几行数据中 OU 列（即已占用的老年代内存）的最小值。
> 第2步：
> 然后，我们每隔一段较长的时间重复一次上述操作，来获得多组 OU 最小值。如果这些值呈上涨趋势，则说明该 Java 程序的老年代内存已使用量在不断上涨，这意味着无法回收的对象在不断增加，因此很有可能存在内存泄漏。

##### 2.1.1.3、jinfo

jinfo  [ options ] pid    实时查看和修改JVM配置参数

pid:java 进程ID 必须要加上

- jinfo -sysprops PID：可以查看由System.getProperties()取得的参数
- jinfo -flags PID：查看曾经赋过值的一些参数
- jinfo -flag 具体参数 PID：查看某个java进程的具体参数的值
- java -XX:+PrintFlagsInitial：查看所有JVM参数启动的初始值
- java -XX:+PrintFlagsFinal：查看所有JVM参数的最终值
- java -XX:+PrintCommandLineFlags：查看那些已经被用户或者JVM设置过的详细的XX参数的名称和值

##### 2.1.1.4、jmap

jmap(JVM Memory Map)：作用一方面是获取dump文件（堆转储快照文件，二进制文件），它还可以获取目标Java进程的内存相关信息，包括Java堆各区域的使用情况、堆中对象的统计信息、类加载信息等。

导出内存映像文件&内存使用情况。

jmap [option] <pid>  
jmap [option] <executable <core>
jmap [option] [server_id@]<remote server IP or hostname>

option参数：

![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/jmap%20option.png)

- 导出内存镜像文件

  手动：

  jmap -dump:format=b,file=<filename.hprof> <pid>

  jmap -dump:live,format=b,file=<filename.hprof> <pid>

  自动：

  -XX:+HeapDumpOnOutOfMemoryError

  -XX:HeapDumpPath=<filename.hprof>

- 显示堆内存相关信息

  jmap -heap pid

  -jmap -histo pid

- 查看系统的ClassLoader信息

  jmap -permstat pid

- 査看堆积在finalizer队列中的对象

  jmap -finalizerinfo

##### 2.1.1.5、jhat

jhat [option] [dumpfile]    JDK自带堆分析工具

option参数：

- -stack false|true  关闭 | 打开对象分配调用栈跟踪
- -refs false|true  关闭 | 打开对象引用跟踪
- -port port-number  设置jhat HTTP Server的端口号，默认7000
- -exclude exclude-file 执行对象查询时需要排除的数据成员列表文件
- -baseline exclude-file  指定一个基准堆转储
- -debug int  设置debug级别
- -version  启动后显示版本信息就退出
-  -J<flag>   传入启动参数，比如-J -Xmx512m

##### 2.1.1.6、jstack

jstack option pid    打印JVM中线程快照

​		jstack(JVM Stack Trace)：用于生成虚拟机指定进程当前时刻的线程快照(虚拟机堆栈跟踪)。 线程快照就是当前虚拟机内指定进程的每一条线程正在执行的方法堆栈的集合。

​		生成线程快照的作用：可用于定位线程出现长时间停顿的原因，如线程间死锁、死循环、请求外部资源导致的长时间等待等问题。这些都是导致线程长时间停顿的常见原因。当线程出现停顿时，就可以用jstack显示各个线程调用的堆栈情况。

在thread dump中，要留意下面几种状态 

- 死锁，Deadlock（重点关注） 
- 等待资源，Waiting on condition（重点关注） 
- 等待获取监视器，Waiting on monitor entry（重点关注） 
- 阻塞，Blocked（重点关注） 
- 执行中，Runnable
- 暂停，Suspended
- 对象等待中，Object.wait() 或 TIMED_WAITING
- 停止，Parked

#### 2.1.2、诊断工具

- JDK自带的工具
  1. jconsole：JDK自带的可视化监控工具。查看Java应用程序的运行概况、监控堆信息、永久区（或元空间）使用情况、类加载情况等。位置：jdk\bin\jconsole.exe
  2. Visual VM:Visual VM是一个工具，它提供了一个可视界面，用于查看Java虚拟机上运行的基于Java技术的应用程序的详细信息。位置：jdk\bin\jvisualvm.exe
  3. JMC:Java Mission Control，内置Java Flight Recorder。能够以极低的性能开销收集Java虚拟机的性能数据。
- 第三方工具
  1. MAT: MAT(Memory Analyzer Tool)是基于Eclipse的内存分析工具，是一个快速、功能丰富的Java heap分析工具，它可以帮助我们查找内存泄漏和减少内存消耗。
  2. JProfiler：商业软件，需要付费。功能强大。

##### 2.1.2.1、jConsole

​		在JDK中自带的java监控和管理控制台。用于对JVM中内存、线程和类等的监控，是一个基于JMX(java management extensions)的GUI性能监控工具。官网：https://docs.oracle.com/javase/7/docs/technotes/guides/management/jconsole.html

- 位置：jdk\bin\jconsole.exe
- 三种连接方式：
  1. Local：使用JConsole连接一个正在本地系统运行的JVM,并且执行程序的和运行JConsole的需要
     是同一个用户。JConsole使用文件系统的授权通过RMI连接器连接到平台的MBean服务器上。
     这种从本地连接的监控能力只有Sun的JDK具有。
  2. Remote：使用下面的URL通过RMI连接器连接到一个JMX代理，service:jmx:rmi:///jndi/rmi://hostName:portNum/jmxrmi。JConsole为建立连接，需要在环境变量中设置mx.remote.credentials来指定用户名和密码，从而进行授权。
  3. Advanced：使用一个特殊的URL连接JMX代理。一般情况使用自己定制的连接器而不是RMI提供的连接器
     来连接JMX代理，或者是一个使用JDK1.4的实现了JMX和JMX Rmote的应用。
- 主要作用
  1. 监控内存
  2. 监控线程
  3. 监控死锁
  4. 类加载与虚拟机信息

##### 2.1.2.2、Visual VM

​		Visual VM是一个功能强大的多合一故障诊断和性能监控的可视化工具。它集成了多个JDK命令行工具，使用Visual VM可用于显示虚拟机进程及进程的配置和环境信息(jps,jinfo)，监视应用程序的CPU、GC、堆、方法区及线程的信息(jstat、jstack)等，甚至代替JConsole。

- 位置：jdk\bin\jvisualvm.exe

- 安装插件VisualGC

- 链接方式

  1. 本地连接：监控本地Java进程的CPU、类、线程等

  2. 远程连接：

     1-确定远程服务器的ip地址

     2-添加JMX（通过JMX技术具体监控远端服务器哪个Java进程

     3-修改bin/catalina.sh文件，连接远程的tomcat

     4-在.../conf中添加jmxremote.access和jmxremote.password文件

     5-将服务器地址改为公网ip地址

     6-设置阿里云安全策略和防火墙策略

     7-启动tomcat，查看tomcat启动日志和端口监听

     8-JMX中输入端口号、用户名、密码登录

- 主要功能

  1-生成/读取堆内存快照

  2-查看JVM参数和系统属性

  3-查看运行中的虚拟机进程

  4-生成/读取线程快照

  5-程序资源的实时监控

  6-JMX代理连接、远程环境监控、CPU分析和内存分析

##### 2.1.2.3、eclipse MAT

​		MAT(Memory Analyzer Tool)工具是一款功能强大的Java堆内存分析器。可以用于查找内存泄漏以及查看内存消耗情况。

- 获取堆dump文件：MAT可以分析heap dump文件。在进行内存分析时，只要获得了反映当前设备内存映像的hprof文件，通过MAT打开就可以直观地看到当前的内存信息。内存信息包括：

  1. 所有的对象信息，包括对象实例、成员变量、存储于栈中的基本类型值和存储于堆中的其他对象的引用值。
  2. 所有的类信息，包括classloader、类名称、父类、静态变量等
  3. GCRoot到所有的这些对象的引用路径
  4. 线程信息，包括线程的调用栈及此线程的线程局部变量（TLS）

- 优点：**能够快速为开发人员生成内存泄漏报表，方便定位问题和分析问题。**

- 分析堆dump文件

  1. histogram：展示了各个类的实例数目以及这些实例的Shallow heap 或Retainedheap的总和

  2. thread overview：查看系统中的Java线程、查看局部变量的信息

  3. with outgoing/incoming  references: 获得对象相互引用的关系

  4. 浅堆：浅堆(Shallow Heap)是指一个对象所消耗的内存。在32位系统中，一个对象引用会占据 4个字节，一个int类型会占据4个字节，long型变量会占据8个字节，每个对象头需要占用8 个字节。根据堆快照格式不同，对象的大小可能会向8字节进行对齐。

     以String为例：2个int值共占8字节，对象引用占用4字节，对象头8字节，合计20字节，向8字节对 齐，故占24字节。（jdk7中）

  5. 深堆：深堆是指对象的保留集中所有的对象的浅堆大小之和。

##### 2.1.2.4、Arthas

官方文档：https://arthas.aliyun.com/zh-cn/

- 工程目录

  arthas-agent：基于JavaAgent技术的代理
  bin：一些启动脚本
  arthas-boot：Java版本的一键安装启动脚本
  arthas-client：telnet client代码
  arthas-common：一些共用的工具类和枚举类
  arthas-core：核心库，各种arthas命令的交互和实现
  arthas-demo：示例代码
  arthas-memorycompiler：内存编绎器代码，Fork from https://github.com/skalogs/SkaETL/tree/master/compiler
  arthas-packaging：maven打包相关的
  arthas-site：arthas站点
  arthas-spy：编织到目标类中的各个切面
  static：静态资源
  arthas-testcase：测试

- 启动：java -jar arthas-boot.jar

- 查看运行的 java 进程信息

  jps -mlvV 
  ps -ef| grep java

- 筛选java进程信息

    jps -mlvV | grep [xxx]

- 查看日志
  cat ~/logs/arthas/arthas.log

- 查看帮助

  java -jar arthas-boot.jar -h

- 相关指令

  1. 基础指令

     ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Arthas%E7%9B%B8%E5%85%B3%E6%8C%87%E4%BB%A4.png)

  2. JVM相关

     - dashboard命令：可以查看当前系统的实时数据面板。展示当前tomcat的多线程状态、JVM各区域、GC情况等信息，输入 Q 或者 Ctrl+C 可以退出dashboard命令常用参数：-i1000：每次执行间隔时间，这是单位是毫秒； -n4：执行多少次dashboard，不指定的话会一直刷新

       ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Arthas%E7%9B%B8%E5%85%B3%E6%8C%87%E4%BB%A4jvm.png)

       ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Arthas%E7%9B%B8%E5%85%B3%E6%8C%87%E4%BB%A4jvm1.png)

     - thread命令：查看当前 JVM 的线程堆栈信息，常用参数：

       ​	无 ：显示所有线程的状态信息
       ​     -b  显示当前处于BLOCKED状态的线程，可以排查线程锁的问题
       ​     -i 5000 查看在接下来的多长时间内统计cpu利用率，单位毫秒
       ​     -n 5 查看cpu占用率前5的线程的堆栈信息
       ​     <thread_id> 直接跟着线程id，可以看到指定thread的堆栈信息

     - jvm命令：查看jvm详细的性能数据

  3. class/classloader相关

     ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Arthas%E7%9B%B8%E5%85%B3%E6%8C%87%E4%BB%A4class.png)

     - sc命令：查看 JVM 已加载的类信息https://arthas.aliyun.com/doc/sc
       常用参数：
          class-pattern  类名表达式匹配
          -d  输出当前类的详细信息，包括这个类所加载的原始文件来源、类的声明、加载的ClassLoader等详细信息。如果一个类被多个ClassLoader所加载，则会出现多次
          -E  开启正则表达式匹配，默认为通配符匹配
          -f  输出当前类的成员变量信息（需要配合参数-d一起使用）
          -x  指定输出静态变量时属性的遍历深度，默认为 0，即直接使用 toString 输出

     - sm命令:查看已加载类的方法信息
       https://arthas.aliyun.com/doc/sm
       sm 命令只能看到由当前类所声明 (declaring) 的方法，父类则无法看到。
       常用参数：
          class-pattern  类名表达式匹配
          method-pattern  方法名表达式匹配
          -d  展示每个方法的详细信息
          -E  开启正则表达式匹配，默认为通配符匹配
     - jad命令：反编译指定已加载类的源码
       https://arthas.aliyun.com/doc/jad
       在 Arthas Console 上，反编译出来的源码是带语法高亮的，阅读更方便
       当然，反编译出来的 java 代码可能会存在语法错误，但不影响你进行阅读理解
       编译java.lang.String
     - mc命令：Memory Compiler/内存编译器，编译.java文件生成.class
       https://arthas.aliyun.com/doc/mc
     - classloader命令：查看 classloader 的继承树，urls，类加载信息
       https://arthas.aliyun.com/doc/classloader
       了解当前系统中有多少类加载器，以及每个加载器加载的类数量，帮助您判断是否有类加载器泄漏。
       常用参数：
          -t : 查看ClassLoader的继承树
          -l : 按类加载实例查看统计信息
          -c : 用classloader对应的hashcode 来查看对应的jar urls

  4. monitor/watch/trace相关

     ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Arthas%E7%9B%B8%E5%85%B3%E6%8C%87%E4%BB%A4%E7%9B%91%E6%8E%A7.png)

     - monitor命令：方法执行监控
       对匹配 class-pattern／method-pattern的类、方法的调用进行监控。涉及方法的调用次数、执行时间、失败率等
       https://arthas.aliyun.com/doc/monitor
       monitor 命令是一个非实时返回命令

       常用参数：
          class-pattern  类名表达式匹配
          method-pattern  方法名表达式匹配
          -c  统计周期，默认值为120秒

     - watch命令:方法执行数据观测 
       https://arthas.aliyun.com/doc/watch
       让你能方便的观察到指定方法的调用情况。能观察到的范围为：返回值、抛出异常、入参，通过编写 groovy 表达式进行对应变量的查看。
       常用参数：
          class-pattern  类名表达式匹配
          method-pattern  方法名表达式匹配
          express  观察表达式
          condition-express  条件表达式
          -b  在方法调用之前观察(默认关闭)
          -e  在方法异常之后观察(默认关闭)
          -s  在方法返回之后观察(默认关闭)
          -f  在方法结束之后(正常返回和异常返回)观察 (默认开启)
          -x  指定输出结果的属性遍历深度，默认为0

     - trace命令：方法内部调用路径，并输出方法路径上的每个节点上耗时
       https://arthas.aliyun.com/doc/trace

       参数说明
          class-pattern  类名表达式匹配
          method-pattern  方法名表达式匹配
          condition-express  条件表达式

     - stack命令：输出当前方法被调用的调用路径
       https://arthas.aliyun.com/doc/stack
       常用参数
          class-pattern  类名表达式匹配
          method-pattern  方法名表达式匹配
          condition-express  条件表达式
          -n  执行次数限制

     - tt命令：方法执行数据的时空隧道，记录下指定方法每次调用的入参和返回信息，并能对这些不同的时间下调用进行观测。
       https://arthas.aliyun.com/doc/tt
       TimeTunnel的缩写
       常用参数：
          -t  表明希望记录下类 *Test 的 print 方法的每次执行情况。
          -n 3  指定你需要记录的次数，当达到记录次数时 Arthas 会主动中断tt命令的记录过程，避免人工操作无法停止的情况。
          -s  筛选指定方法的调用信息
          -i  参数后边跟着对应的 INDEX 编号查看到它的详细信息
          -p  重做一次调用  通过 --replay-times 指定 调用次数，通过 --replay-interval 指定多次调用间隔(单位ms, 默认1000ms)

### 2.2、分析GC日志

#### 2.2.1、GC日志分类

- Minor GC

  [名称：GC前内存占用->GC后内存占用（该区内存大小）]

  ```shell
  [GC (Allocation Failure) [PSYoungGen: 31744K->2192K(36864K)] 31744K->2200K(121856K), 0.0139308 secs] [Times: user=0.05 sys=0.01, real=0.01 secs] 
  ```

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Minor%20GC%E6%97%A5%E5%BF%97.png)

- Full GC

  [名称：GC前内存占用->GC后内存占用（该区内存大小）]

  ```shell
  [Full GC (Metadata GC Threshold) [PSYoungGen: 5104K->0K(132096K)] [ParOldGen: 416K->5453K(50176K)] 5520K->5453K(182272K), [Metaspace: 20637K->20637K(1067008K)], 0.0245883 secs] [Times: user=0.06 sys=0.00, real=0.02 secs] 
  ```

  ![](https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/jvm/Full%20GC%E6%97%A5%E5%BF%97.png)

#### 2.2.2、GC日志结构

垃圾收集器：

- 使用Serial收集器在新生代的名字是Default New Generation，因此显示的是"[DefNew"
- 使用ParNew收集器在新生代的名字会变成"[ParNew",意思是"Parallel New Generation"
- 使用Parallel Scavenge收集器在新生代的名字是"[PSYoungGen",这里的JDK1.7使用的就是PSYoungGen、
- 使用Parallel Old Generation收集器在老年代的名字是"[ParOldGen"
- 使用G1收集器的话，会显示为"garbage-first heap"

> **Allocation Failure**
> **表明本次引起GC的原因是因为在年轻代中没有足够的空间能够存储新的数据了。**

GC前后情况：

[PSYoungGen: 5986K->696K(8704K)] 5986K->704K(9216K)

中括号内：GC回收前年轻代堆大小，回收后大小，（年轻代堆总大小）
括号外：GC回收前年轻代和老年代大小，回收后大小，（年轻代和老年代总大小）

GC日志中有三个时间：user，sys和real：

- user – 进程执行用户态代码（核心之外）所使用的时间。这是执行此进程所使用的实际 CPU 时间，其他进程和此程阻塞的时间并不包括在内。在垃圾收集的情况下，表示 GC 线程执行所使用的 CPU 总时间。
- sys – 进程在内核态消耗的 CPU 时间，即在内核执行系统调用或等待系统事件所使用的 CPU 时间
- real – 程序从开始到结束所用的时钟时间。这个时间包括其他进程使用的时间片和进程阻塞的时间（比如等待 I/O 完成）。对于并行gc，这个数字应该接近（用户时间+系统时间）除以垃圾收集器使用的线程数。

> 由于多核的原因，一般的GC事件中，real time是小于sys + user time的，因为一般是多个线程并发的去做GC，所以real time是要小于sys+user time的。如果real>sys+user的话，则你的应用可能存在下列问题：IO负载非常重或者是CPU不够用。

#### 2.2.3、GC日志案例分析

##### 2.2.3.1、Minor GC日志分析

```
2020-11-20T17:19:43.265-0800: 0.822: [GC (ALLOCATION FAILURE) [PSYOUNGGEN: 76800K->8433K(89600K)] 76800K->8449K(294400K), 0.0088371 SECS] [TIMES: USER=0.02 SYS=0.01, REAL=0.01 SECS] 
```

- 日志打印时间。
- **0.822：**GC发生时，Java虚拟机启动依赖经过的秒数。
- **GC (ALLOCATION FAILURE)：**发生了一次垃圾回收，这是一次Minor GC 。它不区分新生代 GC 还是老年代 GC，括号里的内容是gc发生的原因，这里的Allocation Failure的原因是新生代中没有足够区域能够存放需要分配的数据而失败。
- **PSYoungGen：**表示GC发生的区域，区域名称与使用的GC 收集器是密切相关的。详细见2.2.2中的垃圾收集器。
- **76800K->8433K(89600K)：**GC 前该内存区域已使用容量 - > GC 后该区域容量 （该区域总容量），如果是新生代，总容量则会显示整个新生代内存的9/10，即 eden+from/to 区，如果是老年代，总容量则是全部内存大小，无变化。
- **76800K->8449K(294400K)：**在显示完区域容量GC的情况之后，会接着显示整个堆内存区域的GC情况：GC 前堆内存已使用容量 - > GC 堆内存容量（堆内存总容量）。堆内存总容量 = 9/10 新生代+ 老年代 < 初始化的内存大小。
- **0.0088371 SECS：**整个GC所花费的时间，单位是秒
- user：指的是CPU工作在用户态所花费的时间，sys：指的是CPU工作在内核态所花费的时间，real：指的是在此次GC事件中所花费的总时间。

##### 2.2.3.2、Full GC日志分析

```shell
2020-11-20T17:19:43.794-0800: 1.351: [FULL GC (METADATA GC THRESHOLD) [PSYOUNGGEN: 10082K->0K(89600K)] [PAROLDGEN: 32K->9638K(204800K)] 10114K->9638K(294400K),
 [METASPACE: 20158K->20156K(1067008K)], 0.0285388 SECS] [TIMES: USER=0.11 SYS=0.00, REAL=0.03 SECS] 
```

- 日志打印时间。
- **1.351：**GC发生时，Java虚拟机启动依赖经过的秒数。
- **Full GC (Metadata GC Threshold)：**发生了一次垃圾回收，这是一次FULL GC 。它不区分新生代 GC 还是老年代 GC。括号里的内容是gc发生的原因，这里的Metadata GC Threshold的原因是Metaspace区不够用了，Full GC (Ergonomics) ：JVM 自适应调整导致的GC，Full GC (System）： 调用了System.gc( ) 方法。
- **[PSYoungGen: 10082K->0K(89600K)]：**PSYoungGen：表示GC发生的区域，区域名称与使用的GC 收集器是密切相关的（详细见2.2.2中的垃圾收集器）。GC 前该内存区域已使用容量 - > GC 后该区域容量 （该区域总容量），如果是新生代，总容量则会显示整个新生代内存的9/10，即 eden+from/to 区；如果是老年代，总容量则是全部内存大小，无变化。
- **[ParOldGen: 32K->9638K(204800K)]：**老年代区域没有发生GC，因为本次GC是metaspace引起的。
- **10114K->9638K(294400K)：**在显示完区域容量GC的情况之后，会接着显示整个堆内存区域的GC情况：GC 前堆内存已使用容量 - > GC 堆内存容量（堆内存总容量）堆内存总容量 = 9/10 新生代+ 老年代 < 初始化的内存大小。
- **[Metaspace: 20158K->20156K(1067008K)]：**metaspace GC  回收2K空间
- **0.0285388 secs：**整个GC所花费的时间，单位是秒
- user：指的是CPU工作在用户态所花费的时间，sys：指的是CPU工作在内核态所花费的时间，real：指的是在此次GC事件中所花费的总时间

#### 2.2.4、日志分析工具

GCeasy、GCViewer、GChisto、HPjemter

### 2.3、性能调优

#### 2.3.1、为什么需要调优

防止OOM出现、解决程序运行中各种OOM，减少Full GC出现的频率，解决运行慢、卡顿的现象。

#### 2.3.2、性能优化步骤

1. 熟悉业务场景：只有熟悉业务场景才能codeview
2. 性能监控：发现是否存在频繁GC、CPU load过高、OOM、内存泄露、死锁、程序响应时间长等问题
3. 问题分析：打印GC日志，使用命令打印JVM相关信息，导出dump文件分析、使用日志分析工具、查看堆栈信息
4. 性能调优：
   - 根据实际情况增加内存，选择合适的垃圾回收器
   - 优化代码、控制代码上内存的使用
   - 增加服务节点，分散节点压力
   - 合理设置线程池线程数量
   - 使用中间件提供程序运行效率

#### 3.3.3、OOM案例分析

##### 3.3.3.1、堆溢出

- 案例代码

  ```java
  public void addObject(){
      ArrayList<People> people = new ArrayList<>();
      while (true){
          people.add(new People());
      }
  }
  ```

- 报错信息：java.lang.OutOfMemoryError: Java heap space

- 设置jvm参数：-XX:+PrintGCDetails -XX:MetaspaceSize=64m -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=heap/heapdump.hprof -XX:+PrintGCDateStamps -Xms200M  -Xmx200M  -Xloggc:log/gc-oomHeap.log

- 初步分析及解决方案：

  1. 有可能存在大对象分配
  2. 有可能存在内存泄露，导致多次GC后还是无法找到能够容纳对象的内存空间。

  检查代码中是否有大对象的分配；通过dump文件分析是否有内存泄漏的情况；如果没有明显的内存泄露尝试加大堆内存。

- 根据日志分析工具分析dump文件，可以得到是那个对象占用内存多

##### 3.3.3.2、元空间溢出

- 报错信息：java.lang.OutOfMemoryError: Metaspace

- 元空间用于存储已被虚拟机加载的类信息、常量、即时编译器编译后的代码等数据。垃圾回收目标主要是针对常量池的回收和对类型的卸载。

- 案例代码

  ```java
  public void metaSpaceOom(){
      ClassLoadingMXBean classLoadingMXBean = ManagementFactory.getClassLoadingMXBean();
      while (true){
          Enhancer enhancer = new Enhancer();
          enhancer.setSuperclass(People.class);
          enhancer.setUseCache(false);
          enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> {
              System.out.println("我是加强类哦，输出print之前的加强方法");
              return methodProxy.invokeSuper(o,objects);
          });
          People people = (People)enhancer.create();
          people.print();
          System.out.println(people.getClass());
          System.out.println("totalClass:" + classLoadingMXBean.getTotalLoadedClassCount());
          System.out.println("activeClass:" + classLoadingMXBean.getLoadedClassCount());
          System.out.println("unloadedClass:" + classLoadingMXBean.getUnloadedClassCount());
      }
  }
  ```

- jvm参数配置

  -XX:+PrintGCDetails -XX:MetaspaceSize=60m -XX:MaxMetaspaceSize=60m -Xss512K -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=heap/heapdumpMeta.hprof  -XX:SurvivorRatio=8 -XX:+TraceClassLoading -XX:+TraceClassUnloading -XX:+PrintGCDateStamps  -Xms60M  -Xmx60M -Xloggc:log/gc-oomMeta.log

- 分析及解决

  原因：
  1. 运行期间生成了大量的代理类，导致方法区被撑爆，无法卸载
  2. 应用长时间运行，没有重启
  3. 元空间内存设置过小

  解决方法：
  因为该 OOM 原因比较简单，解决方法有如下几种：

  1. 检查是否永久代空间或者元空间设置的过小
  2. 检查代码中是否存在大量的反射操作
  3. dump之后通过mat检查是否存在大量由于反射生成的代理类

##### 3.3.3.3、GC overhead limit exceeded

- 报错信息：java.lang.OutOfMemoryError: GC overhead limit exceeded

- jvm设置：-XX:+PrintGCDetails  -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=heap/dumpExceeded.hprof -XX:+PrintGCDateStamps  -Xms10M  -Xmx10M -Xloggc:log/gc-oomExceeded.log

- 原因分析

  这个是JDK6新加的错误类型，一般都是堆太小导致的。Sun 官方对此的定义：超过98%的时间用来做GC并且回收了不到2%的堆内存时会抛出此异常。本质是一个预判性的异常，抛出该异常时系统没有真正的内存溢出

  解决方法：

  1. 检查项目中是否有大量的死循环或有使用大内存的代码，优化代码。
  2. 添加参数 `-XX:-UseGCOverheadLimit` 禁用这个检查，其实这个参数解决不了内存问题，只是把错误的信息延后，最终出现 java.lang.OutOfMemoryError: Java heap space。
  3. dump内存，检查是否存在内存泄漏，如果没有，加大内存。

##### 3.3.3.4、线程溢出

- 报错信息：java.lang.OutOfMemoryError : unable to create new native Thread

- 问题原因：出现这种异常，基本上都是创建了大量的线程导致的

- 分析及解决方案

  1. 通过 -Xss 设置每个线程栈大小的容量

  2. 能创建的线程数的具体计算公式如下

     (MaxProcessMemory - JVMMemory - ReservedOsMemory) / (ThreadStackSize) = Number of threads
     MaxProcessMemory 指的是进程可寻址的最大空间
     JVMMemory     JVM内存
     ReservedOsMemory 保留的操作系统内存
     ThreadStackSize   线程栈的大小

  3. 检查系统是否有限制

     /proc/sys/kernel/pid_max          系统最大pid值，在大型系统里可适当调大.

     /proc/sys/kernel/threads-max     系统允许的最大线程数

     maxuserprocess（ulimit -u）   系统限制某用户下最多可以运行多少进程或线程

#### 3.3.4、性能优化案例

##### 3.3.4.1、提供服务吞吐量

- 增加堆内存，修改Tomcat JVM配置

  export CATALINA_OPTS="$CATALINA_OPTS -Xms120m"
  export CATALINA_OPTS="$CATALINA_OPTS -XX:SurvivorRatio=8"
  export CATALINA_OPTS="$CATALINA_OPTS -Xmx120m"
  export CATALINA_OPTS="$CATALINA_OPTS -XX:+UseParallelGC"
  export CATALINA_OPTS="$CATALINA_OPTS -XX:+PrintGCDetails"
  export CATALINA_OPTS="$CATALINA_OPTS -XX:MetaspaceSize=64m"
  export CATALINA_OPTS="$CATALINA_OPTS -XX:+PrintGCDateStamps"
  export CATALINA_OPTS="$CATALINA_OPTS -Xloggc:/opt/tomcat8.5/logs/gc.log"

##### 3.3.4.2、JIT优化

​		**逃逸分析(Escape Analysis)**是目前Java虚拟机中比较前沿的优化技术。这是一种可以有效减少Java 程序中同步负载和内存堆分配压力的跨函数全局数据流分析算法。通过逃逸分析，Java Hotspot编译器能够分析出一个新的对象的引用的使用范围，从而决定是否要将这个对象分配到堆上。

逃逸分析的基本行为就是分析对象动态作用域：
当一个对象在方法中被定义后，对象只在方法内部使用，则认为没有发生逃逸。
当一个对象在方法中被定义后，它被外部方法所引用，则认为发生逃逸。例如作为调用参数传递到其他地方中。

- -XX:+DoEscapeAnalysis”显式开启逃逸分析
- -XX：+PrintEscapeAnalysis”查看逃逸分析的筛选结果。

- JIT编译器在编译期间根据逃逸分析的结果，发现如果一个对象并没有逃逸出方法的话，就可能被优化成栈上分配。分配完成后，继续在调用栈内执行，最后线程结束，栈空间被回收，局部变量对象也被回收。这样就无须进行垃圾回收了。
- 在动态编译同步块的时候，JIT编译器可以借助逃逸分析来判断同步块所使用的锁对象是否只能够被一个线程访问而没有被发布到其他线程。如果没有，那么JIT编译器在编译这个同步块的时候就会取消对这部分代码的同步。这样就能大大提高并发性和性能。这个取消同步的过程就叫同步省略，也叫锁消除。
- 在JIT阶段，如果经过逃逸分析，发现一个对象不会被外界访问的话，那么经过JIT优化，就会把这个对象拆解成若干个其中包含的若干个成员变量来代替。这个过程就是标量替换。(标量（Scalar）是指一个无法再分解成更小的数据的数据。Java中的原始数据类型就是标量)

##### 3.3.4.3、合理配置堆内存

- Java整个堆大小设置，Xmx 和 Xms设置为老年代存活对象的3-4倍，即FullGC之后的老年代内存占用的3-4倍。
- 方法区（永久代 PermSize和MaxPermSize 或 元空间 MetaspaceSize 和 MaxMetaspaceSize）设置为老年代存活对象的1.2-1.5倍。
- 年轻代Xmn的设置为老年代存活对象的1-1.5倍。
- 老年代的内存大小设置为老年代存活对象的2-3倍。

## 3、配置参数设置

官方文档：https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html

| 说明                                                       | 配置                                                         | 配置简写                                                     |
| ---------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 配置栈的大小                                               | -XX:ThreadStackSize                                          | -Xsssize                                                     |
| 堆空间大小                                                 | -Xms -Xmx                                                    | -Xms:初始内存 （默认为物理内存的1/64）<br />-Xmx:最大内存（默认为物理内存的1/4） |
| 新生代的大小                                               | -Xmn                                                         | -Xmn（初始值及最大值）通常默认即可                           |
| 老年代占比+                                                | -XX:NewRatio                                                 | -XX:NewRatio，默认比例是1（新）:2（老）<br />-XX:NewRatio=2  |
| Eden占比                                                   | -XX:NewRatio                                                 | -XX:NewRatio，默认比例是8:1:1                                |
| 新生代垃圾最大年龄                                         | -XX:MaxTenuringThreshold                                     | -XX:MaxTenuringThreshold默认15，如果设置为0表示<br />不经过Survivor区，直接进入老年代 |
| 打印GC详细日志                                             | -XX:+PrintGCDetails                                          | -XX:+PrintGCDetails                                          |
| 检查老年代最大连续空间<br />是否大于新生代所有对象的总空间 | -XX:HandlePromotionFailure                                   | -XX:HandlePromotionFailure<br />设置为true，如果大于会尝试一次Minor GC，如果小于或者设置为false就会进行一次Full GC |
| 查看所有参数的<br />最终值                                 | -XX:+PrintFlagsFinal                                         | -XX:+PrintFlagsFinal<br />jinfo -flag SurvivorRatio 进程id   |
| 方法区内存大小                                             | jdk7及以前：<br />-XX:PermSize初始分配空间 <br />-XX:MaxPermSize来设定永久代最大可分配空间。<br />jdk8及以后：<br />-XX:MetaspaceSize和-XX:MaxMetaspaceSize | jdk7及以前：<br />-XX:PermSize初始分配空间 <br />-XX:MaxPermSize来设定永久代最大可分配空间。<br />jdk8及以后：<br />-XX:MetaspaceSize和-XX:MaxMetaspaceSize |
| 直接内存大小                                               | -XX:MaxDirectMemorySize                                      | -XX:MaxDirectMemorySize                                      |
| 输出gc日志信息                                             | -verbose:gc<br />-XX:+PrintGC                                | -verbose:gc<br />-XX:+PrintGC                                |
| 打印详细GC日志                                             | -XX:+PrintGCDetails                                          | -XX:+PrintGCDetails                                          |
| 输出GC发生时的时间戳                                       | -XX:+PrintGCTimeStamps<br />-XX:+PrintGCDateStamps           | -XX:+PrintGCTimeStamps<br />-XX:+PrintGCDateStamps           |
| GC前后堆信息                                               | -XX:+PrintHeapAtGC                                           | -XX:+PrintHeapAtGC                                           |
| GC日志打印到文件中                                         | -Xloggc:<file>                                               | -Xloggc:<file>                                               |

### 3.1、打印设置

- -XX:+PrintCommandLineFlags：可以让在程序运行前打印出用户手动设置或者JVM自动设置的XX选项
- -XX:+PrintFlagsInitial：表示打印出所有XX选项的默认值
- -XX:+PrintFlagsFinal：表示打印出XX选项在运行程序时生效的值
- -XX:+PrintVMOptions：打印JVM的参数

### 3.2、堆、栈、方法区设置

1. 堆
   - -Xms3550m：等价于-XX:InitialHeapSize，设置JVM初始堆内存为3550M
   - -Xmx3550m：等价于-XX:MaxHeapSize，设置JVM最大堆内存为3550M
   - -Xmn2g：设置年轻代大小为2G，官方推荐配置为整个堆大小的3/8
   - -XX:NewSize=1024m：设置年轻代初始值为1024M
   - -XX:MaxNewSize=1024m：设置年轻代最大值为1024M
   - -XX:SurvivorRatio=8：设置年轻代中Eden区与一个Survivor区的比值，默认为8
   - -XX:+UseAdaptiveSizePolicy：自动选择各区大小比例
   - -XX:NewRatio=4：设置老年代与年轻代（包括1个Eden和2个Survivor区）的比值
   - -XX:PretenureSizeThreadshold=1024：设置让大于此阈值的对象直接分配在老年代，单位为字节，只对Serial、ParNew收集器有效
   - -XX:MaxTenuringThreshold=15：默认15，新生代每次MinorGC后，还存活的对象年龄+1，当对象的年龄大于设置的这个值时就进入老年代
   - -XX:+PrintTenuringDistribution：让JVM在每次MinorGC后打印出当前使用的Survivor中对象的年龄分布
   - -XX:TargetSurvivorRatio：表示MinorGC结束后Survivor区域中占用空间的期望比例
2. 栈
   - -Xss128k：设置每个线程的栈大小为128k，等价于 -XX:ThreadStackSize=128k。
3. 方法区
   - -XX:PermSize=256m：设置永久代初始值为256M
   - -XX:MaxPermSize=256m：设置永久代最大值为256M
   - -XX:MetaspaceSize：初始空间大小
   - -XX:MaxMetaspaceSize：最大空间，默认没有限制
   - -XX:+UseCompressedOops：压缩对象指针
   - -XX:+UseCompressedClassPointers：压缩类指针
   - -XX:CompressedClassSpaceSize：设置Klass Metaspace的大小，默认1G
4. 直接内存：-XX:MaxDirectMemorySize指定DirectMemory容量，若未指定，则默认与Java堆最大值一样

### 3.3、OOM设置

- -XX:+HeapDumpOnOutOfMemoryError：表示在内存出现OOM的时候，把Heap转存(Dump)到文件以便后续分析
- -XX:+HeapDumpBeforeFullGC：表示在出现FullGC之前，生成Heap转储文件
- -XX:HeapDumpPath=<path>：指定heap转存文件的存储路径
- -XX:OnOutOfMemoryError：指定一个可行性程序或者脚本的路径，当发生OOM的时候，去执行这个脚本

### 3.4、垃圾回收器设置

- -XX:+PrintCommandLineFlags：查看默认垃圾回收器，命令jinfo –flag 相关垃圾回收器参数 进程ID

- -XX:+UseSerialGC ：Serial回收器

- -XX:+UseParNewGC：手动指定使用ParNew收集器执行内存回收任务。它表示年轻代使用并行收集器，不影响老年代。

- -XX:ParallelGCThreads=N ：限制线程数量，默认开启和CPU数据相同的线程数。

- -XX:+UseParallelGC：手动指定年轻代使用Parallel并行收集器执行内存回收任务。

- -XX:+UseParallelOldGC：  手动指定老年代都是使用并行回收收集器。

- -XX:ParallelGCThreads： 设置年轻代并行收集器的线程数。一般地，最好与CPU数量相等，以避免过多的线程数影响垃圾收集性能。在默认情况下，当CPU 数量小于8个， ParallelGCThreads 的值等于CPU 数量。当CPU数量大于8个，ParallelGCThreads 的值等于3+[5*CPU_Count]/8] 。

- -XX:MaxGCPauseMillis： 设置垃圾收集器最大停顿时间(即STW的时间)。单位是毫秒。

- -XX:+UseConcMarkSweepGC： 手动指定使用CMS 收集器执行内存回收任务。

  开启该参数后会自动将-XX:+UseParNewGC打开。即：ParNew(Young区用)+CMS(Old区用)+Serial Old的组合。

- -XX:CMSlnitiatingOccupanyFraction： 设置堆内存使用率的阈值，一旦达到该阈值，便开始进行回收。

  JDK5及以前版本的默认值为68,即当老年代的空间使用率达到68%时，会执行一次CMS 回收。JDK6及以上版本默认值为92%
  如果内存增长缓慢，则可以设置一个稍大的值，大的阈值可以有效降低CMS的触发频率，减少老年代回收的次数可以较为明显地改善应用程序性能。反之，如果应用程序内存使用率增长很快，则应该降低这个阈值，以避免频繁触发老年代串行收集器。因此通过该选项便可以有效降低Full GC 的执行次数。

- -XX:+UseCMSCompactAtFullCollection： 用于指定在执行完Full GC后对内存空间进行压缩整理，以此避免内存碎片的产生。不过由于内存压缩整理过程无法并发执行，所带来的问题就是停顿时间变得更长了。

- XX:CMSFullGCsBeforeCompaction： 设置在执行多少次Full GC后对内存空间进行压缩整理。

- -XX:ParallelCMSThreads： 设置CMS的线程数量。

  CMS 默认启动的线程数是（ParallelGCThreads+3)/4，ParallelGCThreads 是年轻代并行收集器的线程数。当CPU 资源比较紧张时，受到CMS收集器线程的影响，应用程序的性能在垃圾回收阶段可能会非常糟糕。

- -XX：+UseG1GC ：手动指定使用G1收集器执行内存回收任务。
- -XX:G1HeapRegionSize ：设置每个Region的大小。值是2的幂，范围是1MB到32MB之间，目标是根据最小的Java堆大小划分出约2048个区域。默认是堆内存的1/2000。
- -XX:MaxGCPauseMillis ：设置期望达到的最大GC停顿时间指标(JVM会尽力实现，但不保证达到)。默认值是200ms
- -XX:ParallelGCThread：设置STW时GC线程数的值。最多设置为8
- -XX:ConcGCThreads：设置并发标记的线程数。将n设置为并行垃圾回收线程数(ParallelGCThreads)的1/4左右。
- -XX:InitiatingHeapOccupancyPercent：设置触发并发GC周期的Java堆占用率阈值。超过此值，就触发GC。默认值是45。
- -XX:G1NewSizePercent、-XX:G1MaxNewSizePercent  ：新生代占用整个堆内存的最小百分比（默认5%）、最大百分比（默认60%）
- -XX:G1ReservePercent=10 ：保留内存区域，防止 to space（Survivor中的to区）溢出

> **垃圾回收器的选择建议：**
>
> **优先调整堆的大小让JVM自适应完成。**
> **如果内存小于100M，使用串行收集器**
> **如果是单核、单机程序，并且没有停顿时间的要求，串行收集器**
> **如果是多CPU、需要高吞吐量、允许停顿时间超过1秒，选择并行或者JVM自己选择**
> **如果是多CPU、追求低停顿时间，需快速响应（比如延迟不能超过1秒，如互联网应用），使用并发收集器。官方推荐G1，性能高。现在互联网的项目，基本都是使用G1。**
>
> **特别说明：**
>
> 1. **没有最好的收集器，更没有万能的收集；**
> 2. **调优永远是针对特定场景、特定需求，不存在一劳永逸的收集器**

### 3.5、GC日志设置

- -verbose:gc    输出gc日志信息，默认输出到标准输出
- -XX:+PrintGC  等同于-verbose:gc 表示打开简化的GC日志
- -XX:+PrintGCDetails   在发生垃圾回收时打印内存回收详细的日志，并在进程退出时输出当前内存各区域分配情况
- -XX:+PrintGCTimeStamps  输出GC发生时的时间戳，不可以独立使用，需要配合-XX:+PrintGCDetails使用。
- -XX:+PrintGCDateStamps   输出GC发生时的时间戳（以日期的形式，如 2013-05-04T21:53:59.234+0800），不可以独立使用，需要配合-XX:+PrintGCDetails使用
- -XX:+PrintHeapAtGC  每一次GC前和GC后，都打印堆信息
- -Xloggc:<file>   把GC日志写入到一个文件中去，而不是打印到标准输出中
- -XX:+TraceClassLoading   监控类的加载
- -XX:+PrintGCApplicationStoppedTime   打印GC时线程的停顿时间
- -XX:+PrintGCApplicationConcurrentTime  垃圾收集之前打印出应用未中断的执行时间
- -XX:+PrintReferenceGC   记录回收了多少种不同引用类型的引用
- -XX:+PrintTenuringDistribution  让JVM在每次MinorGC后打印出当前使用的Survivor中对象的年龄分布
- -XX:+UseGCLogFileRotation	启用GC日志文件的自动转储
- -XX:NumberOfGClogFiles=1  GC日志文件的循环数目
- -XX:GCLogFileSize=1M  控制GC日志文件的大小
- -XX:+DisableExplicitGC  禁止hotspot执行System.gc()，默认禁用
- -XX:ReservedCodeCacheSize=<n>[g|m|k]、-XX:InitialCodeCacheSize=<n>[g|m|k]   指定代码缓存的大小
- -XX:+UseCodeCacheFlushing  使用该参数让jvm放弃一些被编译的代码，避免代码缓存被占满时JVM切换到interpreted-only的情况
- -XX:+DoEscapeAnalysis  开启逃逸分析
- -XX:+UseBiasedLocking  开启偏向锁
- -XX:+UseLargePages   开启使用大页面
- -XX:+UseTLAB  使用TLAB，默认打开
- -XX:+PrintTLAB  打印TLAB的使用情况
- -XX:TLABSize  设置TLAB大小

### 3.6、功能性质区分JVM参数

Java HotSpot VM中-XX:的可配置参数列表进行描述；

这些参数可以被松散的聚合成三类：
行为参数（Behavioral Options）：用于改变jvm的一些基础行为；
性能调优（Performance Tuning）：用于jvm的性能调优；
调试参数（Debugging Options）：一般用于打开跟踪、打印、输出等jvm参数，用于显示jvm更加详细的信息；

行为参数(功能开关)

- -XX:-DisableExplicitGC  禁止调用System.gc()；但jvm的gc仍然有效
- -XX:+MaxFDLimit 最大化文件描述符的数量限制
- -XX:+ScavengeBeforeFullGC   新生代GC优先于Full GC执行
- -XX:+UseGCOverheadLimit 在抛出OOM之前限制jvm耗费在GC上的时间比例
- -XX:-UseConcMarkSweepGC 对老生代采用并发标记交换算法进行GC
- -XX:-UseParallelGC  启用并行GC
- -XX:-UseParallelOldGC   对Full GC启用并行，当-XX:-UseParallelGC启用时该项自动启用
- -XX:-UseSerialGC    启用串行GC
- -XX:+UseThreadPriorities    启用本地线程优先级

性能调优

- -XX:LargePageSizeInBytes=4m 设置用于Java堆的大页面尺寸
- -XX:MaxHeapFreeRatio=70 GC后java堆中空闲量占的最大比例
- -XX:MaxNewSize=size 新生成对象能占用内存的最大值
- -XX:MaxPermSize=64m 老生代对象能占用内存的最大值
- -XX:MinHeapFreeRatio=40 GC后java堆中空闲量占的最小比例
- -XX:NewRatio=2  新生代内存容量与老生代内存容量的比例
- -XX:NewSize=2.125m  新生代对象生成时占用内存的默认值
- -XX:ReservedCodeCacheSize=32m   保留代码占用的内存容量
- -XX:ThreadStackSize=512 设置线程栈大小，若为0则使用系统默认值
- -XX:+UseLargePages  使用大页面内存

调试参数

- -XX:-CITime 打印消耗在JIT编译的时间
- -XX:ErrorFile=./hs_err_pid<pid>.log 保存错误日志或者数据到文件中
- -XX:-ExtendedDTraceProbes   开启solaris特有的dtrace探针
- -XX:HeapDumpPath=./java_pid<pid>.hprof  指定导出堆信息时的路径或文件名
- -XX:-HeapDumpOnOutOfMemoryError 当首次遭遇OOM时导出此时堆中相关信息
- -XX:OnError="<cmd args>;<cmd args>" 出现致命ERROR之后运行自定义命令
- -XX:OnOutOfMemoryError="<cmd args>;<cmd args>"  当首次遭遇OOM时执行自定义命令
- -XX:-PrintClassHistogram    遇到Ctrl-Break后打印类实例的柱状信息，与jmap -histo功能相同
- -XX:-PrintConcurrentLocks   遇到Ctrl-Break后打印并发锁的相关信息，与jstack -l功能相同
- -XX:-PrintCommandLineFlags  打印在命令行中出现过的标记
- -XX:-PrintCompilation   当一个方法被编译时打印相关信息
- -XX:-PrintGC    每次GC时打印相关信息
- -XX:-PrintGC Details    每次GC时打印详细信息
- -XX:-PrintGCTimeStamps  打印每次GC的时间戳
- -XX:-TraceClassLoading  跟踪类的加载信息
- -XX:-TraceClassLoadingPreorder  跟踪被引用到的所有类的加载信息
- -XX:-TraceClassResolution   跟踪常量池
- -XX:-TraceClassUnloading    跟踪类的卸载信息
- -XX:-TraceLoaderConstraints 跟踪类加载器约束的相关信息

### 3.7、代码上获取JVM参数

```java
/**
 *
 * 监控我们的应用服务器的堆内存使用情况，设置一些阈值进行报警等处理
 *
 * @create 15:23
 */
public class MemoryMonitor {
    public static void main(String[] args) {
        MemoryMXBean memorymbean = ManagementFactory.getMemoryMXBean();
        MemoryUsage usage = memorymbean.getHeapMemoryUsage();
        System.out.println("INIT HEAP: " + usage.getInit() / 1024 / 1024 + "m");
        System.out.println("MAX HEAP: " + usage.getMax() / 1024 / 1024 + "m");
        System.out.println("USE HEAP: " + usage.getUsed() / 1024 / 1024 + "m");
        System.out.println("\nFull Information:");
        System.out.println("Heap Memory Usage: " + memorymbean.getHeapMemoryUsage());
        System.out.println("Non-Heap Memory Usage: " + memorymbean.getNonHeapMemoryUsage());

        System.out.println("=======================通过java来获取相关系统状态============================ ");
        System.out.println("当前堆内存大小totalMemory " + (int) Runtime.getRuntime().totalMemory() / 1024 / 1024 + "m");// 当前堆内存大小
        System.out.println("空闲堆内存大小freeMemory " + (int) Runtime.getRuntime().freeMemory() / 1024 / 1024 + "m");// 空闲堆内存大小
        System.out.println("最大可用总堆内存maxMemory " + Runtime.getRuntime().maxMemory() / 1024 / 1024 + "m");// 最大可用总堆内存大小

    }
}
```

```java
public class HeapSpaceInitial {
    public static void main(String[] args) {

        //返回Java虚拟机中的堆内存总量
        long initialMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024;
        //返回Java虚拟机试图使用的最大堆内存量
        long maxMemory = Runtime.getRuntime().maxMemory() / 1024 / 1024;

        System.out.println("-Xms : " + initialMemory + "M");
        System.out.println("-Xmx : " + maxMemory + "M");

        System.out.println("系统内存大小为：" + maxMemory * 4.0 / 1024 + "G");
        System.out.println("系统内存大小为：" + initialMemory * 64.0 / 1024 + "G");
    }
}
```



## 4、问题收集

### 4.1、**什么时候对象会进入老年代？**
1. 对象在Survivor区经过一次Minor GC后年龄增加，当年龄达到阈值（默认是15，可以通过参数-XX:MaxTenuringThreshold设置）后会晋升到老年代。
2. 在Survivor空间中，如果相同年龄的所有对象大小总和大于Survivor空间的一半，无需等待年龄到达阈值的情况。
3. 在一次Minor GC中，仍然存活的对象不能在Survivor区完全容纳，这时对象会通过担保机制进入老年代。

### 4.2、**OOM如果解决？**

1. 要解决OOM或者Heap Space的异常，首先通过内存分析工具对dump出来的堆转存储快照进行分析，重点确认内存中对象是否有必要，也就是要分清楚是出现了内存泄漏还是会内存溢出。
2. 如果是内存溢出，可以通过工具查看内存泄漏对象到GC ROOT的引用链，就能找到泄漏对象是通过怎样的路径与GC ROOT相关联并导致垃圾回收器无法自动回收，因此掌握了GC Roots引用链的信息就可以比较准确的定位内存泄漏的代码位置。
3. 如果不是内存泄漏，就应当检查虚拟机的堆参数，对物理机器内存对比看是否可以调大，从代码上检查是否存在某些对象生命周期过长、持有状态时间过长的情况，尝试减少程序运行时的内存消耗。

