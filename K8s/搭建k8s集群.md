# 搭建K8s集群

## 1、准备三台虚拟机环境

### 1.1、设置三台虚拟机的ip

```shell
[root@localhost ~]# vi /etc/sysconfig/network-scripts/ifcfg-eth1
```

![image-20221205211921500](../images/k8s/image-20221205211921500.png)

### 1.2、添加NatNewYork网络

虚拟机使用统一的网络配置,

![image-20221205212040863](../images/k8s/image-20221205212040863.png)

![image-20221205212129949](../images/k8s/image-20221205212129949.png)

### 1.3、虚拟机的ip情况

![image-20221205212540629](../images/k8s/image-20221205212540629.png)

- 检查网络是否互通，能否访问互联网

  ![image-20221205212806561](../images/k8s/image-20221205212806561.png)

### 1.4、关闭防火墙

```shell
[root@localhost ~]#  systemctl stop firewalld
[root@localhost ~]#  systemctl disable firewalld
```

### 1.5、关闭Linux安全策略

```shell
[root@localhost ~]#  sed -i 's/enforcing/disabled/' /etc/selinux/config
```

### 1.6、禁止全局

```shell
[root@localhost ~]#  setenforce 0
```

### 1.7、关闭内存交换

```shell
[root@localhost ~]# swapoff -a
#永久关闭
[root@localhost ~]# sed -ri 's/.swap./#&/' /etc/fstab
[root@localhost ~]# free -h
关闭内存交换后swap为0,不关闭的话会出现The connection to the server 10.0.2.15:6443 was refused - did you specify the right host or port?
```

### 1.8、修改虚拟机主机名

```shell
[root@localhost ~]#  hostnamectl set-hostname k8s-master
```

![image-20221205213509897](../images/k8s/image-20221205213509897.png)

### 1.9、添加主机名与IP关系

```shell
[root@localhost ~]#  vi /etc/hosts

10.0.2.15 k8s-master
10.0.2.12 k8s-node1
10.0.2.13 k8s-node2
```

### 1.10、将桥接的IPV4流量传递到iptables的链

```shell
[root@k8s-master ~]# cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge-nf-call-ip6tables = 1
net.bridge-nf-call-iptables = 1
EOF
```

## 2、安装docker

参考官方文档：https://docs.docker.com/engine/install/centos/。

这里我是从原来安装好的虚拟机复制的，所以已经安装好了。

## 3、部署k8s

参考官方文档：https://www.kubernetes.org.cn/7189.html

### 3.1、配置阿里云yum源

```shell
[root@localhost ~]# cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

### 3.2、安装kubeadm，kubelet、kubectl

```shell
[root@localhost ~]# yum install -y kubelet-1.17.3 kubeadm-1.17.3 kubectl-1.17.3
[root@localhost ~]#  systemctl enable kubelet
[root@localhost ~]#  systemctl start kubelet
```

### 3.3、安装master需要的镜像

```shell
[root@localhost ~]# vi master_images.sh
[root@localhost ~]# cat master_images.sh 
#!/bin/bash
images=(
	kube-apiserver:v1.17.3
	kube-proxy:v1.17.3
	kube-controller-manager:v1.17.3
	kube-scheduler:v1.17.3
	coredns:1.6.5
	etcd:3.4.3-0
	pause:3.1
)
for imageName in ${images[@]} ; do
	docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
	# docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/$imageName
done

[root@localhost ~]# chmod 700 master_images.sh
[root@localhost ~]# ./master_images.sh

```

### 3.4、master节点初始化

```shell
[root@localhost ~]# kubeadm init --apiserver-advertise-address=10.0.2.15 --image-repository registry.cn-hangzhou.aliyuncs.com/google_containers --kubernetes-version v1.17.3 --service-cidr=10.96.0.0/16 --pod-network-cidr=10.244.0.0/16
```

如下图所示则初始化成功，复制下面这句话后面其他节点加入需要使用

![image-20221205223815075](../images/k8s/image-20221205223704584.png)

- 执行上面配置文件

  ```shell
  [root@localhost ~]# mkdir -p $HOME/.kube
  [root@localhost ~]# sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  [root@localhost ~]# sudo chown $(id -u):$(id -g) $HOME/.kube/config
  ```

### 3.5、安装Pod网络插件

[kube-flannel.yml]: kube-flannel.yml

```shell
[root@localhost ~]#  kubectl apply -f kube-flannel.yml
podsecuritypolicy.policy/psp.flannel.unprivileged created
clusterrole.rbac.authorization.k8s.io/flannel created
clusterrolebinding.rbac.authorization.k8s.io/flannel created
serviceaccount/flannel created
configmap/kube-flannel-cfg created
daemonset.apps/kube-flannel-ds-amd64 created
daemonset.apps/kube-flannel-ds-arm64 created
daemonset.apps/kube-flannel-ds-arm created
daemonset.apps/kube-flannel-ds-ppc64le created
daemonset.apps/kube-flannel-ds-s390x created
```

删除可以用：kubectl delete -f kube-flannel.yml

### 3.6、查看master的状态

node节点为NotReady，因为corednspod没有启动，缺少网络pod

```shell
[root@localhost ~]# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
error: unable to recognize "https://docs.projectcalico.org/manifests/calico.yaml": no matches for kind "PodDisruptionBudget" in version "policy/v1"
#如果执行后报错使用如下方法
[root@k8s-master ~]# curl https://docs.projectcalico.org/v3.18/manifests/calico.yaml -O
[root@k8s-master ~]# kubectl apply -f calico.yaml
```

### 3.7、获取当前所有命名空间的pod情况

```shell
[root@localhost ~]# kubectl get pods --all-namespaces
#实时监控节点pod情况
[root@localhost ~]# watch kubectl get pods --all-namespaces -o wide 

Every 2.0s: kubectl get pods --all-namespaces -o wide                                                                                                                                                                    Mon Dec  5 15:00:41 2022

NAMESPACE     NAME                                      READY   STATUS                  RESTARTS   AGE   IP           NODE         NOMINATED NODE   READINESS GATES
kube-system   calico-kube-controllers-5b9fb8df9-k4gvr   1/1     Running                 0          17m   10.244.0.4   k8s-master   <none>           <none>
kube-system   calico-node-spk9q                         0/1     Init:CrashLoopBackOff   7          17m   10.0.2.15    k8s-master   <none>           <none>
kube-system   coredns-7f9c544f75-9xwjp                  1/1     Running                 0          24m   10.244.0.2   k8s-master   <none>           <none>
kube-system   coredns-7f9c544f75-nfvr8                  1/1     Running                 0          24m   10.244.0.3   k8s-master   <none>           <none>
kube-system   etcd-k8s-master                           1/1     Running                 0          24m   10.0.2.15    k8s-master   <none>           <none>
kube-system   kube-apiserver-k8s-master                 1/1     Running                 0          24m   10.0.2.15    k8s-master   <none>           <none>
kube-system   kube-controller-manager-k8s-master        1/1     Running                 0          24m   10.0.2.15    k8s-master   <none>           <none>
kube-system   kube-flannel-ds-amd64-7gq7p               1/1     Running                 0          19m   10.0.2.15    k8s-master   <none>           <none>
kube-system   kube-proxy-4h44m                          1/1     Running                 0          24m   10.0.2.15    k8s-master   <none>           <none>
kube-system   kube-scheduler-k8s-master                 1/1     Running                 0          24m   10.0.2.15    k8s-master   <none>           <none>

```

![image-20221205230118724](../images/k8s/image-20221205230118724.png)

### 3.8、当master节点ready，其他节点就可以加入

```shell
[root@localhost ~]# kubectl get node
NAME         STATUS   ROLES    AGE   VERSION
k8s-master   Ready    master   18m   v1.17.3
```

```shell
[root@localhost ~]# kubeadm join 10.0.2.15:6443 --token ree3ad.y6vqdcu780ov8915 \
    --discovery-token-ca-cert-hash sha256:016ea9a9662fed71fa3a04fcdfc90341c8e6d1544a4ce385f90819a2c9aa63d1
```

```shell
[root@localhost ~]# kubectl get node
NAME         STATUS     ROLES    AGE   VERSION
k8s-master   Ready      master   27m   v1.17.3
k8s-node1    NotReady   <none>   46s   v1.17.3
k8s-node2    NotReady   <none>   22s   v1.17.3
```

## 4、安装 KubeSphere

前提条件：https://v2-1.docs.kubesphere.io/docs/zh-CN/installation/prerequisites/

参考官方文档：https://v3-0.docs.kubesphere.io/zh/docs/quick-start/minimal-kubesphere-on-k8s/

### 4.1、安装helm

参考官方文档：https://devopscube.com/install-configure-helm-kubernetes/

这里我们直接使用离线安装，将helm-v2.16.3-linux-amd64.tar 上传至服务器。下载地址：https://github.com/helm/helm/releases/tag/v2.17.0

```shell
[root@k8s-master ~]# tar -zxvf helm-v2.16.3-linux-amd64.tar
[root@k8s-master ~]# cd linux-amd64/
[root@k8s-master linux-amd64]# mv helm /usr/local/bin/helm
[root@k8s-master linux-amd64]# mv tiller /usr/local/bin/tiller
#检测是否安装成功
[root@k8s-master linux-amd64]# helm help
[root@k8s-master linux-amd64]# tiller -help
```

#### 4.1.1、创建权限

将helm-rbac.yaml  传到服务器

```shell
[root@k8s-master linux-amd64]# kubectl apply -f helm-rbac.yaml
serviceaccount/tiller created
clusterrolebinding.rbac.authorization.k8s.io/tiller created
```

#### 4.1.2、Helm初始化

```shell
[root@k8s-master ~]# helm init --service-account tiller --upgrade -i registry.cn-hangzhou.aliyuncs.com/google_containers/tiller:v2.16.3 --stable-repo-url https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
Creating /root/.helm 
Creating /root/.helm/repository 
Creating /root/.helm/repository/cache 
Creating /root/.helm/repository/local 
Creating /root/.helm/plugins 
Creating /root/.helm/starters 
Creating /root/.helm/cache/archive 
Creating /root/.helm/repository/repositories.yaml 
Adding stable repo with URL: https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts 
Adding local repo with URL: http://127.0.0.1:8879/charts 
$HELM_HOME has been configured at /root/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://v2.helm.sh/docs/securing_installation/
```

### 4.2、安装OpenEBS

参考官方文档：https://v2-1.docs.kubesphere.io/docs/zh-CN/appendix/install-openebs/

#### 4.2.1、取消 master 节点的 Taint

- 确认 master 节点是否有 Taint，如下看到 master 节点有 Taint。

  ```shell
  [root@k8s-master ~]# kubectl describe node k8s-master | grep Taint
  Taints:             node-role.kubernetes.io/master:NoSchedule
  ```

- 去掉 master 节点的 Taint

  ```shell
  [root@k8s-master ~]# kubectl taint nodes k8s-master node-role.kubernetes.io/master:NoSchedule-
  ```

```shell
[root@k8s-master ~]# kubectl create ns openebs
namespace/openebs created
[root@k8s-master ~]# kubectl get ns
NAME              STATUS   AGE
default           Active   3h3m
kube-node-lease   Active   3h3m
kube-public       Active   3h3m
kube-system       Active   3h3m
openebs           Active   6s
```

- 若集群已安装了 Helm，可通过 Helm 命令来安装 OpenEBS：  

  ```shell
  [root@k8s-master ~]# helm install --namespace openebs --name openebs stable/openebs --version 1.5.0
  ```

- 也可以通过 kubectl 命令安装：  

  ```shell
  [root@k8s-master ~]# kubectl apply -f https://openebs.github.io/charts/openebs-operator-1.5.0.yaml
  ```

安装可能出现以下异常信息：

> Error: failed to download "stable/openebs" (hint: running `helm repo update` may help)

修改repo源：

```shell
[root@k8s-master ~]# helm repo list
NAME  	URL                                                   
local 	http://127.0.0.1:8879/charts                          
stable	https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
[root@k8s-master ~]# helm repo remove stable
[root@k8s-master ~]# helm repo add stable http://mirror.azure.cn/kubernetes/charts/
[root@k8s-master ~]# helm install --namespace openebs --name openebs stable/openebs --version 1.5.0
```

> Error: incompatible versions client[v2.17.0] server[v2.16.3]

修改对应的版本：

```shell
[root@k8s-master ~]# helm version
Client: &version.Version{SemVer:"v2.17.0", GitCommit:"a690bad98af45b015bd3da1a41f6218b1a451dbe", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.16.3", GitCommit:"1ee0254c86d4ed6887327dabed7aa7da29d7eb0d", GitTreeState:"clean"}
#由于前面helm安装了2.17.0，tiller安装2.16.3导致，所以重新安装helm2.16.3即可
```

- 安装 OpenEBS 后将自动创建 4 个 StorageClass，查看创建的 StorageClass，如没有StorageClass就需要按照4.3创建存储类型：  

  ```shell
  #必须等openebs都running才能查出
  [root@k8s-master ~]# kubectl get sc
  NAME                         PROVISIONER                                                RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
  openebs-device               openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2d14h
  openebs-hostpath (default)   openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2d14h
  openebs-jiva-default         openebs.io/provisioner-iscsi                               Delete          Immediate              false                  2d14h
  openebs-snapshot-promoter    volumesnapshot.external-storage.k8s.io/snapshot-promoter   Delete          Immediate              false                  2d14h
  ```

- 如下将 openebs-hostpath 设置为默认的 StorageClass：  

```shell
[root@k8s-master ~]# kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

- 至此，OpenEBS 的 LocalPV 已作为默认的存储类型创建成功。可以通过命令 `kubectl get pod -n openebs`来查看 OpenEBS 相关 Pod 的状态，若 Pod 的状态都是 running，则说明存储安装成功。
- 建议根据官网测试一下StorageClass能否正常使用

### 4.3、安装KubeSphere

```shell
#安装KubeSphere
[root@k8s-master ~]# kubectl apply -f https://raw.githubusercontent.com/kubesphere/ks-installer/v2.1.1/kubesphere-minimal.yaml
#查看安装日志
[root@k8s-master ~]# kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
#出现下面日志就标识安装成功可以访问了
#####################################################
###              Welcome to KubeSphere!           ###
#####################################################

Console: http://10.0.2.15:30880
Account: admin
Password: P@88w0rd

NOTES：
  1. After logging into the console, please check the
     monitoring status of service components in
     the "Cluster Status". If the service is not
     ready, please wait patiently. You can start
     to use when all components are ready.
  2. Please modify the default password after login.

#####################################################
```

