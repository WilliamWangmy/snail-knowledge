# 目录

## 1、Java

### 1.1、[Java IO分类思维导图](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Java/Java%20IO%E5%88%86%E7%B1%BB%E6%80%9D%E7%BB%B4%E5%AF%BC%E5%9B%BE.xmind)

## 2、JVM

### 2.1、[Jvm总结文档](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Jvm/Jvm%E6%80%BB%E7%BB%93%E6%96%87%E6%A1%A3.md)

### 2.2、[Jvm相关面试题](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Jvm/Jvm%E7%9B%B8%E5%85%B3%E9%9D%A2%E8%AF%95%E9%A2%98.md)

## 3、JUC

### 3.1、[JUC总结文档](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Juc/JUC%E6%80%BB%E7%BB%93%E6%96%87%E6%A1%A3.md)

## 4、Linux

### 4.1、[Linux常用命令合集](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Linux/Linux%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4%E5%90%88%E9%9B%86.md)

## 5、Docker

### 5.1、[安装Harbor](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Docker/%E5%AE%89%E8%A3%85Harbor.md)

### 5.2、[制作Tomcat镜像](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Docker/%E5%88%B6%E4%BD%9CTomcat%E9%95%9C%E5%83%8F.md)

### 5.3、[Docker常用命令](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Docker/Docker%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4.md)

## 6、K8s

### 6.1、[搭建K8s集群](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/K8s/%E6%90%AD%E5%BB%BAk8s%E9%9B%86%E7%BE%A4.md)

## 7、Rancher

### 7.1、[安装Rancher](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#1%E5%AE%89%E8%A3%85rancher)

### 7.2、[Rancher部署Mysql](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#4rancher%E9%83%A8%E7%BD%B2mysql)

### 7.3、[Rancher部署Nacos](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#5rancher%E9%83%A8%E7%BD%B2nacos)

### 7.4、[Rancher部署Redis](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#6rancher%E9%83%A8%E7%BD%B2redis)

### 7.5、[Rancher部署Nginx](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#7rancher%E9%83%A8%E7%BD%B2nginx)

### 7.6、[Rancher配置Harbor](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#8rancher%E9%85%8D%E7%BD%AEharbor)

### 7.7、[Rancher部署Minio](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Rancher/Rancher%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md#9rancher%E9%83%A8%E7%BD%B2minio)

## 8、Kafka

### 8.1、[Kafka总结文档](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Kafka/Kafka总结文档.md)

## 9、RabbitMQ

## 10、Redis

### 10.1、[Redis总结文档](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Redis/Redis总结文档.md)

## 11、Spring

## 12、Mysql

## 13、Oracle

### 13.1、[Linux下安装Oracle](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Oracle/Linux下安装Oracle.md)

### 13.2、[Oracle常用sql](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Oracle/Oracle常用sql.md)

### 13.3、[Oracle常见命令和错误码](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Oracle/Oracle常见命令和错误码.md)

## 14、神通数据库

## 15、Nginx

## 16、其他

### 16.1、[Java面试题](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Other/Java%E9%9D%A2%E8%AF%95%E9%A2%98.md)

### 16.2、[什么是布隆过滤器](https://gitee.com/WilliamWangmy/snail-knowledge/blob/master/Other/什么是布隆过滤器.md)







