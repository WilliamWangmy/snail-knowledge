## 1、查询文件

```shell
# find / -name 文件名称
[root@boco ~]# find / -name test.zip
# 移动文件夹下的所有文件导另一个目录
mv /home/packageA/* /home/packageB/
```

## 2、导出Mysql表结构及数据

```shell
#mysqldump -u 用户名 -p密码 数据库 > 导出路径
mysqldump -u root -p123456 snail-cloud > /var/log/mysql/database_data.sql
```

## 3、导入Mysql表结构及数据

```sql
#mysql -u用户名 -p密码 数据库 > 需要导入的数据库文件
mysql -uroot -p123456 tm-base < tm-base.sql
```

## 4、网络相关

```shell
#修改ip相关的信息
cd /etc/sysconfig/network-scripts/
# 根据ifconfi查看ip对应的网卡，修改对应的文件
vi ifcfg-em1
#重启网卡
sudo systemctl restart network
```

