# 1、安装包准备

这里我已经准备好安装包harbor-offline-installer-v2.0.0.tgz，在harbor的目录下。然后需要上传到服务器的对应的目录。

![部署harbor1](../images/docker/部署harbor1.png)

解压安装包

![部署harbor2](../images/docker/部署harbor2.png)

# 2、前置环境准备

​		由于harbor是依赖于docker-compose，所以要先安装docker-compose。

​		安装过程中可能会出现一下问题。

```shell
#出现以下问题
[root@k8s-node2 harbor]#  yum install -y docker-compose
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.aliyun.com
 * extras: mirrors.aliyun.com
 * updates: mirrors.aliyun.com
No package docker-compose available.
Error: Nothing to do
#执行以下命令继续安装
[root@k8s-node2 harbor]# yum install -y epel-release
[root@k8s-node2 harbor]# sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
[root@k8s-node2 harbor]# yum install -y docker-compose
```

# 3、安装harbor

首先进入harbor的解压目录。拷贝一个yml文件进行安装配置

```shell
[root@localhost harbor]# cp harbor.yml.tmpl harbor.yml
#修改如下配置
[root@k8s-node2 harbor]# vi harbor.yml

#修改host，自己服务器的ip
# DO NOT use localhost or 127.0.0.1, because Harbor needs to be accessed by external clients.
hostname: 192.168.56.11
#注释https
#https:
  # https port for harbor, default is 443
  #port: 443
  # The path of cert and key files for nginx
  #certificate: /your/certificate/path
  #private_key: /your/private/key/path
#修改密码
# Remember Change the admin password from UI after launching Harbor.
harbor_admin_password: 123456

#执行安装命令
[root@k8s-node2 harbor]# ./install.sh
```

![部署harbor3](../images/docker/部署harbor3.png)

# 4、开启harbor远程

如果不开启远程，就无法推送镜像要harbor仓库。

```shell
root@k8s-node2 harbor]# vi /lib/systemd/system/docker.service
#添加-H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
#配置Docker拉取的镜像仓库
[root@k8s-node2 harbor]# vi /etc/docker/daemon.json
{
#这里是Docker的镜像配置，可以配置阿里云、网易等镜像仓库
"registry-mirrors": ["xxxx"],
#这里配置的harbor私库
"insecure-registries":["192.168.56.11","192.168.56.11:80","192.168.56.11:5000"]
}
#应用配置的Docker
[root@k8s-node2 harbor]# systemctl daemon-reload
#重启Docker
[root@k8s-node2 harbor]# systemctl restart docker
#停止harbor
[root@k8s-node2 harbor]# docker-compose stop
#启动harbor
[root@k8s-node2 harbor]# docker-compose up -d
#测试是否安装成功
[root@k8s-node2 ~]# docker login 192.168.56.11
Username: admin
Password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

页面访问服务器ip：端口进行访问测试。

![部署harbor4](../images/docker/部署harbor4.png)

# 5、卸载

```shell
# 停止Harbor
[root@k8s-node2 harbor]# docker-compose stop
# 停止Harbor相关容器
[root@k8s-node2 harbor]# sudo docker stop $(sudo docker ps -aq --filter name=harbor)
# 删除Harbor相关容器
[root@k8s-node2 harbor]# sudo docker rm $(sudo docker ps -aq --filter name=harbor)
# 删除Harbor相关镜像
[root@k8s-node2 harbor]# sudo docker rmi $(sudo docker images --filter=reference='goharbor/*' -q)
# 删除Harbor相关文件夹
[root@k8s-node2 harbor]# rm -rf `find / -name harbor`
```

