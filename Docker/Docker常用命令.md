# Docker常用命令

```shell
# 重新加载docker配置文件
sudo systemctl daemon-reload
# 重启docker
sudo systemctl restart docker
```

## 1、镜像

### 1.1、删除镜像

```shell
# 删除全部镜像 
[root@root ~]# docker rmi $(docker images -q) 
# 删除无用的镜像 
[root@root ~]# docker image prune 
# 删除无用的镜像  包含没有打标签的
[root@root ~]# docker image prune -a
# 删除镜像
[root@root ~]# docker rmi -f image_id
#删除包含镜像名称的镜像
[root@root ~]# docker rmi $(sudo docker images --filter=reference='goharbor/*' -q)
#删除none无用的镜像
[root@root ~]# docker images  | grep none | awk '{print $3}' | xargs docker rmi
```

### 1.2、移动镜像到另一台服务器

```shell
#将镜像打包
docker save -o /home/rancher-agent.tar rancher/rancher-agent:v2.6.3
#将所有镜像打包
docker save -o /home/images.tar $(docker images -q) 
#移动镜像
scp /home/rancher-agent.tar root@目标服务器ip:/home
#在目标服务器加载镜像
docker load -i rancher-agent.tar
#用docker iamges就可以查看导移动的镜像了
```

## 2、容器

### 2.1、删除容器

```shell
# 停止所有容器 
[root@root ~]# docker stop $(docker ps -a -q) 
# 删除所有停止的容器 
[root@root ~]# docker rm $(docker ps -a -q)
# 停止包含某个名称的容器
[root@root ~]# docker stop $(sudo docker ps -aq --filter name=harbor)
# 删除包含某个名称的容器
[root@root ~]# docker rm $(sudo docker ps -aq --filter name=harbor)
```

· ### 2.2、进入容器

```shell
# docker exec -it 容器id /bin/bash
[root@k8s-node2 ~]# docker exec -it 656b16b69a04 /bin/bash
```

## 3、日志

### 3.1、查看指定容器日志

```shell
#bfff65e9caa1(容器id)
[root@root ~]# docker logs -f bfff65e9caa1

$ docker logs [OPTIONS] CONTAINER
  Options:
        --details        显示更多的信息
    -f, --follow         跟踪实时日志
        --since string   显示自某个timestamp之后的日志，或相对时间，如42m（即42分钟）
        --tail string    从日志末尾显示多少行日志， 默认是all
    -t, --timestamps     显示时间戳
        --until string   显示自某个timestamp之前的日志，或相对时间，如42m（即42分钟）
```

- 查看指定时间后的日志，只显示最后100行：

  ```shell
  #CONTAINER_ID  容器id
  [root@root ~]# docker logs -f -t --since="2018-02-08" --tail=100 CONTAINER_ID
  ```

- 查看最近30分钟的日志:

  ```shell
  #CONTAINER_ID  容器id
  [root@root ~]# docker logs --since 30m CONTAINER_ID
  ```

- 查看某时间之后的日志：

  ```shell
  #CONTAINER_ID  容器id
  [root@root ~]# docker logs -t --since="2018-02-08T13:23:37" CONTAINER_ID
  ```

- 查看某时间段日志：

  ```shell
  #CONTAINER_ID  容器id
  [root@root ~]# docker logs -t --since="2018-02-08T13:23:37" --until "2018-02-09T12:23:37" CONTAINER_ID
  ```

  

## 4、部署镜像

### 4.1、安装Mysql

- 建立映射文件夹

  ```shell
  mkdir -p /mydata/mysql/{logs,conf,data}
  -- logs为日志目录
  -- conf为my.cnf配置文件目录
  -- data为数据文件存放路径
  ```

- 配置mysql，my.cnf配置内容

  ```shell
  [mysqld]
  #mysql8.0部署配置文件
  user=mysql
  character-set-server=utf8
  default_authentication_plugin=mysql_native_password
  secure_file_priv=/var/lib/mysql
  expire_logs_days=7
  sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,ONLY_FULL_GROUP_BY
  max_connections=5000
  #skip-grant-tables
  skip-name-resolve
  lower_case_table_names=1
  [client]
  default-character-set=utf8
  #innodb_directories=”/data/mysqldata;”
  ```

- 安装mysql

  ```shell
  docker run -p 3306:3306 -d --name mysql  \
  --privileged=true   \
  -v /mydata/mysql/data:/var/lib/mysql \
  -v /mydata/mysql/conf:/etc/mysql \
  -v /mydata/mysql/logs:/var/log/mysql  \
  -v /etc/localtime:/etc/localtime  \
  -e MYSQL_ROOT_PASSWORD=123456  \
  mysql:8.0.25
  
  参数说明： 
  -p 代表端口映射（-p 3306:3306表示将容器3306端口映射到主机3306端口）
  -d 在后台运行容器，并且打印容器id
  --name 标识容器名称
  -v 给容器挂载存储卷，挂载到容器的某个目录（目录映射）
  -privileged=true 表示容器具有主机权限
  -e 表示配置环境变量
  ```

### 4.2、安装Nacos

```shell
docker run --name nacos -d -p 8848:8848 -p 9948:9948 --privileged=true --restart=always \
-e JVM_XMS=256m  \
-e JVM_XMX=256m  \
-e MODE=standalone  \
-e PREFER_HOST_MODE=hostname  \
-e SPRING_DATASOURCE_PLATFORM=mysql  \
-e MYSQL_SERVICE_HOST=192.168.56.11  \
-e MYSQL_SERVICE_PORT=3306  \
-e MYSQL_SERVICE_USER=root  \
-e MYSQL_SERVICE_PASSWORD=123456  \
-e MYSQL_SERVICE_DB_NAME=mysql  \
nacos/nacos-server:v2.1.0 

参数说明： 
-p 代表端口映射（-p 8848:8848表示将容器8848端口映射到主机8848端口，9948表示服务端gRPC请求服务端端口，用于服务间同步）
-d 在后台运行容器，并且打印容器id
--name 标识容器名称
-privileged=true 表示容器具有主机权限
-e 表示配置环境变量
```

### 4.3、安装Redis

- 建立挂载映射目录

  ```shell
  [root@localhost ~]# mkdir -p /mydata/redis/{conf,data}
  ```

- 安装

  ```shell
  docker run -itd  -p 6379:6379 \
  -v /mydata/redis/conf/redis.conf:/usr/local/etc/redis/redis.conf   \
  -v /mydata/redis/data/:/data   \
  --name redis   \
  redis   \
  redis-server /usr/local/etc/redis/redis.conf
  
  参数说明： 
  -p 代表端口映射（-p 6379:6379表示将容器6379端口映射到主机8848端口）
  -d 在后台运行容器，并且打印容器id
  --name 标识容器名称
  ```

  