# 制作Tomcat镜像

## 1、准备相关镜像上传Linux目录

![image-20221124161138593](..\images\docker\Docker制作镜像1.png)

## 2、编写Dockerfile

```sh

FROM centos:7
#注释作者
MAINTAINER wangmingyong<wangmingyong@boco.com.cn>
#将外部文件添加到内部
COPY readme.txt /usr/local/readme.txt
#将dk和tomcat加入到容器
ADD jdk-7u80-linux-x64.tar.gz /usr/local/
ADD apache-tomcat-7.0.109.tar.gz /usr/local/
#安装vim命令
RUN yum -y install vim
#设置环境
ENV MYPATH /usr/local
#设置工作目录
WORKDIR $MYPATH
#设置JAVA_HOME 环境变量，后边版本对应上方安装包
ENV JAVA_HOME /usr/local/jdk1.7.0_80
#设置CLASSPATH 环境变量
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
#设置CATALINA_HOME 环境变量
ENV CATALINA_HOME /usr/local/apache-tomcat-7.0.109
#设置CATALINA_BASE 环境变量
ENV CATALINA_BASE /usr/local/apache-tomcat-7.0.109
#设置PATH
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin
#暴露端口
EXPOSE 8080
#指定容器启动后执行的命令
CMD /usr/local/apache-tomcat-7.0.109/bin/startup.sh && tail -F /usr/local/apache-tomcat-7.0.109/logs/catalina.out
```

## 3、生成镜像

```shell
[root@boco images]# docker build -t tomcat:7.0.109-arthas .
```

## 4、启动镜像

```shell
[root@boco ~]# docker run --name tomcat-test -p 8088:8080 tomcat:7.0.109-arthas
```

![image-20221124161835789](..\images\docker\Docker制作镜像2.png)

## 5、将镜像推送Harbor

### 5.1、给镜像打上tag

```shell
[root@boco images]# docker tag tomcat:7.0.109-arthas 192.168.26.39/scms/tomcat:7.0.109-arthas
```

### 5.2、推送到Harbor

```shell
[root@boco images]# docker push 192.168.26.39/scms/tomcat:7.0.109-arthas
```

