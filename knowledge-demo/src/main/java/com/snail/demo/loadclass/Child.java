package com.snail.demo.loadclass;

/**
 * @Description: Child
 * @Author: William.Wmy
 * @CreateDate: 2024/3/18 16:11
 * @Version: V1.0
 */
public class Child extends Parent {

    private String A = "Child 实例变量";
    private static String B = "Child 静态变量";

    static {
        System.out.println("Child静态变量:"+B);
        System.out.println("Child静态代码块");
    }

    public Child() {
        System.out.println("Child实例变量:"+A);
        System.out.println("Child构造方法");
    }
}
