package com.snail.demo.loadclass;

/**
 * @Description: Parent
 * @Author: William.Wmy
 * @CreateDate: 2024/3/18 16:11
 * @Version: V1.0
 */
public class Parent {

    private static String A = "Parent 静态变量";
    private String B = "Parent 实例变量";

    static {
        System.out.println("Parent静态变量:"+A);
        System.out.println("Parent静态代码块");
    }

    public Parent() {
        System.out.println("Parent实例变量:"+B);
        System.out.println("Parent构造方法");
    }
}
